/* GNU GPL version 3 */

#include <stdio.h>
#include <string.h>

#define LBS 32*1032
char lbuf[LBS];
char c = 0;
char pc = 0;
int ic = 0;
char *p = (char *) 0;
char *q = (char *) 0;
char *r = (char *) 0;
int pend = 0;

int
main (int argc, char *argv)
{

  printf ("#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)\n");
  printf ("#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)\n");
  printf
    ("#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)\n");

  for (;;)
    {
      ic = 0;
      memset (lbuf, 0, LBS);

      while ((c = fgetc (stdin)) != EOF)
	{
	  if (c == '\n')
	    {
	      break;
	    }
	  lbuf[ic] = (char) c;
	  ic++;
	}

      if (ic == 0)
	{
	  break;
	}

      p = strstr (lbuf, "path");

      if (p)
	{
	  q = strstr (p, " d=\"");
	  if (q)
	    {
	      r = strchr (q, '\"');
	      if (r)
		{
		  r++;

		  while ((*r))
		    {
		      if ((*r) == '\"')
			{
			  break;
			}
		      if ((*r) == 'z')
			{
			  break;
			}
		      if ((*r) == 'm')
			{
			  if (pend)
			    {
			      printf (");\n");
			    }
			  printf ("MCOMMAND(");
			  pend = 1;
			  r++;
			}
		      if ((*r) == 'l')
			{
			  if (pend)
			    {
			      printf (");\n");
			    }
			  printf ("LCOMMAND(");
			  pend = 1;
			  r++;
			}
		      if ((*r) == 'c')
			{
			  if (pend)
			    {
			      printf (");\n");
			    }
			  printf ("CCOMMAND(");
			  pend = 1;
			  r++;
			}
		      if ((*r) == ' ')
			{
			  (*r) = ',';
			}
		      printf ("%c", *r);
		      r++;
		    }
		  if (pend)
		    {
		      printf (");\n");
		    }
		  pend = 0;
		}
	    }
	}

    }

  return (0);
}

/* end */
