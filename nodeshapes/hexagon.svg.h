#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(0.001638,49.928436);
LCOMMAND(20.998971,-49.928161);
LCOMMAND(55.996859,0);
LCOMMAND(20.998947,49.928161);
LCOMMAND(-20.998947,49.92823);
LCOMMAND(-55.996859,0);
LCOMMAND(-20.998971,-49.92823);
