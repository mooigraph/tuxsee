#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(0.001461,20.164606);
LCOMMAND(20.765173,-20.166117);
LCOMMAND(83.234846,0);
LCOMMAND(0,100.999985);
LCOMMAND(-104.000019,0);
LCOMMAND(0,-80.833868);
