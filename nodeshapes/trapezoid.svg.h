#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(0.001794,99.995079);
LCOMMAND(18.375489,-99.995748);
LCOMMAND(61.253256,0);
LCOMMAND(18.375465,99.995748);
LCOMMAND(-98.00421,0);
