#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(83.004883,99.502281);
LCOMMAND(3.299919,-13.200714);
LCOMMAND(13.200981,-3.30024);
LCOMMAND(-16.5009,16.500954);
LCOMMAND(-82.503042,0);
LCOMMAND(0,-99.003847);
LCOMMAND(99.003943,0);
LCOMMAND(0,82.502894);
