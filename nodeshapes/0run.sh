#!/bin/sh
# assuming bash shell
rm ./a.out
indent s2c.c
gcc s2c.c
for i in *.svg
do
	echo "converting $i"
	cat $i |./a.out >$i.h
	cpp -E -P <$i.h >$i.c
done
rm ./a.out
rm s2c.c~
#end

