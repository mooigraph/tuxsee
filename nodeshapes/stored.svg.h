#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(16.668457,-0.001147);
LCOMMAND(83.333855,0);
CCOMMAND(-9.204796,0,-16.666779,22.361598,-16.666779,49.94583);
CCOMMAND(0,27.584614,7.461983,49.946049,16.666779,49.946049);
LCOMMAND(-83.333855,0);
LCOMMAND(0,0);
CCOMMAND(-9.204898,0,-16.666863,-22.361435,-16.666863,-49.946049);
CCOMMAND(0,-27.584232,7.461965,-49.94583,16.666863,-49.94583);
