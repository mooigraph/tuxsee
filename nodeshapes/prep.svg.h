#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(0.001692,49.148949);
LCOMMAND(19.399959,-49.150682);
LCOMMAND(58.200041,0);
LCOMMAND(19.400024,49.150682);
LCOMMAND(-19.400024,49.151901);
LCOMMAND(-58.200041,0);
LCOMMAND(-19.399959,-49.151901);
