#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(10.23709,0.68179);
LCOMMAND(-9.735158,9.637667);
LCOMMAND(0,87.842156);
LCOMMAND(87.430715,0.223083);
LCOMMAND(11.034882,-9.860947);
LCOMMAND(0,-87.84196);
LCOMMAND(-88.730439,0);
LCOMMAND(0,0);
