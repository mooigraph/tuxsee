#define MCOMMAND(dx,dy) cairo_rel_move_to(crp,dx,dy)
#define LCOMMAND(dx,dy) cairo_rel_line_to(crp,dx,dy)
#define CCOMMAND(x1,y1,x2,y2,x,y) cairo_rel_curve_to(crp,x1,y1,x2,y2,x,y)
MCOMMAND(0.500176,46.50098);
LCOMMAND(47.501994,-46.000977);
LCOMMAND(47.501736,46.000977);
LCOMMAND(-47.501736,46.000996);
LCOMMAND(-47.501994,-46.000996);
