
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

/*
 * node database indexed on the uniq node id number which is an int
 */

#include <stdio.h>
#include <string.h>

#include "splay-tree.h"
#include "main.h"
#include "mem.h"
#include "uniqnodeid.h"

/* nodes database */
static splay_tree gunode = (splay_tree) 0;

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void uniqnodeid_splay_tree_delete_key_fn(splay_tree_key keydata);

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void uniqnodeid_splay_tree_delete_value_fn(splay_tree_value valuedata);

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void uniqnodeid_splay_tree_delete_key_fn(splay_tree_key keydata)
{
	/* key is a int id not allocated */
	if (keydata) {
	}
	return;
}

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void uniqnodeid_splay_tree_delete_value_fn(splay_tree_value valuedata)
{
	/* value is (struct unode *) deallocated in nes */
	if (valuedata) {
	}
	return;
}

/* clear database */
void uniqnodeid_clear(void)
{
	gunode = splay_tree_delete(gunode);
	return;
}

/* find node in database */
struct unode *uniqnodeid(int id)
{

	splay_tree_node spn = (splay_tree_node) 0;

	if (gunode == NULL) {
		/* indexed on int id */
		gunode = splay_tree_new(splay_tree_compare_ints,	/* splay_tree_compare_fn */
					uniqnodeid_splay_tree_delete_key_fn,	/* splay_tree_delete_key_fn */
					uniqnodeid_splay_tree_delete_value_fn	/* splay_tree_delete_value_fn */
		    );
	}

	spn = splay_tree_lookup((splay_tree) gunode, (splay_tree_key) id);

	if (spn) {
		return ((struct unode *)spn->value);
	} else {
		return ((struct unode *)0);
	}
}

/* add node to database */
void uniqnodeid_add(int id, struct unode *un)
{

	if (un == (struct unode *)0) {
		return;
	}

	if (gunode == NULL) {
		/* indexed on int id */
		gunode = splay_tree_new(splay_tree_compare_ints,	/* splay_tree_compare_fn */
					uniqnodeid_splay_tree_delete_key_fn,	/* splay_tree_delete_key_fn */
					uniqnodeid_splay_tree_delete_value_fn	/* splay_tree_delete_value_fn */
		    );
	}

	splay_tree_insert(gunode,	/* splay_tree */
			  (splay_tree_key) id, (splay_tree_value) un);

	return;
}

/* end */
