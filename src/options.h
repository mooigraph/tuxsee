
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef OPTIONS_H
#define OPTIONS_H 1

/* */
extern void commandline_options (int argc, char *argv[]);

/* */
extern int option_splines;

/* */
extern int option_no_singlenodes;

/* */
extern int option_selfedges;

/* */
extern char *option_filename;

/* */
extern char *option_outfilename;

/* */
extern int option_init_folded;

/* */
extern int option_init_unfolded;

/* */
extern int option_nodenames;

/* */
extern int option_no_edgelabels;

/* */
extern int option_multi_arrows;

/* */
extern int option_subgraph_area;

/* */
extern int option_toggle_altcolor;

/* */
extern int option_testcode;

/* */
extern int option_memlog;

/* */
extern int option_memtrack;

/* */
extern int option_ldebug;

/* */
extern int option_gdebug;

/* */
extern int option_placedebug;

/* */
extern int option_colorpage;

/* */
extern int option_parsedebug;

/* */
extern int option_dryrun;

/* */
extern char *option_algo;

/* */
extern char *option_prealgo;

/* follow edge in prio algo, 1/0 */
extern int option_feprio;

/* xy-spread factor */
extern int option_xyspread;

/* down() 1 or up() 0 as last in prio algo */
extern int option_pud;

#endif

/* end */
