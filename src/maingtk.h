
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef MAINGTK_H
#define MAINGTK_H 1

/* pixels in padding for gtk_box_pack try values 0, 1, 5, 20 */
#define PACKPADDING 0

extern void maingtk (int argc, char *argv[]);
extern void maingtk_textsizes (void);
extern void update_status_text (char *text);
extern void maingtk_saveaspng (char *infile, char *outfile);
extern void maingtk_saveassvg (char *infile, char *outfile);

/* check for valid save filename */
extern char *valid_save_filename (char *fname);

/* draw node and edge data if valid data */
extern struct drawn *drawnldata;
extern struct drawe *draweldata;

/* zoom scaling factor changed by zoom slider */
extern double zfactor;

/* x offset changed by x slider */
extern int vxmin;

/* y offset changed by y slider */
extern int vymin;

/* draw in alt color grey when set */
extern int altcolor;

/* name of last dir where file was opened */
extern char *lastdir;

/* name of last dir where file was saved */
extern char *lastdir2;

#endif

/* end */
