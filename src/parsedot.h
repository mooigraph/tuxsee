
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef PARSEDOT_H
#define PARSEDOT_H 1

/* delayed compound edges (struct only used here) */
struct des
{
  /* from nodename/subgraph */
  union
  {
    char *name;
    struct usubg *subg;
  } f;
  /* to nodename/subgraph */
  union
  {
    char *name;
    struct usubg *subg;
  } t;
  /* type of data */
  unsigned char typ;
};

/* defs for typ in struct des */
#define DES_N_S 1		/* from is nodename, to is subgraph */
#define DES_S_S 2
#define DES_N_N 3
#define DES_S_N 4		/* from is subgraph, to is nodename */

/* helper struct for splay edge cache (struct only used here) */
struct ecdot
{
  char *fromname;
  char *toname;
};

/* parsedot.c */
extern int token;
extern char *nd_url;
extern int nd_bcolor;
extern int nd_cs;
extern int nd_fillcolor;
extern char *nd_label;
extern unsigned char nd_labeltype;
extern int nd_shape;
extern char *nd_fontsize;
extern char *nd_fontname;
extern unsigned char nd_stylefilled;
extern int ed_color;
extern int ed_fcolor;
extern char *ed_ffontname;
extern unsigned char ed_thickness;
extern char *ed_label;
extern int ed_style;
extern char *ed_fontsize;
extern int ed_cs;
extern int nsubgraph;

/* parsedot.c */
extern int dot_lex (void);
extern void dot_unlex (void);
extern void set_node_defaults (struct unode *un, struct usubg *subg);
extern void set_edge_defaults (struct uedge *ue, struct usubg *subg);
extern char *is_a_dot_escstring_node (char *str, struct unode *un);
extern char *is_a_dot_htmlstring_node (char *str, struct unode *un);
extern int is_a_dotcompoundgraph (struct usubg *subg);
extern void dot_desadd (char *fname, struct usubg *fsubg, char *tname, struct usubg *tsubg, int code);
extern void dot_pe_clear (void);
extern void set_subg_defaults (struct usubg *rootedon, struct usubg *newsg);
extern int parsedot2 (struct usubg *subg);

/* parsedot-nd.c */
extern int is_a_dotnodedefault (struct usubg *subg);
extern int is_a_dot_shape_name (char *s);

/* parsedot-n.c */
int is_a_dotne_node (struct usubg *subg, char *fname, char *port, char *compass);

/* parsedot-ed.c */
extern int is_a_dotedgedefault (struct usubg *subg);

/* parsedot-e.c */
extern int is_a_dotne (struct usubg *subg);
extern struct usubg *pe_lastsubg;
extern struct unode *pe_lastnode;
extern int pe_lasttoken;

/* parsedot-sg.c */
extern int is_a_dotsubgraph (struct usubg *subg);

#endif

/* end */

