#
# /*
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  *
#  * SPDX-License-Identifier: GPL-3.0+
#  * License-Filename: LICENSE
#  *
#  */
#

bin_PROGRAMS=tuxsee

tuxsee_SOURCES = \
	color.c \
	folding.c \
	lex-dot.c \
	lex-tys.flex.c \
	lmain.c \
	manual.c \
	mem.c \
	nes.c \
	options.c \
	parsedot.c \
	parsetys.c \
	parsedot-n.c \
	parsedot-nd.c \
	parsedot-ed.c \
	parsedot-e.c \
	parsedot-sg.c \
	rhp.c \
	source.c \
	splay-tree.c \
	uniqnode.c \
	uniqnodeid.c \
	uniqstring.c

# gtk2+ version
tuxsee_CFLAGS = \
	@CFLAGS@ @PACKAGE_CFLAGS@ @WARNING_CFLAGS@ @GTK_CFLAGS@

# gtk2+ version
tuxsee_tolink = \
	@GTK_LIBS@

# gtk3+ version Linux
tuxsee$(EXEEXT): main.c maingtk.c maingtk2.c $(tuxsee_SOURCES)
	$(CC) $(tuxsee_CFLAGS) main.c maingtk.c maingtk2.c $(tuxsee_SOURCES) -I. -I.. $(tuxsee_tolink) -lm -o tuxsee

# test with Linux sparse tool
sparsetuxsee$(EXEEXT): main.c maingtk.c maingtk2.c $(tuxsee_SOURCES)
	sparsec @PACKAGE_CFLAGS@ @WARNING_CFLAGS@ @GTK_CFLAGS@ main.c maingtk.c maingtk2.c $(tuxsee_SOURCES) -I. -I.. $(tuxsee_tolink) -lm -o tuxsee

# windows version
# the G_OS_WIN32 flag is used to change settings in glib-2/gio/ gcredentials.h and uid_t
# the _WIN32 is used by libxml2
tuxsee_exeCFLAGS = \
	-mms-bitfields \
	-DWIN32 \
	-DG_OS_WIN32 \
	-DG_PLATFORM_WIN32 \
	-D_WIN32

# windows version
tuxsee_tolinkw32dll = \
	-llibgtk-win32-2.0-0 \
	-llibcairo-2 \
	-llibglib-2.0-0 \
	-llibgdk-win32-2.0-0 \
	-llibgobject-2.0-0 \
	-llibpangocairo-1.0-0 \
	-llibpango-1.0-0 \
	-llibrsvg-2-2 \
	-llibxml2-2

# windows version
tuxsee_tolinkw32 = \
	-lgdi32 \
	-lcomdlg32 \
	-lcomctl32 \
	-luuid \
	-loleaut32 \
	-lole32

# windows version
tuxsee_exeI = \
	-I/usr/include/gtk-2.0 \
	-I/usr/include/librsvg-2.0 \
	-I/usr/include/glib-2.0 \
	-I/usr/lib/x86_64-linux-gnu/glib-2.0/include \
	-I/usr/lib/x86_64-linux-gnu/gtk-2.0/include \
	-I/usr/include/gdk-pixbuf-2.0 \
	-I/usr/include/cairo \
	-I/usr/include/libpng12 \
	-I/usr/include/pixman-1 \
	-I/usr/include/freetype2 \
	-I/usr/include/atk-1.0 \
	-I/usr/include/pango-1.0 \
	-I/usr/include/pixman-1

# cross compiled windows 32bits version on Linux runs with wine.
# note the -L./bin which should be a temporary "bin" directory
# in this src directory with the win32 dll's in it.
# charset.dll iconv.dll intl.dll jpeg62.dll libatk-1.0-0.dll
# libcairo-2.dll libcroco-0.6-3.dll libgdk_pixbuf-2.0-0.dll
# libgdk-win32-2.0-0.dll libgio-2.0-0.dll libglib-2.0-0.dll
# libgmodule-2.0-0.dll libgobject-2.0-0.dll libgthread-2.0-0.dll
# libgtk-win32-2.0-0.dll libpango-1.0-0.dll libpangocairo-1.0-0.dll
# libpangoft2-1.0-0.dll libpangowin32-1.0-0.dll libpng13.dll
# librsvg-2-2.dll libtiff3.dll libxml2-2.dll zlib1.dll
# if there is a problem with fonts with the cairo lib the
# mscorefonts must be installed
tuxseeexe: main.c
	$(CC) -L./bin -Wl,-verbose $(tuxsee_exeCFLAGS) main.c maingtk.c maingtk2.c rpl.c $(tuxsee_SOURCES) $(tuxsee_exeI) -I. -I.. $(tuxsee_tolinkw32dll) $(tuxsee_tolinkw32) -o tuxsee.exe

# test prog
mainrhp$(EXEEXT): mainrhp.c rhp.c mem.c
	$(CC) -std=c99 -O0 -Wall -I. -I.. mainrhp.c rhp.c mem.c -o mainrhp

# rhp static lib
librhp: rhp.c mem.c
	$(CC) -I. -I.. -c rhp.c
	$(CC) -I. -I.. -c mem.c
	ar -cvq librhp.a rhp.o mem.o
	ar -t librhp.a
	$(CC) -I. -I.. -fPIC -c rhp.c
	$(CC) -I. -I.. -fPIC -c mem.c
	$(CC) -shared -Wl,-soname,librhp.so.1 -o librhp.so.1 rhp.o mem.o
	nm librhp.so.1

llvmdot: rhp.c
	clang -emit-llvm -c -I. -I.. rhp.c -o rhp.bc
	opt-7 -dot-cfg-only rhp.bc
	opt-7 -dot-cfg rhp.bc
	opt-7 -dot-callgraph rhp.bc
	opt-7 -dot-dom rhp.bc
	opt-7 -dot-postdom rhp.bc

# run emscripten and generate javascript of layouter core
# mainrhp.c is a test program for use with rhp.c
# needs llvm with javascript output compiled in or use emscripten sdk
ems: rhp.c mem.c
	emcc -v --emit-symbol-map -O0 -I. -I.. rhp.c mem.c rhpmain.c -o rhp.js

indent:
	./Lindent $(tuxsee_SOURCES)

#
clean-generic:
	rm -v -f ./massif.out.*
	rm -v -f ./mainrhp
	rm -v -f ./*.rhp.txt
	rm -v -f ./a.out
	rm -v -f ./*.pdf
	rm -v -f *~
	rm -v -f ./*.so.1
	rm -v -f ./*.o
	rm -v -f ./*.a
	rm -v -f ./*.i
	rm -v -f ./*.s
	rm -v -f ./O
	rm -v -f ./OO
	rm -v -f ./O1
	rm -v -f ./O2
	rm -v -f ./O3
	rm -v -f ./dsmake.output
	rm -v -f ./dsmake.warnings
	rm -v -f ./dsmake.errors
	rm -v -f ./manual.pdf
	rm -v -f *.dot
	rm -v -f *.*r.*
	rm -v -f *.*t.*
	rm -v -f *.bc
	rm -v -f *.plist
	cp source.c.org source.c
	cp manual.c.org manual.c
	rm -v -f tuxsee.exe

# use flawfinder from d. wheeler
# flawfinder --listrules (to see the checks)
# -m 0 (use min. level 0)
# -S (print single lines)
flawf:
	flawfinder -m 0 -S *.c

# using --show-leak-kinds=all will enable to find
# issues with the use-after-free exploits
#vcheck:
#valgrind --tool=memcheck -v --leak-check=full --show-leak-kinds=all --track-origins=yes  ./tuxsee 1>O1 2>O2

# using -t option
# tuxsee -t yes -t yes >O
# grep memtrace O
# sort on first chars
# sort -k 1,9 <O

# when cross-compile with mingw64 for windows -mwindows option is needed to avoid console window at startup
# but the the messages to stdout/stderr bare not visible.

# /* end */
