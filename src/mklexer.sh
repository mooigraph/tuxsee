#!/bin/sh -x
#
# /*
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  *
#  * SPDX-License-Identifier: GPL-3.0+
#  * License-Filename: LICENSE
#  *
#  */
#

# own customized flex tool lexer generator
# MYFLEX=$(HOME)/bin/myflex2.5.36/bin/myflex
MYFLEX=flex

# own customized bison parser generator
# MYBISON=$(HOME)/bin/mybison2.7/bin/mybison
MYBISON=bison

# own customized flex tool lexer generator commandline options
# -d   == add debug code
# -8   == generate 8 bits scanner
# -f   == fast large scanner
# -R   == reentrant lexer for threaded parsing

echo "tys lexer"
rm -v -f lex-tys.flex.c
$MYFLEX -d -8 -f --yylineno --outfile=lex-tys.flex.c lex-tys.l

# /* end */
