
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef MAINGTK2_H
#define MAINGTK2_H 1

/* maintgk.c where to draw */
extern GtkWidget *drawingarea1;

extern GtkWidget *mainwindow1;

/* maingtk sliders */
#if GTK_HAVE_API_VERSION_2
extern GtkObject *adjvscale1;
extern GtkObject *adjvscale2;
extern GtkObject *adjhscale1;
#endif

#if GTK_HAVE_API_VERSION_3
extern GtkAdjustment *adjvscale1;
extern GtkAdjustment *adjvscale2;
extern GtkAdjustment *adjhscale1;
#endif

/* save as x3d output.html format */
extern void on_top_level_window_saveasx3d1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as vrml 3d */
extern void on_top_level_window_saveasvrml1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as dot file format */
extern void on_top_level_window_saveasdot1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as gml file format */
extern void on_top_level_window_saveasgml1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as dia software xml format */
extern void on_top_level_window_saveasdia1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as els format */
extern void on_top_level_window_saveasels1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save tulip graph software tlp file format */
extern void on_top_level_window_saveastulip1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save source.tgz */
extern void
on_top_level_window_saveassource1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save manual.pdf */
extern void
on_top_level_window_saveasmanual1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as svg-j document */
extern void on_top_level_window_saveassvgjs1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as svg document */
extern void on_top_level_window_saveassvg1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* save as pdf document */
extern void
on_top_level_window_saveaspdf1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* treeview window */
extern void on_treeview_window_destroy (GtkWidget * widget, gpointer data);

/* treeview window */
extern void on_top_level_window_treeview (GtkWidget * widget, gpointer data);

/* save as png image */
extern void on_top_level_window_saveaspng1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* called from main() when running as console tool generating png image */
extern void maingtk_saveaspng (char *infile, char *outfile);

#endif

/* end */
