


/* default setting for graph as graph[option,option option...option] */
static int
is_a_dotgraphdefault (struct usubg *subg)
{

    if(token != DOT_GRAPH){
	    return(0);
	}

  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }

  /* [ */
  token = dot_lex ();
  if (token != DOT_BRACKETO)
    {
      parser_error_string = "expected [ at graph default";
      return (1);
    }

  /* keep on reading the attributes */
  for (;;)
    {
      token = dot_lex ();
      if (token == 0)
	{
	  parser_error_string = "un-expected end-of-file at graph default";
	  return (1);
	}
      if (token == DOT_COMMA)
	{
	  /* skip optional comma */
	  token = dot_lex ();
	}
	  if (token == DOT_SEMIC)
	    {
	      /* skip optional semicomma */
	      token = dot_lex ();
	    }

      if (token == DOT_BRACKETC)
	{
	  /* end of options */
	  break;
	}
      if (token == DOT_CHAR)
	{
	  /* single char keyword */
	  if (strcmp (lastchar, "K") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default K option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default K option";
		  return (1);
		}
	    }
	  else
	    {
	      /* unknown char=value */
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default unknown char option";
		  return (1);
		}
	      /* unknown or "unknown" */
	      token = dot_lex ();
	    }
	}
      else if (token == DOT_ID)
	{
	  /* multi char keywords */
	  /*
	   * start of the options
	   */
	  if (strcmp (lastid, "_background") == 0)
	    {
	      /* this does not seem to work. xdot format */
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default _background option";
		  return (1);
		}
	      /* color or "color" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected name of color at graph default _background option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "bb") == 0)
	    {
	      /* bb=rect as in bb="0,0,100,200" */
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default bb option";
		  return (1);
		}
	      /* "rect" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected rectangle at graph default bb option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "bgcolor") == 0)
	    {
	      /* drawing background color */
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default bgcolor option";
		  return (1);
		}
	      /* color or "color" or transparent or "transparent" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected color at graph default bgcolor option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "center") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default center option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* 1/0 */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default center option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "charset") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default charset option";
		  return (1);
		}
	      /* "name" or name */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected name of char set at graph default charset option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "clusterrank") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default clusterrank option";
		  return (1);
		}
	      /* local/global/none */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected local, global or none at graph default clusterrank option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "colorscheme") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default colorscheme option";
		  return (1);
		}
	      /* name or "name" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected name of color scheme at graph default colorscheme option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "comment") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default comment option";
		  return (1);
		}
	      /* name or "name" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default comment option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "compound") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default compound option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default compound option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "concentrate") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default concentrate option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default concentrate option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "Damping") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default damping option";
		  return (1);
		}
	      /* number or "number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default damping option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "defaultdist") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default defaultdist option";
		  return (1);
		}
	      /* number or "number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default defaultdist option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "dim") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default dim option";
		  return (1);
		}
	      /* number or "number" int */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default dim option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "dimen") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default dimen option";
		  return (1);
		}
	      /* number or "number" int */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default dimen option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "diredgeconstraints") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default diredgeconstraints option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default diredgeconstraints option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "dpi") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default dpi option";
		  return (1);
		}
	      /* number or "number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default dpi option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "epsilon") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default epsilon option";
		  return (1);
		}
	      /* number or "number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default epsilon option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "esep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default esep option";
		  return (1);
		}
	      /* number or "number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default esep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fillcolor") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fillcolor option";
		  return (1);
		}
	      /* color or "color" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected color at graph default fillcolor option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fontcolor") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fontcolor option";
		  return (1);
		}
	      /* color or "color" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected color at graph default fontcolor option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fontname") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fontname option";
		  return (1);
		}
	      /* name or "name" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected name at graph default fontname option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fontnames") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fontnames option";
		  return (1);
		}
	      /* names or "names" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected name at graph default fontnames option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fontpath") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fontpath option";
		  return (1);
		}
	      /* path or "path" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected pathname at graph default fontpath option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "fontsize") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default fontsize option";
		  return (1);
		}
	      /* fp-number or "fp-number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default fontsize option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "forcelabels") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default forcelabels option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default forcelabels option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "gradientangle") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default gradientangle option";
		  return (1);
		}
	      /* int number or "int number" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default gradientangle option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "href") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default href option";
		  return (1);
		}
	      /* url or "url" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected url at graph default href option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "id") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default id option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default id option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "imagepath") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default imagepath option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default imagepath option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "inputscale") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default inputscale option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default inputscale option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "label") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default label option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default label option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "label_scheme") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default label_scheme option";
		  return (1);
		}
	      /* int or "int" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default label_scheme option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "labeljust") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default labeljust option";
		  return (1);
		}
	      /* c/l/r or "c/l/r" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_CHAR)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected ' c ', ' l ' or ' c ' at graph default labeljust option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "labelloc") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default labelloc option";
		  return (1);
		}
	      /* t/b or "t/b" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_CHAR)
		{
		  /* strcmp(lastchar,"t")==0 */
		}
	      else
		{
		  parser_error_string = "expected ' t ' or ' b ' at graph default labelloc option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "landscape") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default landscape option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default landscape option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "layerlistsep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default layerlistsep option";
		  return (1);
		}
	      /* "comma-string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default layerlistsep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "layers") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default layers option";
		  return (1);
		}
	      /* "id:id:id..." */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default layers option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "layerselect") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default layerselect option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default layerselect option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "layersep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default layersep option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default layersep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "layout") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default layout option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default layout option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "levels") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default levels option";
		  return (1);
		}
	      /* int or "int" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default levels option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "levelsgap") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default levelsgap option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default levelsgap option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "lheight") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default lheight option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default lheight option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "lp") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default lp option";
		  return (1);
		}
	      /* "point" as in "%f,%f(!)?" also 3d */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected point at graph default lp option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "lwidth") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default lwidth option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default lwidth option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "margin") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default margin option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* in a node: margin with optional x,y direction in pixels around label text %lf,%lf */
		  /* in a graph default the page margin as fp number in inch */
		}
	      else
		{
		  parser_error_string = "expected number at graph default margin option";
		  return (1);
		}
	      /* i=sscanf("lf,%lf"); i has no of args found */
	    }
	  else if (strcmp (lastid, "maxiter") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default maxiter option";
		  return (1);
		}
	      /* int-num or "int-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default maxiter option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "mclimit") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default mclimit option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default mclimit option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "mindist") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default mindist option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default mindist option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "mode") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default mode option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default mode option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "model") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default model option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default model option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "mosek") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default mosek option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default mosek option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "nodesep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default nodesep option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default nodesep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "nojustify") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default nojustify option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default nojustify option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "normalize") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default normalize option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default normalize option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "notranslate") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default notranslate option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default notranslate option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "nslimit") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default nslimit option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default nslimit option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "nslimit1") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default nslimit1 option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default nslimit1 option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "ordering") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default ordering option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default ordering option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "orientation") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default orientation option";
		  return (1);
		}
	      /* "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default orientation option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "outputorder") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default outputorder option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default outputorder option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "overlap") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default overlap option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default overlap option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "overlap_scaling") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default overlap_scaling option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default overlap_scaling option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "overlap_shrink") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default overlap_shrink option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default overlap_shrink option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "pack") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default pack option";
		  return (1);
		}
	      /* true/false or "true/false" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default pack option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "packmode") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default packmode option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default packmode option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "pad") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default pad option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default pad option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "page") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default page option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default page option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "pagedir") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default pagedir option";
		  return (1);
		}
	      /* dir-string or "dir-string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected direction string at graph default pagedir option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "quadtree") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default quadtree option";
		  return (1);
		}
	      /* quad-string or "quad-string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected quad string at graph default quadtree option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "quantum") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default quantum option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default quantum option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "rankdir") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default rankdir option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default rankdir option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "ranksep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default ranksep option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default ranksep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "ratio") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default ratio option";
		  return (1);
		}
	      /* fp-num or "fp-num" or fill or auto */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number or fill or auto at graph default ratio option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "remincross") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default remincross option";
		  return (1);
		}
	      /* true/false */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default remincross option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "repulsiveforce") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default repulsiveforce option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default repulsiveforce option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "resolution") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default resolution option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default resolution option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "root") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default root option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default root option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "rotate") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default rotate option";
		  return (1);
		}
	      /* int-num or "int-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default rotate option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "rotation") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default rotation option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default rotation option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "scale") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default scale option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default scale option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "searchsize") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default searchsize option";
		  return (1);
		}
	      /* int-num or "int-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default searchsize option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "sep") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default sep option";
		  return (1);
		}
	      /* fp-num or "fp-num" or pointf */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default sep option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "showboxes") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default showboxes option";
		  return (1);
		}
	      /* int-num or "int-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default showboxes option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "size") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default size option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default size option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "smoothing") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default smoothing option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default smoothing option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "sortv") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default sortv option";
		  return (1);
		}
	      /* int-num or "int-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default sortv option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "splines") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default splines option";
		  return (1);
		}
	      /* true/false */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default splines option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "start") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default start option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default start option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "style") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default style option";
		  return (1);
		}
	      /* string or "string" comma sep list */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default style option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "stylesheet") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default stylesheet option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default stylesheet option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "target") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default target option";
		  return (1);
		}
	      /* string or "string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default target option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "truecolor") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default truecolor option";
		  return (1);
		}
	      /* true/false */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected true or false at graph default truecolor option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "url") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default url option";
		  return (1);
		}
	      /* url-string or "url-string" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default url option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "viewport") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default viewport option";
		  return (1);
		}
	      /* string or "string" "%lf,%lf,%lf,%lf,%lf" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected string at graph default viewport option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "voro_margin") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default voro_margin option";
		  return (1);
		}
	      /* fp-num or "fp-num" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default voro_margin option";
		  return (1);
		}
	    }
	  else if (strcmp (lastid, "xdotversion") == 0)
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default xdotversion option";
		  return (1);
		}
	      /* example 1.2 or "1.2" */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_NUM)
		{
		  /* */
		}
	      else
		{
		  parser_error_string = "expected number at graph default xdotversion option";
		  return (1);
		}
	    }
	  else
	    {
	      /* = */
	      token = dot_lex ();
	      if (token != DOT_IS)
		{
		  parser_error_string = "expected = at graph default unknown option";
		  return (1);
		}
	      /* unknown or "unknown" or number or single char */
	      token = dot_lex ();
	      if (token == DOT_STRING)
		{
		  /* */
		}
	      else if (token == DOT_ID)
		{
		  /* */
		}
	      else if (token == DOT_CHAR)
		{
		  /* */
		}
	      else
		{
		  /* ? */
		}
	    }
	}
      else
	{
	  parser_error_string = "expected keyword at graph default";
	  return (1);
	}

      /*
       * end of the options
       */
    }

  /* oke */
  return (1);
}


/* _background color in xdot format, does not seem to work */
static int
is_a_dot_background (struct usubg *subg)
{
  char *s = NULL;
  if (strcmp (lastid, "_background") != 0)
    {
      return (0);
    }
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      s = laststring;
    }
  else if (token == DOT_ID)
    {
      s = lastid;
    }
  else
    {
      parser_error_string = "expected name of color at _background";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* background color in root graph */
      if (s)
	{
	}
    }
  return (1);
}

/* bb=rect as in bb="0,0,100,200" int values */
static int
is_a_dotbb (struct usubg *subg)
{
  if (strcmp (lastid, "bb") != 0)
    {
      return (0);
    }
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected rectangle at bb";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotbgcolor (struct usubg *subg)
{
  int c = 0;
  char *s = NULL;
  /* is background color for drawing and the initial fill color */
  /* same as graph[bgcolor=color]; */
  if (strcmp (lastid, "bgcolor") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* color or "color" or transparent or "transparent" or colorlist */
   /* in a colorlist a gradient is possible
  * with colors seperated by ':'
  * and color specified as <color>;value
  * where color is a known color name or hex code
  * and value a floating point number of percentage
  * and all floating point numbers must be 1
  * example with 30% red and the rest green:
  * bgcolor="#ff0000;0.3:#00ff00";
  */

  token = dot_lex ();
  if (token == DOT_STRING)
    {
      s = laststring;
    }
  else if (token == DOT_ID)
    {
      s = lastid;
    }
  else
    {
      parser_error_string = "expected name of color at graph bgcolor option";
      return (1);
    }
  if (strlen (s) == 0)
    {
      /* "" color */
      /* unknown color */
      if (subg)
	{
	  /* */
	}
      else
	{
	  /* white */
	  bgcr = 0xff;
	  bgcg = 0xff;
	  bgcb = 0xff;
	}
    }
  else
    {
      c = colorcode (s);
      if (c == -1)
	{
	  /* unknown color or transparent mode */
	  if (subg)
	    {
	      /* */
	    }
	  else
	    {
	      /* white */
	      bgcr = 0xff;
	      bgcg = 0xff;
	      bgcb = 0xff;
	    }
	}
      else
	{
	  if (subg)
	    {
	      /* */
	    }
	  else
	    {
	      /* root graph background color */
	      bgcr = (c & 0x00ff0000) >> 16;
	      bgcg = (c & 0x0000ff00) >> 8;
	      bgcb = (c & 0x000000ff);
	    }
	}
    }
  return (1);
}

/* */
static int
is_a_dotcenter (struct usubg *subg)
{
  /* is default false and if true does center drawing on page */
  if (strcmp (lastid, "center") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph center option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotcharset (struct usubg *subg)
{
  /* charset="name" */
  if (strcmp (lastid, "charset") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of charset at graph charset option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotclusterrank (struct usubg *subg)
{
  /* is default local and can be global or none */
  if (strcmp (lastid, "clusterrank") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* local/global/none or "local/global/none" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected local, global or none at graph clusterrank option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotcolorscheme (struct usubg *subg)
{
  /* colorscheme=name */
  if (strcmp (lastid, "colorscheme") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* name or "name" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of color scheme at graph colorscheme option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotcomment (struct usubg *subg)
{
  /* is any string and format-dependent */
  if (strcmp (lastid, "comment") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph comment option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotcompound (struct usubg *subg)
{
  /* is default false and if true then allow edges between clusters */
  if (strcmp (lastid, "compound") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* 1/0 */
    }
  else
    {
      parser_error_string = "expected true or false at graph compound option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotconcentrate (struct usubg *subg)
{
  /* is default false and enables edge concentrators */
  if (strcmp (lastid, "concentrate") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph concentrate option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdamping (struct usubg *subg)
{
  /* Damping=1.0 fp-number */
  if (strcmp (lastid, "Damping") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph damping option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdefaultdist (struct usubg *subg)
{
  /* defaultdist=1.0 fp-number */
  if (strcmp (lastid, "defaultdist") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph defaultdist option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdim (struct usubg *subg)
{
  /* dim=1 int-number */
  if (strcmp (lastid, "dim") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph dim option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdiredgeconstraints (struct usubg *subg)
{
  /* diredgeconstraints=string */
  if (strcmp (lastid, "diredgeconstraints") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph diredgeconstraints option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdpi (struct usubg *subg)
{
  /* dpi=fp-number */
  if (strcmp (lastid, "dpi") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph dpi option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotepsilon (struct usubg *subg)
{
  /* epsilon=fp-number */
  if (strcmp (lastid, "epsilon") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph epsilon option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotesep (struct usubg *subg)
{
  /* esep=fp-number */
  if (strcmp (lastid, "esep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph esep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotdimen (struct usubg *subg)
{
  /* dimen=1 int-number */
  if (strcmp (lastid, "dimen") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph dimen option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfontcolor (struct usubg *subg)
{
  /* is default black and the type face color */
  if (strcmp (lastid, "fontcolor") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* color or "color" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of color at graph fontcolor option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfontname (struct usubg *subg)
{
  /* is default Times-Roman and the font family to use */
  if (strcmp (lastid, "fontname") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* name or "name" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of font at graph fontname option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfontnames (struct usubg *subg)
{
  /* font names to use */
  if (strcmp (lastid, "fontnames") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* names or "names" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of font at graph fontnames option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfontpath (struct usubg *subg)
{
  char *tmpfontpath = NULL;
  /* is a list of directories to search for fonts and should look like this
   * "/usr/X11R6/lib/X11/fonts/TrueType:/usr/X11R6/lib/X11/fonts/truetype:
   * /usr/X11R6/lib/X11/fonts/TTF:/usr/share/fonts/TrueType:
   * /usr/share/fonts/truetype:/usr/openwin/lib/X11/fonts/TrueType:
   * /usr/X11R6/lib/X11/fonts/Type1"
   * directories separated by a ':' DOT_COLON
   */
  if (strcmp (lastid, "fontpath") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* path or "path" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      tmpfontpath = laststring;
    }
  else if (token == DOT_ID)
    {
      /* list of directories to search for fonts */
      tmpfontpath = lastid;
    }
  else if (token == DOT_CHAR)
    {
      /* list of directories to search for fonts */
      tmpfontpath = lastchar;
    }
  else
    {
      parser_error_string = "expected list of directories to search for fonts at graph fontpath option";
      return (1);
    }
  /* depends on gtk+ if&how to handle this */
  if (strlen (tmpfontpath) == 0)
    {
      tmpfontpath = NULL;
      parser_error_string = "empty list of directories to search for fonts at graph fontpath option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfontsize (struct usubg *subg)
{
  char *tmpfontsize = NULL;
  /* is default 14 the point size of label text */
  if (strcmp (lastid, "fontsize") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      tmpfontsize = laststring;
    }
  else if (token == DOT_NUM)
    {
      tmpfontsize = lastnum;
    }
  else
    {
      parser_error_string = "expected number at graph fontsize option";
      return (1);
    }
  if (tmpfontsize)
    {
      if (atoi (tmpfontsize) == 0)	/* should be strtol() */
	{
	  tmpfontsize = NULL;
	}
      else if (strlen (tmpfontsize) == 0)
	{
	  tmpfontsize = NULL;
	}
      else
	{			/* assume oke */
	}
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotforcelabels (struct usubg *subg)
{
  /* true/false */
  if (strcmp (lastid, "forcelabels") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph forcelabels option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotgradientangle (struct usubg *subg)
{
  /* int number */
  if (strcmp (lastid, "gradientangle") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph gradientangle option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dothref (struct usubg *subg)
{
  /* href=url */
  if (strcmp (lastid, "href") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* url or "url" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected url at graph href option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotid (struct usubg *subg)
{
  /* id=string */
  if (strcmp (lastid, "id") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph id option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotimagepath (struct usubg *subg)
{
  /* imagepath=string */
  if (strcmp (lastid, "imagepath") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph imagepath option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotinputscale (struct usubg *subg)
{
  /* inputscale=fp-num */
  if (strcmp (lastid, "inputscale") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph inputscale option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotk (struct usubg *subg)
{
  /* K=fp-num */
  if (strcmp (lastchar, "K") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph K option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlabelloc (struct usubg *subg)
{
  /* is default top and ”t” or ”b” for top-justified and bottom-justified cluster label text */
  if (strcmp (lastid, "labelloc") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* t/b or "t/b" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_CHAR)
    {
      /* strcmp(lastchar,"t")==0 */
      /* */
    }
  else
    {
      parser_error_string = "expected ' t ' or ' b ' for top or bottom justified at graph labelloc option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlandscape (struct usubg *subg)
{
  /* true/false */
  if (strcmp (lastid, "landscape") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* strcmp(lastid,"true")==0 */
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph landscape option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlayerlistsep (struct usubg *subg)
{
  /* "comma-string" */
  if (strcmp (lastid, "layerlistsep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "comma-string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph layerlistsep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlayers (struct usubg *subg)
{
  /* is "id:id:id.." */
  if (strcmp (lastid, "layers") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "id:id:id.." */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected \"id:id:id...\" at graph layers option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlayerselect (struct usubg *subg)
{
  /* is "string" */
  if (strcmp (lastid, "layerselect") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected \"string\" at graph layerselect option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlayersep (struct usubg *subg)
{
  /* is "string" */
  if (strcmp (lastid, "layersep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected \"string\" at graph layersep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlayout (struct usubg *subg)
{
  /* is "string" */
  if (strcmp (lastid, "layout") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
    }
  else
    {
      parser_error_string = "expected \"string\" at graph layout option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlevels (struct usubg *subg)
{
  /* is int */
  if (strcmp (lastid, "levels") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int or "int" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph levels option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlevelsgap (struct usubg *subg)
{
  /* is fp-num */
  if (strcmp (lastid, "levelsgap") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph levelsgap option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlheight (struct usubg *subg)
{
  /* is fp-num */
  if (strcmp (lastid, "lheight") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph lheight option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlp (struct usubg *subg)
{
  /* lp=point as in "%f,%f(!)?" also 3d */
  if (strcmp (lastid, "lp") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "point" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected point at graph lp option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlwidth (struct usubg *subg)
{
  /* lwidth=fp-num */
  if (strcmp (lastid, "lwidth") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph lwidth option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmargin (struct usubg *subg)
{
  /* is default .5 the margin included in page inches */
  if (strcmp (lastid, "margin") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph margin option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmaxiter (struct usubg *subg)
{
  /* is int */
  if (strcmp (lastid, "maxiter") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int-number or "int-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph maxiter option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmclimit (struct usubg *subg)
{
  /* is default 1.0 and the scale factor for mincross iterations */
  if (strcmp (lastid, "mclimit") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph mclimit option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmindist (struct usubg *subg)
{
  /* is fp-num */
  if (strcmp (lastid, "mindist") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph mindist option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmode (struct usubg *subg)
{
  /* is string */
  if (strcmp (lastid, "mode") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph mode option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmodel (struct usubg *subg)
{
  /* is string */
  if (strcmp (lastid, "model") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph model option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotmosek (struct usubg *subg)
{
  /* is true/false */
  if (strcmp (lastid, "mosek") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph mosek option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnodesep (struct usubg *subg)
{
  /* is default .25 the separation between nodes in inches */
  if (strcmp (lastid, "nodesep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph nodesep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnormalize (struct usubg *subg)
{
  /* is fp-num */
  if (strcmp (lastid, "normalize") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph normalize option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnojustify (struct usubg *subg)
{
  /* is true/false */
  if (strcmp (lastid, "nojustify") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph nojustify option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnotranslate (struct usubg *subg)
{
  /* is true/false */
  if (strcmp (lastid, "notranslate") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph notranslate option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotoverlap_shrink (struct usubg *subg)
{
  /* is true/false */
  if (strcmp (lastid, "overlap_shrink") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph overlap_shrink option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotpack (struct usubg *subg)
{
  /* is true/false */
  if (strcmp (lastid, "pack") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph pack option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnslimit (struct usubg *subg)
{
  /* fp-num, if set to f then bounds network simplex iterations by (f)(number of nodes) when setting x-coordinates */
  if (strcmp (lastid, "nslimit") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph nslimit option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotnslimit1 (struct usubg *subg)
{
  /* fp-num, if set to f then bounds network simplex iterations by (f)(number of nodes) when ranking nodes */
  if (strcmp (lastid, "nslimit1") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph nslimit1 option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotoverlap_scaling (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "overlap_scaling") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph overlap_scaling option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotpad (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "pad") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph pad option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotordering (struct usubg *subg)
{
  /* string, if out then out edge order is preserved */
  if (strcmp (lastid, "ordering") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* out or "out" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph ordering option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotorientation (struct usubg *subg)
{
  /* string, is default portrait if rotate is not used and the value is landscape then draw landscape orientation */
  if (strcmp (lastid, "orientation") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* portrait/landscape or "portrait/landscape" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected portrait or landscape at graph orientation option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotoutputorder (struct usubg *subg)
{
  /* string, */
  if (strcmp (lastid, "outputorder") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph outputorder option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotpage (struct usubg *subg)
{
  /* is unit of pagination as in "8.5,11" */
  if (strcmp (lastid, "page") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* check for example 8.5,11 */
    }
  else
    {
      parser_error_string = "expected number at graph page option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotpagedir (struct usubg *subg)
{
  /* is string, BL, BR etc the traversal order of pages */
  if (strcmp (lastid, "pagedir") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* dir or "dir" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected direction at graph pagedir option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotquadtree (struct usubg *subg)
{
  /* is string, normal, fast */
  if (strcmp (lastid, "quadtree") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* dir or "dir" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected direction at graph quadtree option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotquantum (struct usubg *subg)
{
  /* if quantum 0.0 then node label dimensions will be rounded to integral multiples of quantum */
  if (strcmp (lastid, "quantum") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph quantum option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}


/* */
static int
is_a_dotranksep (struct usubg *subg)
{
  /* fp-num, is default .75 and the separation between ranks in inches */
  if (strcmp (lastid, "ranksep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num or "fp-num" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph ranksep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotratio (struct usubg *subg)
{
  /* fp-num, is the approximate aspect ratio desired specified as fill or auto */
  if (strcmp (lastid, "ratio") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fill/auto or "fill/auto" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number or fill or auto at graph ratio option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotrepulsiveforce (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "repulsiveforce") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph repulsiveforce option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotresolution (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "resolution") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-num */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph resolution option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotremincross (struct usubg *subg)
{
  /* if set to true and there are multiple clusters re-run crossing minimization */
  if (strcmp (lastid, "remincross") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph remincross option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsplines (struct usubg *subg)
{
  /* true/false */
  if (strcmp (lastid, "splines") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph splines option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dottruecolor (struct usubg *subg)
{
  /* true/false */
  if (strcmp (lastid, "truecolor") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph truecolor option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotrotate (struct usubg *subg)
{
  /* int, the drawing by degrees and if set to 90 then the orientation is landscape */
  if (strcmp (lastid, "rotate") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int-number or "int-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph rotate option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotrotation (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "rotation") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph rotation option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotscale (struct usubg *subg)
{
  /* fp-num */
  if (strcmp (lastid, "scale") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph scale option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsep (struct usubg *subg)
{
  /* fp-num or pointf */
  if (strcmp (lastid, "sep") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph sep option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsamplepoints (struct usubg *subg)
{
  /* is default 8 and the number of points used to represent ellipses and circles on output drawing */
  if (strcmp (lastid, "samplepoints") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      parser_error_string = "expected = at graph samplepoints option";
      return (1);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph samplepoints option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsearchsize (struct usubg *subg)
{
  /* int, is default 30 and the maximum number of edges with negative cut values to check when looking for a minimum one during network simplex */
  if (strcmp (lastid, "searchsize") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int-number or "int-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph searchsize option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotshowboxes (struct usubg *subg)
{
  /* int, */
  if (strcmp (lastid, "showboxes") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int-number or "int-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph showboxes option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsortv (struct usubg *subg)
{
  /* int, */
  if (strcmp (lastid, "sortv") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int-number or "int-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph sortv option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsize (struct usubg *subg)
{
  /* fp-num, is the maximum drawing size in inches */
  if (strcmp (lastid, "size") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph size option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotvoro_margin (struct usubg *subg)
{
  /* fp-num, */
  if (strcmp (lastid, "voro_margin") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* fp-number or "fp-number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph voro_margin option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_doturl (struct usubg *subg)
{
  /* escstring, is the URL associated with graph and format-dependent */
  if (strcmp (lastid, "url") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* url or "url" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* check url */
    }
  else if (token == DOT_ID)
    {
      /* check url */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected url string at graph url option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotviewport (struct usubg *subg)
{
  /* string, "%lf,%lf,%lf,%lf,%lf" */
  if (strcmp (lastid, "viewport") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* check numbers */
    }
  else if (token == DOT_ID)
    {
      /* check numbers */
    }
  else
    {
      parser_error_string = "expected string at graph viewport option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotoverlap (struct usubg *subg)
{
  if (strcmp (lastid, "overlap") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* true/false or "true/false" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected true or false at graph overlap option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotpackmode (struct usubg *subg)
{
  if (strcmp (lastid, "packmode") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph packmode option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotcolor (struct usubg *subg)
{
  if (strcmp (lastid, "color") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* colorname "colorname" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of color at graph color option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotfillcolor (struct usubg *subg)
{
  if (strcmp (lastid, "fillcolor") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* colorname or "colorname" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected name of color at graph fillcolor option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlabel (struct usubg *subg)
{
  char *label = NULL;
  int labelset = 0;
  if (strcmp (lastid, "label") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* labelname or "labelname" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      label = laststring;
      labelset = 1;
    }
  else if (token == DOT_ID)
    {
      label = lastid;
      labelset = 2;
    }
  else if (token == DOT_NUM)
    {
      label = lastnum;
      labelset = 3;
    }
  else if (token == DOT_HTML)
    {
      label = lasthtml;
      labelset = 4;
    }
  else
    {
      parser_error_string = "expected string at graph label option";
      return (1);
    }
  if (label)
    {
      /* check for "" */
      if (strlen (label) == 0)
	{
	  labelset = 0;
	}
    }
  if (subg)
    {
      /* set as label for the folded subgraph in summary node */
      switch (labelset)
	{
	case 0:		/* not set */
	  subg->summaryn->label = NULL;
	  break;
	case 1:		/* string "" */
	  subg->summaryn->label = is_a_dot_escstring_node (label, subg->summaryn);
	  if (subg->summaryn->label == NULL)
	    {			/* parse error */
	      return (1);
	    }
	  break;
	case 2:		/* id */
	  subg->summaryn->label = label;
	  break;
	case 3:		/* number */
	  subg->summaryn->label = label;
	  break;
	case 4:		/* html */
	  subg->summaryn->label = is_a_dot_htmlstring_node (label, subg->summaryn);
	  break;
	default:		/* shouldnothappen */
	  subg->summaryn->label = NULL;
	  break;
	}
      subg->label = subg->summaryn->label;
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlabel_scheme (struct usubg *subg)
{
  if (strcmp (lastid, "label_scheme") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* int or "int" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph label_scheme option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotlabeljust (struct usubg *subg)
{
  if (strcmp (lastid, "labeljust") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* l/r/c "l/r/c" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_CHAR)
    {
      /* */
      /* if (strcmp(lastchar,"l")==0) */
    }
  else if (token == DOT_ID)
    {
      /* if strcmp("l",lastid)==0)
       * if (strcmp("r",lastid)==0)
       */
    }
  else
    {
      parser_error_string = "expected ' c ', ' l ' or ' r ' at graph labeljust option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotstyle (struct usubg *subg)
{
  /* example: style=bold; */
  if (strcmp (lastid, "style") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* style or "style" comma sep list */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected style string at graph style option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dottarget (struct usubg *subg)
{
  /* escstring */
  if (strcmp (lastid, "target") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph target option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotxdotversion (struct usubg *subg)
{
  /* string */
  if (strcmp (lastid, "xdotversion") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" example 1.2 or "1.2" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_NUM)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected number at graph xdotversion option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotstylesheet (struct usubg *subg)
{
  /* string */
  if (strcmp (lastid, "stylesheet") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph stylesheet option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* pen width double */
static int
is_a_dotpenwidth (struct usubg *subg)
{
  char *penwstr = NULL;
  int thickness = 0;
  double thicknessd = 0;
  if (strcmp (lastid, "penwidth") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* number or "number" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      penwstr = laststring;
    }
  else if (token == DOT_NUM)
    {
      penwstr = lastnum;
    }
  else
    {
      parser_error_string = "expected number at graph penwidth option";
      return (1);
    }
  if (penwstr == NULL)
    {
      parser_error_string = "expected number at graph penwidth option (nil string)";
      return (1);
    }
  /* check for "" */
  if (strlen (penwstr) == 0)
    {
      thickness = 0;		/* default */
    }
  else
    {
      /* can add extra check on chars left */
      thicknessd = strtod (penwstr, NULL);
      if (errno)
	{
	  parser_error_string = "expected number at graph penwidth option (error string)";
	  return (1);
	}
      thicknessd = round (thicknessd);
      thickness = (int) thicknessd;
      /* limiter, see main.h */
      if (thickness < 0)
	{
	  thickness = 0;
	}
      if (thickness > 15)
	{
	  thickness = 15;
	}
    }
  /* fixme */
  if (thickness)
    {
    }
  if (subg)
    {
    }
  else
    {
    }
  return (1);
}

/* pen color */
static int
is_a_dotpencolor (struct usubg *subg)
{
  if (strcmp (lastid, "pencolor") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph pencolor option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotrankdir (struct usubg *subg)
{
  if (strcmp (lastid, "rankdir") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* direction or "direction" LR/TB */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected LR or TB at graph rankdir option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotrank (struct usubg *subg)
{
  if (strcmp (lastid, "rank") != 0)
    {
      return (0);
    }

  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" same rank=same */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph rank option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotroot (struct usubg *subg)
{
  if (strcmp (lastid, "root") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph root option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotsmoothing (struct usubg *subg)
{
  if (strcmp (lastid, "smoothing") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph smoothing option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* */
static int
is_a_dotstart (struct usubg *subg)
{
  if (strcmp (lastid, "start") != 0)
    {
      return (0);
    }
  /* = */
  token = dot_lex ();
  if (token != DOT_IS)
    {
      dot_unlex ();
      return (0);
    }
  /* string or "string" */
  token = dot_lex ();
  if (token == DOT_STRING)
    {
      /* */
    }
  else if (token == DOT_ID)
    {
      /* */
    }
  else
    {
      parser_error_string = "expected string at graph start option";
      return (1);
    }
  if (subg)
    {
      /* */
    }
  else
    {
      /* */
    }
  return (1);
}

/* end */
