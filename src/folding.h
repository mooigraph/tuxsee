
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef FOLDING_H
#define FOLDING_H 1

/* optional extra level spreading vertical (0/1) */
extern int levelextra;

/* */
extern int settings_init_folded;

/* */
extern int maxlevel;

/* */
extern int nlevels;

/* */
extern int dfsstartlevel;

/* */
extern void folding_clear (void);

/* */
extern void folding (void);

/* */
extern void markselfedges (void);

/* */
extern void marksinglenodes (void);

/* */
extern int nodesinlevels (void);

/* */
extern void longedges (void);

/* */
extern void folding_all (int status);

/* */
extern void folding_build_wl (void);

#endif

/* end */
