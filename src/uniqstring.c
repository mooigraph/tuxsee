
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 * Copyright tuxsee authors
 */

/* for every (char *)"string" is a single strdup()'ed copy used at multiple places to reduce memory usage. */

#include <stdio.h>
#include <string.h>

#include "splay-tree.h"
#include "main.h"
#include "mem.h"
#include "uniqstring.h"

/* database with the strings */
static splay_tree gustring = (splay_tree) 0;

/* clear all data */
void uniqstring_clear(void)
{
	gustring = splay_tree_delete(gustring);
	return;
}

/* like strdup() but return uniq copy of string and re-use earlier entry's */
char *uniqstring(char *s)
{
	char *buf = NULL;
	splay_tree_node spn = (splay_tree_node) 0;

	if (s == NULL) {
		return ((char *)0);
	}

	if (strlen(s) == 0) {
		return ((char *)"");
	}

	if (gustring == NULL) {
		/* indexed on (char *) */
		gustring = splay_tree_new(splay_tree_compare_strings,	/* splay_tree_compare_fn */
					  splay_tree_free_key,	/* splay_tree_delete_key_fn */
					  (splay_tree_delete_value_fn) 0);
	}

	/* check if in database */
	spn = splay_tree_lookup((splay_tree) gustring, (splay_tree_key) s);

	/* found ilona */
	if (spn) {
		return ((char *)spn->key);
	}

	/* create zeroed buffer for fresh copy, +1 for terminating \0 */
	buf = mymalloc((strlen(s) + 1), __FUNCTION__, __LINE__);

	/* copy (dest, src, n); do not use strcpy() anymore, is a safety bug. */
	(void)memmove((void *)buf, (const void *)s, (size_t)strlen(s));

	/* add to database */
	splay_tree_insert(gustring,	/* splay_tree */
			  (splay_tree_key) buf,	/* index */
			  (splay_tree_value) 0	/* unused */
	    );

	return ((char *)buf);
}

/* end */
