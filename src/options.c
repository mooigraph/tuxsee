
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 * Copyright tuxsee authors
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "splay-tree.h"
#include "main.h"
#include "lmain.h"
#include "options.h"

/* draw splines if set --splines yes/no -s yes/no */
int option_splines = 1;

/* do not draw single nodes if set --singlenodes yes/no -1 yes/no */
int option_no_singlenodes = 0;

/* draw self edges if set --selfedges yes/no -S yes/no */
int option_selfedges = 0;

/* optional graph filename at commandline to start with */
char *option_filename = NULL;

/* optional output image filename at commandline and run as console tool */
char *option_outfilename = NULL;

/* if set force all subgraphs folded initially --init-folded yes/no -f yes/no */
int option_init_folded = 0;

/* if set force all subgraphs folded initially --init-unfolded yes/no -F yes/no */
int option_init_unfolded = 0;

/* if set force to use the node name as node label in drawing --nodenames yes/no -n yes/no */
int option_nodenames = 0;

/* if set do not include edgelabels in the graph --edgelabels yes/no -l yes/no */
int option_no_edgelabels = 0;

/* draw multiple arrows at long edges --multi-arrows yes/no -a yes/no */
int option_multi_arrows = 1;

/* if set draw subgraph area rectangle --subgraph-area yes/no -g yes/no */
int option_subgraph_area = 1;	/* 1 is on */

/* if set toggle altcolor bit in altcolor drawing --toggle-altcolor yes/no -T yes/no */
int option_toggle_altcolor = 0;

/* if set print memory usage --debug 0/1/2 or -D 0/1/2 */
/* int option_memlog = 0; moved to mem.c */

/* if set run memory tracker and usage */
/* int option_memtrack = 1; moved to mem.c */

/* if set print layouter debug --debug 0/1/2 or -D 0/1/2 */
int option_ldebug = 0;

/* if set placement nodes debug */
int option_placedebug = 0;

/* if set print gui debug --debug 0/1/2 or -D 0/1/2 */
int option_gdebug = 0;

/* if set enable testing code --testcode yes/no -t yes/no */
int option_testcode = 0;

/* if set print at start a html page with the color database --colorpage -C */
int option_colorpage = 0;

/* if set print parser debug output */
int option_parsedebug = 0;

/* if set dryrun mode for testing */
int option_dryrun = 0;

/* if set the layout algorithm to start with */
char *option_algo = NULL;

/* if set the layout pre-algorithm to start with */
char *option_prealgo = NULL;

/* follow edge in prio algo, 1/0 */
int option_feprio = 0;

/* down() 1 or up() 0 as last in prio algo */
int option_pud = 0;

/* xy-spread factor 5..50 */
int option_xyspread = 25;

/* Declarations for getopt.
   Copyright (C) 1989-1994,1996-1999,2001,2003,2004,2005,2006
   Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */
#ifndef _GETOPT_H
#ifndef __need_getopt
#define _GETOPT_H 1
#endif
#define __GETOPT_PREFIX my
/* Standalone applications should #define __GETOPT_PREFIX to an
   identifier that prefixes the external functions and variables
   defined in this header.  When this happens, include the
   headers that might declare getopt so that they will not cause
   confusion if included after this file.  Then systematically rename
   identifiers so that they do not collide with the system functions
   and variables.  Renaming avoids problems with some compilers and
   linkers.  */
#if defined __GETOPT_PREFIX && !defined __need_getopt
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#undef __need_getopt
#undef getopt
#undef getopt_long
#undef getopt_long_only
#undef optarg
#undef opterr
#undef optind
#undef optopt
#define __GETOPT_CONCAT(x, y) x ## y
#define __GETOPT_XCONCAT(x, y) __GETOPT_CONCAT (x, y)
#define __GETOPT_ID(y) __GETOPT_XCONCAT (__GETOPT_PREFIX, y)
#define getopt __GETOPT_ID (getopt)
#define getopt_long __GETOPT_ID (getopt_long)
#define getopt_long_only __GETOPT_ID (getopt_long_only)
#define optarg __GETOPT_ID (optarg)
#define opterr __GETOPT_ID (opterr)
#define optind __GETOPT_ID (optind)
#define optopt __GETOPT_ID (optopt)
#endif
/* Standalone applications get correct prototypes for getopt_long and
   getopt_long_only; they declare "char **argv".  libc uses prototypes
   with "char *const *argv" that are incorrect because getopt_long and
   getopt_long_only can permute argv; this is required for backward
   compatibility (e.g., for LSB 2.0.1).

   This used to be `#if defined __GETOPT_PREFIX && !defined __need_getopt',
   but it caused redefinition warnings if both unistd.h and getopt.h were
   included, since unistd.h includes getopt.h having previously defined
   __need_getopt.

   The only place where __getopt_argv_const is used is in definitions
   of getopt_long and getopt_long_only below, but these are visible
   only if __need_getopt is not defined, so it is quite safe to rewrite
   the conditional as follows:
*/
#if !defined __need_getopt
#if defined __GETOPT_PREFIX
#define __getopt_argv_const	/* empty */
#else
#define __getopt_argv_const const
#endif
#endif
/* If __GNU_LIBRARY__ is not already defined, either we are being used
   standalone, or this is the first header included in the source file.
   If we are being used with glibc, we need to include <features.h>, but
   that does not exist if we are standalone.  So: if __GNU_LIBRARY__ is
   not defined, include <ctype.h>, which will pull in <features.h> for us
   if it's from glibc.  (Why ctype.h?  It's guaranteed to exist and it
   doesn't flood the namespace with stuff the way some other headers do.)  */
#if !defined __GNU_LIBRARY__
#include <ctype.h>
#endif
#ifndef __THROW
#ifndef __GNUC_PREREQ
#define __GNUC_PREREQ(maj, min) (0)
#endif
#if defined __cplusplus && __GNUC_PREREQ (2,8)
#define __THROW	throw ()
#else
#define __THROW
#endif
#endif
#ifdef	__cplusplus
extern "C" {
#endif

/* For communication from `getopt' to the caller.
   When `getopt' finds an option that takes an argument,
   the argument value is returned here.
   Also, when `ordering' is RETURN_IN_ORDER,
   each non-option ARGV-element is returned here.  */

/* extern char *optarg; */

/* Index in ARGV of the next element to be scanned.
   This is used for communication to and from the caller
   and for communication between successive calls to `getopt'.

   On entry to `getopt', zero means this is the first call; initialize.

   When `getopt' returns -1, this is the index of the first of the
   non-option elements that the caller should itself scan.

   Otherwise, `optind' communicates from one call to the next
   how much of ARGV has been scanned so far.  */

/* extern int optind; */

/* Callers store zero here to inhibit the error message `getopt' prints
   for unrecognized options.  */

/* extern int opterr; */

/* Set to an option character which was unrecognized.  */

/* extern int optopt; */

#ifndef __need_getopt
/* Describe the long-named options requested by the application.
   The LONG_OPTIONS argument to getopt_long or getopt_long_only is a vector
   of `struct option' terminated by an element containing a name which is
   zero.

   The field `has_arg' is:
   no_argument		(or 0) if the option does not take an argument,
   required_argument	(or 1) if the option requires an argument,
   optional_argument	(or 2) if the option takes an optional argument.

   If the field `flag' is not NULL, it points to a variable that is set
   to the value given in the field `val' when the option is found, but
   left unchanged if the option is not found.

   To have a long-named option do something other than set an `int' to
   a compiled-in constant, such as set a value from `optarg', set the
   option's `flag' field to zero and its `val' field to a nonzero
   value (the equivalent single-letter option character, if there is
   one).  For long options that have a zero `flag' field, `getopt'
   returns the contents of the `val' field.  */

	struct option {
		const char *name;
		/* has_arg can't be an enum because some compilers complain about
		   type mismatches in all the code that assumes it is an int.  */
		int has_arg;
		int *flag;
		int val;
	};
/* Names for the values of the `has_arg' field of `struct option'.  */
#define no_argument		0
#define required_argument	1
#define optional_argument	2
#endif				/* need getopt */
/* Get definitions and prototypes for functions to process the
   arguments in ARGV (ARGC of them, minus the program name) for
   options given in OPTS.

   Return the option character from OPTS just read.  Return -1 when
   there are no more options.  For unrecognized options, or options
   missing arguments, `optopt' is set to the option letter, and '?' is
   returned.

   The OPTS string is a list of characters which are recognized option
   letters, optionally followed by colons, specifying that that letter
   takes an argument, to be placed in `optarg'.

   If a letter in OPTS is followed by two colons, its argument is
   optional.  This behavior is specific to the GNU `getopt'.

   The argument `--' causes premature termination of argument
   scanning, explicitly telling `getopt' that there are no more
   options.

   If OPTS begins with `-', then non-option arguments are treated as
   arguments to the option '\1'.  This behavior is specific to the GNU
   `getopt'.  If OPTS begins with `+', or POSIXLY_CORRECT is set in
   the environment, then do not permute arguments.  */
	static int getopt(int ___argc, char *const *___argv, const char *__shortopts);	/* __THROW; */
#ifndef __need_getopt
	static int getopt_long(int ___argc, char *__getopt_argv_const * ___argv, const char *__shortopts,
			       const struct option *__longopts, int *__longind);
	/* __THROW; */
	static int getopt_long_only(int ___argc, char *__getopt_argv_const * ___argv, const char *__shortopts, const struct option *__longopts, int *__longind);	/* __THROW; */
#endif
#ifdef	__cplusplus
}
#endif
/* Make sure we later can get all the definitions and declarations.  */
#undef __need_getopt
#endif				/* getopt.h */
/* Internal declarations for getopt.
   Copyright (C) 1989-1994,1996-1999,2001,2003,2004
   Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */
#ifndef _GETOPT_INT_H
#define _GETOPT_INT_H	1
static int _getopt_internal(int ___argc, char **___argv,			    const char *__shortopts, const struct option *__longopts, int *__longind, int __long_only,
			    int __posixly_correct);
/* Reentrant versions which can handle parsing multiple argument
   vectors at the same time.  */
/* Data type for reentrant functions.  */
struct _getopt_data {
	/* These have exactly the same meaning as the corresponding global
	   variables, except that they are used for the reentrant
	   versions of getopt.  */
	int optind;
	int opterr;
	int optopt;
	char *optarg;
	/* Internal members.  */
	/* True if the internal members have been initialized.  */
	int __initialized;
	/* The next char to be scanned in the option-element
	   in which the last option character we returned was found.
	   This allows us to pick up the scan where we left off.

	   If this is zero, or a null string, it means resume the scan
	   by advancing to the next ARGV-element.  */
	char *__nextchar;
	/* Describe how to deal with options that follow non-option ARGV-elements.

	   If the caller did not specify anything,
	   the default is REQUIRE_ORDER if the environment variable
	   POSIXLY_CORRECT is defined, PERMUTE otherwise.

	   REQUIRE_ORDER means don't recognize them as options;
	   stop option processing when the first non-option is seen.
	   This is what Unix does.
	   This mode of operation is selected by either setting the environment
	   variable POSIXLY_CORRECT, or using `+' as the first character
	   of the list of option characters, or by calling getopt.

	   PERMUTE is the default.  We permute the contents of ARGV as we
	   scan, so that eventually all the non-options are at the end.
	   This allows options to be given in any order, even with programs
	   that were not written to expect this.

	   RETURN_IN_ORDER is an option available to programs that were
	   written to expect options and other ARGV-elements in any order
	   and that care about the ordering of the two.  We describe each
	   non-option ARGV-element as if it were the argument of an option
	   with character code 1.  Using `-' as the first character of the
	   list of option characters selects this mode of operation.

	   The special argument `--' forces an end of option-scanning regardless
	   of the value of `ordering'.  In the case of RETURN_IN_ORDER, only
	   `--' can cause `getopt' to return -1 with `optind' != ARGC.  */
	enum {
		REQUIRE_ORDER, PERMUTE, RETURN_IN_ORDER
	} __ordering;
	/* If the POSIXLY_CORRECT environment variable is set
	   or getopt was called.  */
	int __posixly_correct;
	/* Handle permutation of arguments.  */
	/* Describe the part of ARGV that contains non-options that have
	   been skipped.  `first_nonopt' is the index in ARGV of the first
	   of them; `last_nonopt' is the index after the last of them.  */
	int __first_nonopt;
	int __last_nonopt;
#if defined _LIBC && defined USE_NONOPTION_FLAGS
	int __nonoption_flags_max_len;
	int __nonoption_flags_len;
#endif
};

/* The initializer is necessary to set OPTIND and OPTERR to their
   default values and to clear the initialization flag.  */
#define _GETOPT_DATA_INITIALIZER	{ 1, 1 }
static int _getopt_internal_r(int ___argc, char **___argv,
			      const char *__shortopts,
			      const struct option *__longopts, int *__longind, int __long_only, int __posixly_correct,
			      struct _getopt_data *__data);
static int _getopt_long_r(int ___argc, char **___argv, const char *__shortopts, const struct option *__longopts, int *__longind,
			  struct _getopt_data *__data);
static int _getopt_long_only_r(int ___argc, char **___argv, const char *__shortopts, const struct option *__longopts,
			       int *__longind, struct _getopt_data *__data);
#endif /* getopt_int.h */
/* Getopt for GNU.
   NOTE: getopt is now part of the C library, so if you don't know what
   "Keep this file name-space clean" means, talk to drepper@gnu.org
   before changing it!
   Copyright (C) 1987,88,89,90,91,92,93,94,95,96,98,99,2000,2001,2002,2003,2004,2006
	Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */
#undef _LIBC
#undef __VMS
#undef USE_IN_LIBIO
#undef TEST
#ifndef _LIBC
#include <config.h>
#endif
/*
#include "getopt.h"
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef __VMS
#include <unixlib.h>
#endif
/*
#ifdef _LIBC
# include <libintl.h>
#else
# include "gettext.h"
# define _(msgid) gettext (msgid)
#endif
*/
#define _(msgid) (msgid)
#if defined _LIBC && defined USE_IN_LIBIO
#include <wchar.h>
#endif
#ifndef attribute_hidden
#define attribute_hidden
#endif
/* Unlike standard Unix `getopt', functions like `getopt_long'
   let the user intersperse the options with the other arguments.

   As `getopt_long' works, it permutes the elements of ARGV so that,
   when it is done, all the options precede everything else.  Thus
   all application programs are extended to handle flexible argument order.

   Using `getopt' or setting the environment variable POSIXLY_CORRECT
   disables permutation.
   Then the application's behavior is completely standard.

   GNU application programs can use a third alternative mode in which
   they can distinguish the relative order of options and other arguments.  */
/*
#include "getopt_int.h"
*/
/* For communication from `getopt' to the caller.
   When `getopt' finds an option that takes an argument,
   the argument value is returned here.
   Also, when `ordering' is RETURN_IN_ORDER,
   each non-option ARGV-element is returned here.  */
static char *optarg;
/* Index in ARGV of the next element to be scanned.
   This is used for communication to and from the caller
   and for communication between successive calls to `getopt'.

   On entry to `getopt', zero means this is the first call; initialize.

   When `getopt' returns -1, this is the index of the first of the
   non-option elements that the caller should itself scan.

   Otherwise, `optind' communicates from one call to the next
   how much of ARGV has been scanned so far.  */
/* 1003.2 says this must be 1 before any call.  */
static int optind = 1;
/* Callers store zero here to inhibit the error message
   for unrecognized options.  */
static int opterr = 1;
/* Set to an option character which was unrecognized.
   This must be initialized on some systems to avoid linking in the
   system's own getopt implementation.  */
static int optopt = '?';
/* Keep a global copy of all internal members of getopt_data.  */
static struct _getopt_data getopt_data;
#if defined HAVE_DECL_GETENV && !HAVE_DECL_GETENV
extern char *getenv();
#endif
#ifdef _LIBC
/* Stored original parameters.
   XXX This is no good solution.  We should rather copy the args so
   that we can compare them later.  But we must not use malloc(3).  */
extern int __libc_argc;
extern char **__libc_argv;
/* Bash 2.0 gives us an environment variable containing flags
   indicating ARGV elements that should not be considered arguments.  */
#ifdef USE_NONOPTION_FLAGS
/* Defined in getopt_init.c  */
extern char *__getopt_nonoption_flags;
#endif
#ifdef USE_NONOPTION_FLAGS
#define SWAP_FLAGS(ch1, ch2) \
  if (d->__nonoption_flags_len > 0)					      \
    {									      \
      char __tmp = __getopt_nonoption_flags[ch1];			      \
      __getopt_nonoption_flags[ch1] = __getopt_nonoption_flags[ch2];	      \
      __getopt_nonoption_flags[ch2] = __tmp;				      \
    }
#else
#define SWAP_FLAGS(ch1, ch2)
#endif
#else /* !_LIBC */
#define SWAP_FLAGS(ch1, ch2)
#endif /* _LIBC */
/* Exchange two adjacent subsequences of ARGV.
   One subsequence is elements [first_nonopt,last_nonopt)
   which contains all the non-options that have been skipped so far.
   The other is elements [last_nonopt,optind), which contains all
   the options processed since those non-options were skipped.

   `first_nonopt' and `last_nonopt' are relocated so that they describe
   the new indices of the non-options in ARGV after they are moved.  */
static void exchange(char **argv, struct _getopt_data *d)
{
	int bottom = d->__first_nonopt;
	int middle = d->__last_nonopt;
	int top = d->optind;
	char *tem;
	/* Exchange the shorter segment with the far end of the longer segment.
	   That puts the shorter segment into the right place.
	   It leaves the longer segment in the right place overall,
	   but it consists of two parts that need to be swapped next.  */
#if defined _LIBC && defined USE_NONOPTION_FLAGS
	/* First make sure the handling of the `__getopt_nonoption_flags'
	   string can work normally.  Our top argument must be in the range
	   of the string.  */
	if (d->__nonoption_flags_len > 0 && top >= d->__nonoption_flags_max_len) {
		/* We must extend the array.  The user plays games with us and
		   presents new arguments.  */
		char *new_str = malloc(top + 1);
		if (new_str == NULL)
			d->__nonoption_flags_len = d->__nonoption_flags_max_len = 0;
		else {
			memset(__mempcpy(new_str, __getopt_nonoption_flags, d->__nonoption_flags_max_len), '\0',
			       top + 1 - d->__nonoption_flags_max_len);
			d->__nonoption_flags_max_len = top + 1;
			__getopt_nonoption_flags = new_str;
		}
	}
#endif

	while (top > middle && middle > bottom) {
		if (top - middle > middle - bottom) {
			/* Bottom segment is the short one.  */
			int len = middle - bottom;
			register int i;
			/* Swap it with the top part of the top segment.  */
			for (i = 0; i < len; i++) {
				tem = argv[bottom + i];
				argv[bottom + i] = argv[top - (middle - bottom) + i];
				argv[top - (middle - bottom) + i] = tem;
				SWAP_FLAGS(bottom + i, top - (middle - bottom) + i);
			}
			/* Exclude the moved bottom segment from further swapping.  */
			top -= len;
		} else {
			/* Top segment is the short one.  */
			int len = top - middle;
			register int i;
			/* Swap it with the bottom part of the bottom segment.  */
			for (i = 0; i < len; i++) {
				tem = argv[bottom + i];
				argv[bottom + i] = argv[middle + i];
				argv[middle + i] = tem;
				SWAP_FLAGS(bottom + i, middle + i);
			}
			/* Exclude the moved top segment from further swapping.  */
			bottom += len;
		}
	}

	/* Update records for the slots the non-options now occupy.  */

	d->__first_nonopt += (d->optind - d->__last_nonopt);
	d->__last_nonopt = d->optind;
}

/* Initialize the internal data when the first call is made.  */

static const char *_getopt_initialize(int argc, char **argv, const char *optstring, int posixly_correct, struct _getopt_data *d)
{
	/* argc argv unused */
	if (argc) {
	}
	if (argv) {
	}
	/* Start processing options with ARGV-element 1 (since ARGV-element 0
	   is the program name); the sequence of previously skipped
	   non-option ARGV-elements is empty.  */

	d->__first_nonopt = d->__last_nonopt = d->optind;
	d->__nextchar = NULL;
	d->__posixly_correct = posixly_correct || !!getenv("POSIXLY_CORRECT");
	/* Determine how to handle the ordering of options and nonoptions.  */
	if (optstring[0] == '-') {
		d->__ordering = RETURN_IN_ORDER;
		++optstring;
	} else if (optstring[0] == '+') {
		d->__ordering = REQUIRE_ORDER;
		++optstring;
	} else if (d->__posixly_correct)
		d->__ordering = REQUIRE_ORDER;
	else
		d->__ordering = PERMUTE;
#if defined _LIBC && defined USE_NONOPTION_FLAGS
	if (!d->__posixly_correct && argc == __libc_argc && argv == __libc_argv) {
		if (d->__nonoption_flags_max_len == 0) {
			if (__getopt_nonoption_flags == NULL || __getopt_nonoption_flags[0] == '\0')
				d->__nonoption_flags_max_len = -1;
			else {
				const char *orig_str = __getopt_nonoption_flags;
				int len = d->__nonoption_flags_max_len = strlen(orig_str);
				if (d->__nonoption_flags_max_len < argc)
					d->__nonoption_flags_max_len = argc;
				__getopt_nonoption_flags = (char *)malloc(d->__nonoption_flags_max_len);
				if (__getopt_nonoption_flags == NULL)
					d->__nonoption_flags_max_len = -1;
				else
					memset(__mempcpy(__getopt_nonoption_flags, orig_str, len), '\0',
					       d->__nonoption_flags_max_len - len);
			}
		}
		d->__nonoption_flags_len = d->__nonoption_flags_max_len;
	} else
		d->__nonoption_flags_len = 0;
#endif
	return optstring;
}

/* Scan elements of ARGV (whose length is ARGC) for option characters
   given in OPTSTRING.

   If an element of ARGV starts with '-', and is not exactly "-" or "--",
   then it is an option element.  The characters of this element
   (aside from the initial '-') are option characters.  If `getopt'
   is called repeatedly, it returns successively each of the option characters
   from each of the option elements.

   If `getopt' finds another option character, it returns that character,
   updating `optind' and `nextchar' so that the next call to `getopt' can
   resume the scan with the following option character or ARGV-element.

   If there are no more option characters, `getopt' returns -1.
   Then `optind' is the index in ARGV of the first ARGV-element
   that is not an option.  (The ARGV-elements have been permuted
   so that those that are not options now come last.)

   OPTSTRING is a string containing the legitimate option characters.
   If an option character is seen that is not listed in OPTSTRING,
   return '?' after printing an error message.  If you set `opterr' to
   zero, the error message is suppressed but we still return '?'.

   If a char in OPTSTRING is followed by a colon, that means it wants an arg,
   so the following text in the same ARGV-element, or the text of the following
   ARGV-element, is returned in `optarg'.  Two colons mean an option that
   wants an optional arg; if there is text in the current ARGV-element,
   it is returned in `optarg', otherwise `optarg' is set to zero.

   If OPTSTRING starts with `-' or `+', it requests different methods of
   handling the non-option ARGV-elements.
   See the comments about RETURN_IN_ORDER and REQUIRE_ORDER, above.

   Long-named options begin with `--' instead of `-'.
   Their names may be abbreviated as long as the abbreviation is unique
   or is an exact match for some defined option.  If they have an
   argument, it follows the option name in the same ARGV-element, separated
   from the option name by a `=', or else the in next ARGV-element.
   When `getopt' finds a long-named option, it returns 0 if that option's
   `flag' field is nonzero, the value of the option's `val' field
   if the `flag' field is zero.

   LONGOPTS is a vector of `struct option' terminated by an
   element containing a name which is zero.

   LONGIND returns the index in LONGOPT of the long-named option found.
   It is only valid when a long-named option has been found by the most
   recent call.

   If LONG_ONLY is nonzero, '-' as well as '--' can introduce
   long-named options.

   If POSIXLY_CORRECT is nonzero, behave as if the POSIXLY_CORRECT
   environment variable were set.  */

static int
_getopt_internal_r(int argc, char **argv, const char *optstring,
		   const struct option *longopts, int *longind, int long_only, int posixly_correct, struct _getopt_data *d)
{
	int print_errors = d->opterr;
	if (optstring[0] == ':')
		print_errors = 0;
	if (argc < 1)
		return -1;
	d->optarg = NULL;
	if (d->optind == 0 || !d->__initialized) {
		if (d->optind == 0)
			d->optind = 1;	/* Don't scan ARGV[0], the program name.  */
		optstring = _getopt_initialize(argc, argv, optstring, posixly_correct, d);
		d->__initialized = 1;
	}

	/* Test whether ARGV[optind] points to a non-option argument.
	   Either it does not have option syntax, or there is an environment flag
	   from the shell indicating it is not an option.  The later information
	   is only used when the used in the GNU libc.  */
#if defined _LIBC && defined USE_NONOPTION_FLAGS
#define NONOPTION_P (argv[d->optind][0] != '-' || argv[d->optind][1] == '\0' \
		      || (d->optind < d->__nonoption_flags_len		      \
			  && __getopt_nonoption_flags[d->optind] == '1'))
#else
#define NONOPTION_P (argv[d->optind][0] != '-' || argv[d->optind][1] == '\0')
#endif

	if (d->__nextchar == NULL || *d->__nextchar == '\0') {
		/* Advance to the next ARGV-element.  */

		/* Give FIRST_NONOPT & LAST_NONOPT rational values if OPTIND has been
		   moved back by the user (who may also have changed the arguments).  */
		if (d->__last_nonopt > d->optind)
			d->__last_nonopt = d->optind;
		if (d->__first_nonopt > d->optind)
			d->__first_nonopt = d->optind;
		if (d->__ordering == PERMUTE) {
			/* If we have just processed some options following some non-options,
			   exchange them so that the options come first.  */

			if (d->__first_nonopt != d->__last_nonopt && d->__last_nonopt != d->optind)
				exchange((char **)argv, d);
			else if (d->__last_nonopt != d->optind)
				d->__first_nonopt = d->optind;
			/* Skip any additional non-options
			   and extend the range of non-options previously skipped.  */
			while (d->optind < argc && NONOPTION_P)
				d->optind++;
			d->__last_nonopt = d->optind;
		}

		/* The special ARGV-element `--' means premature end of options.
		   Skip it like a null option,
		   then exchange with previous non-options as if it were an option,
		   then skip everything else like a non-option.  */

		if (d->optind != argc && !strcmp(argv[d->optind], "--")) {
			d->optind++;
			if (d->__first_nonopt != d->__last_nonopt && d->__last_nonopt != d->optind)
				exchange((char **)argv, d);
			else if (d->__first_nonopt == d->__last_nonopt)
				d->__first_nonopt = d->optind;
			d->__last_nonopt = argc;
			d->optind = argc;
		}

		/* If we have done all the ARGV-elements, stop the scan
		   and back over any non-options that we skipped and permuted.  */

		if (d->optind == argc) {
			/* Set the next-arg-index to point at the non-options
			   that we previously skipped, so the caller will digest them.  */
			if (d->__first_nonopt != d->__last_nonopt)
				d->optind = d->__first_nonopt;
			return -1;
		}

		/* If we have come to a non-option and did not permute it,
		   either stop the scan or describe it to the caller and pass it by.  */

		if (NONOPTION_P) {
			if (d->__ordering == REQUIRE_ORDER)
				return -1;
			d->optarg = argv[d->optind++];
			return 1;
		}

		/* We have found another option-ARGV-element.
		   Skip the initial punctuation.  */

		d->__nextchar = (argv[d->optind] + 1 + (longopts != NULL && argv[d->optind][1] == '-'));
	}

	/* Decode the current option-ARGV-element.  */

	/* Check whether the ARGV-element is a long option.

	   If long_only and the ARGV-element has the form "-f", where f is
	   a valid short option, don't consider it an abbreviated form of
	   a long option that starts with f.  Otherwise there would be no
	   way to give the -f short option.

	   On the other hand, if there's a long option "fubar" and
	   the ARGV-element is "-fu", do consider that an abbreviation of
	   the long option, just like "--fu", and not "-f" with arg "u".

	   This distinction seems to be the most useful approach.  */

	if (longopts != NULL
	    && (argv[d->optind][1] == '-' || (long_only && (argv[d->optind][2] || !strchr(optstring, argv[d->optind][1]))))) {
		char *nameend;
		const struct option *p;
		const struct option *pfound = NULL;
		int exact = 0;
		int ambig = 0;
		int indfound = -1;
		int option_index;
		for (nameend = d->__nextchar; *nameend && *nameend != '='; nameend++)
			/* Do nothing.  */ ;
		/* Test all long options for either exact match
		   or abbreviated matches.  */
		for (p = longopts, option_index = 0; p->name; p++, option_index++)
			if (!strncmp(p->name, d->__nextchar, nameend - d->__nextchar)) {
				if ((unsigned int)(nameend - d->__nextchar) == (unsigned int)strlen(p->name)) {
					/* Exact match found.  */
					pfound = p;
					indfound = option_index;
					exact = 1;
					break;
				} else if (pfound == NULL) {
					/* First nonexact match found.  */
					pfound = p;
					indfound = option_index;
				} else if (long_only || pfound->has_arg != p->has_arg || pfound->flag != p->flag
					   || pfound->val != p->val)
					/* Second or later nonexact match found.  */
					ambig = 1;
			}

		if (ambig && !exact) {
			if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
				char *buf;
				if (__asprintf(&buf, _("%s: option `%s' is ambiguous\n"), argv[0], argv[d->optind]) >= 0) {
					_IO_flockfile(stderr);
					int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
					((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
					__fxprintf(NULL, "%s", buf);
					((_IO_FILE *) stderr)->_flags2 = old_flags2;
					_IO_funlockfile(stderr);
					free(buf);
				}
#else
				fprintf(stderr, _("%s: option `%s' is ambiguous\n"), argv[0], argv[d->optind]);
#endif
			}
			d->__nextchar += strlen(d->__nextchar);
			d->optind++;
			d->optopt = 0;
			return '?';
		}

		if (pfound != NULL) {
			option_index = indfound;
			d->optind++;
			if (*nameend) {
				/* Don't test has_arg with >, because some C compilers don't
				   allow it to be used on enums.  */
				if (pfound->has_arg)
					d->optarg = nameend + 1;
				else {
					if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
						char *buf;
						int n;
#endif
						if (argv[d->optind - 1][1] == '-') {
							/* --option */
#if defined _LIBC && defined USE_IN_LIBIO
							n = __asprintf(&buf, _("\
%s: option `--%s' doesn't allow an argument\n"), argv[0], pfound->name);
#else
							fprintf(stderr, _("\
%s: option `--%s' doesn't allow an argument\n"), argv[0], pfound->name);
#endif
						} else {
							/* +option or -option */
#if defined _LIBC && defined USE_IN_LIBIO
							n = __asprintf(&buf, _("\
%s: option `%c%s' doesn't allow an argument\n"), argv[0], argv[d->optind - 1][0], pfound->name);
#else
							fprintf(stderr, _("\
%s: option `%c%s' doesn't allow an argument\n"), argv[0], argv[d->optind - 1][0], pfound->name);
#endif
						}

#if defined _LIBC && defined USE_IN_LIBIO
						if (n >= 0) {
							_IO_flockfile(stderr);
							int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
							((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
							__fxprintf(NULL, "%s", buf);
							((_IO_FILE *) stderr)->_flags2 = old_flags2;
							_IO_funlockfile(stderr);
							free(buf);
						}
#endif
					}

					d->__nextchar += strlen(d->__nextchar);
					d->optopt = pfound->val;
					return '?';
				}
			} else if (pfound->has_arg == 1) {
				if (d->optind < argc)
					d->optarg = argv[d->optind++];
				else {
					if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
						char *buf;
						if (__asprintf(&buf, _("\
%s: option `%s' requires an argument\n"), argv[0], argv[d->optind - 1]) >= 0) {
							_IO_flockfile(stderr);
							int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
							((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
							__fxprintf(NULL, "%s", buf);
							((_IO_FILE *) stderr)->_flags2 = old_flags2;
							_IO_funlockfile(stderr);
							free(buf);
						}
#else
						fprintf(stderr, _("%s: option `%s' requires an argument\n"), argv[0],
							argv[d->optind - 1]);
#endif
					}
					d->__nextchar += strlen(d->__nextchar);
					d->optopt = pfound->val;
					return optstring[0] == ':' ? ':' : '?';
				}
			}
			d->__nextchar += strlen(d->__nextchar);
			if (longind != NULL)
				*longind = option_index;
			if (pfound->flag) {
				*(pfound->flag) = pfound->val;
				return 0;
			}
			return pfound->val;
		}

		/* Can't find it as a long option.  If this is not getopt_long_only,
		   or the option starts with '--' or is not a valid short
		   option, then it's an error.
		   Otherwise interpret it as a short option.  */
		if (!long_only || argv[d->optind][1] == '-' || strchr(optstring, *d->__nextchar) == NULL) {
			if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
				char *buf;
				int n;
#endif
				if (argv[d->optind][1] == '-') {
					/* --option */
#if defined _LIBC && defined USE_IN_LIBIO
					n = __asprintf(&buf, _("%s: unrecognized option `--%s'\n"), argv[0], d->__nextchar);
#else
					fprintf(stderr, _("%s: unrecognized option `--%s'\n"), argv[0], d->__nextchar);
#endif
				} else {
					/* +option or -option */
#if defined _LIBC && defined USE_IN_LIBIO
					n = __asprintf(&buf, _("%s: unrecognized option `%c%s'\n"), argv[0], argv[d->optind][0],
						       d->__nextchar);
#else
					fprintf(stderr, _("%s: unrecognized option `%c%s'\n"), argv[0], argv[d->optind][0],
						d->__nextchar);
#endif
				}

#if defined _LIBC && defined USE_IN_LIBIO
				if (n >= 0) {
					_IO_flockfile(stderr);
					int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
					((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
					__fxprintf(NULL, "%s", buf);
					((_IO_FILE *) stderr)->_flags2 = old_flags2;
					_IO_funlockfile(stderr);
					free(buf);
				}
#endif
			}
			d->__nextchar = (char *)"";
			d->optind++;
			d->optopt = 0;
			return '?';
		}
	}

	/* Look at and handle the next short option-character.  */

	{
		char c = *d->__nextchar++;
		char *temp = strchr(optstring, c);
		/* Increment `optind' when we start to process its last character.  */
		if (*d->__nextchar == '\0')
			++d->optind;
		if (temp == NULL || c == ':') {
			if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
				char *buf;
				int n;
#endif
				if (d->__posixly_correct) {
					/* 1003.2 specifies the format of this message.  */
#if defined _LIBC && defined USE_IN_LIBIO
					n = __asprintf(&buf, _("%s: illegal option -- %c\n"), argv[0], c);
#else
					fprintf(stderr, _("%s: illegal option -- %c\n"), argv[0], c);
#endif
				} else {
#if defined _LIBC && defined USE_IN_LIBIO
					n = __asprintf(&buf, _("%s: invalid option -- %c\n"), argv[0], c);
#else
					fprintf(stderr, _("%s: invalid option -- %c\n"), argv[0], c);
#endif
				}

#if defined _LIBC && defined USE_IN_LIBIO
				if (n >= 0) {
					_IO_flockfile(stderr);
					int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
					((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
					__fxprintf(NULL, "%s", buf);
					((_IO_FILE *) stderr)->_flags2 = old_flags2;
					_IO_funlockfile(stderr);
					free(buf);
				}
#endif
			}
			d->optopt = c;
			return '?';
		}
		/* Convenience. Treat POSIX -W foo same as long option --foo */
		if (temp[0] == 'W' && temp[1] == ';') {
			char *nameend;
			const struct option *p;
			const struct option *pfound = NULL;
			int exact = 0;
			int ambig = 0;
			int indfound = 0;
			int option_index;
			/* This is an option that requires an argument.  */
			if (*d->__nextchar != '\0') {
				d->optarg = d->__nextchar;
				/* If we end this ARGV-element by taking the rest as an arg,
				   we must advance to the next element now.  */
				d->optind++;
			} else if (d->optind == argc) {
				if (print_errors) {
					/* 1003.2 specifies the format of this message.  */
#if defined _LIBC && defined USE_IN_LIBIO
					char *buf;
					if (__asprintf(&buf, _("%s: option requires an argument -- %c\n"), argv[0], c) >= 0) {
						_IO_flockfile(stderr);
						int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
						((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
						__fxprintf(NULL, "%s", buf);
						((_IO_FILE *) stderr)->_flags2 = old_flags2;
						_IO_funlockfile(stderr);
						free(buf);
					}
#else
					fprintf(stderr, _("%s: option requires an argument -- %c\n"), argv[0], c);
#endif
				}
				d->optopt = c;
				if (optstring[0] == ':')
					c = ':';
				else
					c = '?';
				return c;
			} else
				/* We already incremented `d->optind' once;
				   increment it again when taking next ARGV-elt as argument.  */
				d->optarg = argv[d->optind++];
			/* optarg is now the argument, see if it's in the
			   table of longopts.  */
			for (d->__nextchar = nameend = d->optarg; *nameend && *nameend != '='; nameend++)
				/* Do nothing.  */ ;
			/* Test all long options for either exact match
			   or abbreviated matches.  */
			for (p = longopts, option_index = 0; p->name; p++, option_index++)
				if (!strncmp(p->name, d->__nextchar, nameend - d->__nextchar)) {
					if ((unsigned int)(nameend - d->__nextchar) == strlen(p->name)) {
						/* Exact match found.  */
						pfound = p;
						indfound = option_index;
						exact = 1;
						break;
					} else if (pfound == NULL) {
						/* First nonexact match found.  */
						pfound = p;
						indfound = option_index;
					} else
						/* Second or later nonexact match found.  */
						ambig = 1;
				}
			if (ambig && !exact) {
				if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
					char *buf;
					if (__asprintf(&buf, _("%s: option `-W %s' is ambiguous\n"), argv[0], argv[d->optind]) >= 0) {
						_IO_flockfile(stderr);
						int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
						((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
						__fxprintf(NULL, "%s", buf);
						((_IO_FILE *) stderr)->_flags2 = old_flags2;
						_IO_funlockfile(stderr);
						free(buf);
					}
#else
					fprintf(stderr, _("%s: option `-W %s' is ambiguous\n"), argv[0], argv[d->optind]);
#endif
				}
				d->__nextchar += strlen(d->__nextchar);
				d->optind++;
				return '?';
			}
			if (pfound != NULL) {
				option_index = indfound;
				if (*nameend) {
					/* Don't test has_arg with >, because some C compilers don't
					   allow it to be used on enums.  */
					if (pfound->has_arg)
						d->optarg = nameend + 1;
					else {
						if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
							char *buf;
							if (__asprintf(&buf, _("\
%s: option `-W %s' doesn't allow an argument\n"), argv[0], pfound->name) >= 0) {
								_IO_flockfile(stderr);
								int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
								((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
								__fxprintf(NULL, "%s", buf);
								((_IO_FILE *) stderr)->_flags2 = old_flags2;
								_IO_funlockfile(stderr);
								free(buf);
							}
#else
							fprintf(stderr, _("\
%s: option `-W %s' doesn't allow an argument\n"), argv[0], pfound->name);
#endif
						}

						d->__nextchar += strlen(d->__nextchar);
						return '?';
					}
				} else if (pfound->has_arg == 1) {
					if (d->optind < argc)
						d->optarg = argv[d->optind++];
					else {
						if (print_errors) {
#if defined _LIBC && defined USE_IN_LIBIO
							char *buf;
							if (__asprintf(&buf, _("\
%s: option `%s' requires an argument\n"), argv[0], argv[d->optind - 1]) >= 0) {
								_IO_flockfile(stderr);
								int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
								((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
								__fxprintf(NULL, "%s", buf);
								((_IO_FILE *) stderr)->_flags2 = old_flags2;
								_IO_funlockfile(stderr);
								free(buf);
							}
#else
							fprintf(stderr, _("%s: option `%s' requires an argument\n"), argv[0],
								argv[d->optind - 1]);
#endif
						}
						d->__nextchar += strlen(d->__nextchar);
						return optstring[0] == ':' ? ':' : '?';
					}
				}
				d->__nextchar += strlen(d->__nextchar);
				if (longind != NULL)
					*longind = option_index;
				if (pfound->flag) {
					*(pfound->flag) = pfound->val;
					return 0;
				}
				return pfound->val;
			}
			d->__nextchar = NULL;
			return 'W';	/* Let the application handle it.   */
		}
		if (temp[1] == ':') {
			if (temp[2] == ':') {
				/* This is an option that accepts an argument optionally.  */
				if (*d->__nextchar != '\0') {
					d->optarg = d->__nextchar;
					d->optind++;
				} else
					d->optarg = NULL;
				d->__nextchar = NULL;
			} else {
				/* This is an option that requires an argument.  */
				if (*d->__nextchar != '\0') {
					d->optarg = d->__nextchar;
					/* If we end this ARGV-element by taking the rest as an arg,
					   we must advance to the next element now.  */
					d->optind++;
				} else if (d->optind == argc) {
					if (print_errors) {
						/* 1003.2 specifies the format of this message.  */
#if defined _LIBC && defined USE_IN_LIBIO
						char *buf;
						if (__asprintf(&buf, _("\
%s: option requires an argument -- %c\n"), argv[0], c) >= 0) {
							_IO_flockfile(stderr);
							int old_flags2 = ((_IO_FILE *) stderr)->_flags2;
							((_IO_FILE *) stderr)->_flags2 |= _IO_FLAGS2_NOTCANCEL;
							__fxprintf(NULL, "%s", buf);
							((_IO_FILE *) stderr)->_flags2 = old_flags2;
							_IO_funlockfile(stderr);
							free(buf);
						}
#else
						fprintf(stderr, _("%s: option requires an argument -- %c\n"), argv[0], c);
#endif
					}
					d->optopt = c;
					if (optstring[0] == ':')
						c = ':';
					else
						c = '?';
				} else
					/* We already incremented `optind' once;
					   increment it again when taking next ARGV-elt as argument.  */
					d->optarg = argv[d->optind++];
				d->__nextchar = NULL;
			}
		}
		return c;
	}
}

static int
_getopt_internal(int argc, char **argv, const char *optstring, const struct option *longopts, int *longind, int long_only,
		 int posixly_correct)
{
	int result;
	getopt_data.optind = optind;
	getopt_data.opterr = opterr;
	result = _getopt_internal_r(argc, argv, optstring, longopts, longind, long_only, posixly_correct, &getopt_data);
	optind = getopt_data.optind;
	optarg = getopt_data.optarg;
	optopt = getopt_data.optopt;
	return result;
}

/* glibc gets a LSB-compliant getopt.
   Standalone applications get a POSIX-compliant getopt.  */
#if _LIBC
enum { POSIXLY_CORRECT = 0 };
#else
enum { POSIXLY_CORRECT = 1 };
#endif
static int getopt(int argc, char *const *argv, const char *optstring)
{
	return _getopt_internal(argc, (char **)argv, optstring, NULL, NULL, 0, POSIXLY_CORRECT);
}

#ifdef TEST

/* Compile with -DTEST to make an executable for use in testing
   the above definition of `getopt'.  */

int main(int argc, char **argv)
{
	int c;
	int digit_optind = 0;
	while (1) {
		int this_option_optind = optind ? optind : 1;
		c = getopt(argc, argv, "abc:d:0123456789");
		if (c == -1)
			break;
		switch (c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			if (digit_optind != 0 && digit_optind != this_option_optind)
				printf("digits occur in two different argv-elements.\n");
			digit_optind = this_option_optind;
			printf("option %c\n", c);
			break;
		case 'a':
			printf("option a\n");
			break;
		case 'b':
			printf("option b\n");
			break;
		case 'c':
			printf("option c with value `%s'\n", optarg);
			break;
		case '?':
			break;
		default:
			printf("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if (optind < argc) {
		printf("non-option ARGV-elements: ");
		while (optind < argc)
			printf("%s ", argv[optind++]);
		printf("\n");
	}

	exit(0);
}

#endif /* TEST */

/* getopt_long and getopt_long_only entry points for GNU getopt.
   Copyright (C) 1987,88,89,90,91,92,93,94,96,97,98,2004,2006
     Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */

/*
#ifdef _LIBC
# include <getopt.h>
#else
# include <config.h>
# include "getopt.h"
#endif
#include "getopt_int.h"
*/
#include <stdio.h>

/* This needs to come after some library #include
   to get __GNU_LIBRARY__ defined.  */
#ifdef __GNU_LIBRARY__
#include <stdlib.h>
#endif

#ifndef	NULL
#define NULL 0
#endif

static int
getopt_long(int argc, char *__getopt_argv_const * argv, const char *options, const struct option *long_options, int *opt_index)
{
	return _getopt_internal(argc, (char **)argv, options, long_options, opt_index, 0, 0);
}

static int
_getopt_long_r(int argc, char **argv, const char *options, const struct option *long_options, int *opt_index,
	       struct _getopt_data *d)
{
	return _getopt_internal_r(argc, argv, options, long_options, opt_index, 0, 0, d);
}

/* Like getopt_long, but '-' as well as '--' can indicate a long option.
   If an option that starts with '-' (not '--') doesn't match a long option,
   but does match a short option, it is parsed as a short option
   instead.  */

static int
getopt_long_only(int argc, char *__getopt_argv_const * argv, const char *options, const struct option *long_options, int *opt_index)
{
	return _getopt_internal(argc, (char **)argv, options, long_options, opt_index, 1, 0);
}

static int
_getopt_long_only_r(int argc, char **argv, const char *options, const struct option *long_options, int *opt_index,
		    struct _getopt_data *d)
{
	return _getopt_internal_r(argc, argv, options, long_options, opt_index, 1, 0, d);
}

#ifdef TEST

#include <stdio.h>

int main(int argc, char **argv)
{
	int c;
	int digit_optind = 0;
	while (1) {
		int this_option_optind = optind ? optind : 1;
		int option_index = 0;
		static struct option long_options[] = {
			{
			 "add", 1, 0, 0 },
			{
			 "append", 0, 0, 0 },
			{
			 "delete", 1, 0, 0 },
			{
			 "verbose", 0, 0, 0 },
			{
			 "create", 0, 0, 0 },
			{
			 "file", 1, 0, 0 },
			{
			 0, 0, 0, 0 }
		};
		c = getopt_long(argc, argv, "abc:d:0123456789", long_options, &option_index);
		if (c == -1)
			break;
		switch (c) {
		case 0:
			printf("option %s", long_options[option_index].name);
			if (optarg)
				printf(" with arg %s", optarg);
			printf("\n");
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			if (digit_optind != 0 && digit_optind != this_option_optind)
				printf("digits occur in two different argv-elements.\n");
			digit_optind = this_option_optind;
			printf("option %c\n", c);
			break;
		case 'a':
			printf("option a\n");
			break;
		case 'b':
			printf("option b\n");
			break;
		case 'c':
			printf("option c with value `%s'\n", optarg);
			break;
		case 'd':
			printf("option d with value `%s'\n", optarg);
			break;
		case '?':
			break;
		default:
			printf("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if (optind < argc) {
		printf("non-option ARGV-elements: ");
		while (optind < argc)
			printf("%s ", argv[optind++]);
		printf("\n");
	}

	exit(0);
}

#endif /* TEST */

/* --version or -v */
static void print_version(void)
{
	printf("This is version %s %s\n", PACKAGE_STRING, PACKAGE_URL);
	exit(0);
}

/* --help or -h */
static void print_help(void)
{
	printf("Usage: %s [options] inputfilename\nOptions are:\n", PACKAGE_NAME);
	printf("%s\n", "--algo name              or -a name   set algorithm to start with");
	printf("%s\n", "--colorpage              or -C        print html page with the colors at start");
	printf("%s\n", "--debug 0/1/2            or -D 0/1/2  print debug information, 0=off");
	printf("%s\n", "--edgelabels yes/no      or -l yes/no show edge labels");
	printf("%s\n", "--help                   or -h        print help options");
	printf("%s\n", "--init-folded yes/no     or -f yes/no fold all subgraphs initially");
	printf("%s\n", "--init-unfolded yes/no   or -F yes/no unfold all subgraphs initially");
	printf("%s\n", "--multi-arrows yes/no    or -A yes/no show multiple arrows in long edges");
	printf("%s\n", "--nodenames yes/no       or -n yes/no show node name instead of node labels");
	printf("%s\n", "--output image.png       or -o o.png  write png image and run as console tool");
	printf("%s\n", "--prealgo name           or -p name   set pre-algorithm to start with");
	printf("%s\n", "--parsedebug             or -P        print parsing debug output");
	printf("%s\n", "--placedebug             or -L        print placement debug output");
	printf("%s\n", "--selfedges yes/no       or -S yes/no show self edges");
	printf("%s\n", "--singlenodes yes/no     or -1 yes/no show single nodes");
	printf("%s\n", "--splines yes/no         or -s yes/no show spline edge lines");
	printf("%s\n", "--subgraph-area yes/no   or -g yes/no show subgraph area");
	printf("%s\n", "--testcode yes/no        or -t yes/no enable testing code");
	printf("%s\n", "--toggle-altcolor yes/no or -T yes/no toggle altcolor in grey drawing");
	printf("%s\n", "--version                or -v        show version of this program");
	printf("%s\n", "--ZZ                     or -Z        dryrun mode for testing");
	printf("%s\n", "For the --algo option valid algorithm names are:");
	printf("%s\n", "bary");
	printf("%s\n", "For the --prealgo option valid algorithm names are:");
	printf("%s\n", "none");
	printf("%s is GNU GPL version 3 Free Software free to copy, share, use and improve\nat %s\n", PACKAGE_NAME, PACKAGE_URL);
	printf("%s\n", "/*");
	printf(" *  %s is a GNU GPL free software program to draw directed graphs.\n", PACKAGE_NAME);
	printf("%s\n", " *");
	printf("%s\n", " *  This program is free software: you can redistribute it and/or modify");
	printf("%s\n", " *  it under the terms of the GNU General Public License as published by");
	printf("%s\n", " *  the Free Software Foundation, either version 3 of the License, or");
	printf("%s\n", " *  (at your option) any later version.");
	printf("%s\n", " *");
	printf("%s\n", " *  This program is distributed in the hope that it will be useful,");
	printf("%s\n", " *  but WITHOUT ANY WARRANTY; without even the implied warranty of");
	printf("%s\n", " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
	printf("%s\n", " *  GNU General Public License for more details.");
	printf("%s\n", " *");
	printf("%s\n", " *  You should have received a copy of the GNU General Public License");
	printf("%s\n", " *  along with this program.  If not, see <http://www.gnu.org/licenses/>.");
	printf("%s\n", " *");
	printf("%s\n", " *  These are the four essential freedoms with GNU GPL software:");
	printf("%s\n", " *  1: freedom to run the program, for any purpose");
	printf("%s\n", " *  2: freedom to study how the program works, and change it to make it do what you wish");
	printf("%s\n", " *  3: freedom to redistribute copies to help your Free Software girlfriends and friends");
	printf("%s\n",
	       " *  4: freedom to distribute copies of your modified versions to your Free Software girlfriends and friends");
	printf("%s\n", " *   ,            ,");
	printf("%s\n", " *  /              \\");
	printf("%s\n", " * ((__-^^-,-^^-__))");
	printf("%s\n", " * `-_---'  `---_-'");
	printf("%s\n", " *  `--|o`   'o|--'");
	printf("%s\n", " *      \\  `  /");
	printf("%s\n", " *       ): :(");
	printf("%s\n", " *       :o_o:");
	printf("%s\n", " *        \"-\"");
	printf("%s\n", " */");
	exit(0);
}

void commandline_options(int argc, char *argv[])
{
	int c = 0;
	int option_index = 0;
	/*
	 * struct option
	 * {
	 * const char *name;
	 * int has_arg;
	 * int *flag;
	 * int val;
	 * };
	 *
	 * Names for the values of the `has_arg' field of `struct option'.
	 * #define no_argument         0
	 * #define required_argument   1
	 * #define optional_argument   2
	 */
	static struct option long_options[] = {
		{
		 "algo", required_argument, NULL, 'a' },
		{
		 "colorpage", no_argument, NULL, 'C' },
		{
		 "debug", required_argument, NULL, 'D' },
		{
		 "edgelabels", required_argument, NULL, 'l' },
		{
		 "help", no_argument, NULL, 'h' },
		{
		 "init-folded", required_argument, NULL, 'f' },
		{
		 "init-unfolded", required_argument, NULL, 'F' },
		{
		 "multi-arrows", required_argument, NULL, 'A' },
		{
		 "nodenames", required_argument, NULL, 'n' },
		{
		 "output", required_argument, NULL, 'o' },
		{
		 "prealgo", required_argument, NULL, 'p' },
		{
		 "parsedebug", no_argument, NULL, 'P' },
		{
		 "placedebug", no_argument, NULL, 'L' },
		{
		 "selfedges", required_argument, NULL, 'S' },
		{
		 "singlenodes", required_argument, NULL, '1' },
		{
		 "splines", required_argument, NULL, 's' },
		{
		 "subgraph-area", required_argument, NULL, 'g' },
		{
		 "testcode", required_argument, NULL, 't' },
		{
		 "toggle-altcolor", required_argument, NULL, 'T' },
		{
		 "version", no_argument, NULL, 'v' },
		{
		 "ZZ", no_argument, NULL, 'Z' },
		{
		 NULL, 0, NULL, 0 }
	};
	/* pacify gcc */
	if (0) {
		(void)getopt( /* int argc */ 0, /* char *const *argv */ NULL,
			     /* const char *optstring */ NULL);
		(void)getopt_long_only( /* int argc */ 0,
				       /* char *__getopt_argv_const * argv */ NULL,
				       /* const char *options */ NULL,
				       /* const struct option *long_options */ NULL,
				       /* int *opt_index */ NULL);
		(void)_getopt_long_r( /* int argc */ 0, /* char **argv */ NULL,
				     /* const char *options */ NULL,
				     /* const struct option *long_options */ NULL,
				     /* int *opt_index */ NULL,
				     /* struct _getopt_data *d */ NULL);
		(void)_getopt_long_only_r( /* int argc */ 0, /* char **argv */ NULL,
					  /* const char *options */ NULL,
					  /* const struct option *long_options */
					  NULL, /* int *opt_index */ NULL,
					  /* struct _getopt_data *d */ NULL);
	}
	if (argc == 1) {
		return;
	}
	if (argv) {
	}

	for (;;) {
		option_index = 0;
		c = getopt_long(argc, argv, "1:A:a:g:hf:l:n:o:p:s:t:vCD:G:LPS:T:Z", long_options, &option_index);
		if (c == -1) {
			break;
		}

		switch (c) {
		case 0:
			printf("option %s", long_options[option_index].name);
			if (optarg) {
				printf(" with arg %s", optarg);
			}
			printf("\n");
			break;
			/* --singlenodes yes/no -1 yes/no */
		case '1':
			if (strcmp(optarg, "yes") == 0) {
				/* do not draw single nodes if set */
				option_no_singlenodes = 0;
			} else if (strcmp(optarg, "no") == 0) {
				/* do not draw single nodes if set */
				option_no_singlenodes = 1;
			} else {
				/* use compiled-in default */
			}
			break;
			/* --algo name or -a name set start algorithm */
		case 'a':
			option_algo = is_valid_algo(optarg);
			if (option_algo == NULL) {
				printf("%s(): ignored invalid algorithm name %s with option --algo or -a\n", __FUNCTION__, optarg);
				fflush(stdout);
			}
			break;
			/* --multi-arrows yes/no -A yes/no */
		case 'A':
			if (strcmp(optarg, "yes") == 0) {
				/* draw multiple arrows at long edges */
				option_multi_arrows = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* draw multiple arrows at long edges */
				option_multi_arrows = 0;
			} else {
				/* use compiled-in default */
			}
			break;
			/* --init-folded yes/no -f yes/no */
		case 'f':
			if (strcmp(optarg, "yes") == 0) {
				/* if set force all subgraphs folded initially */
				option_init_folded = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set force all subgraphs folded initially */
				option_init_folded = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --subgraph-area yes/no -g yes/no */
		case 'g':
			if (strcmp(optarg, "yes") == 0) {
				/* if set draw subgraph area rectangle */
				option_subgraph_area = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set draw subgraph area rectangle */
				option_subgraph_area = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --help or -h */
		case 'h':
			print_help();
			break;
			/* --edgelabels yes/no -l yes/no */
		case 'l':
			if (strcmp(optarg, "yes") == 0) {
				/* if set do not include edgelabels in the graph */
				option_no_edgelabels = 0;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set do not include edgelabels in the graph */
				option_no_edgelabels = 1;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --nodenames yes/no -n yes/no */
		case 'n':
			if (strcmp(optarg, "yes") == 0) {
				/* if set force to use the node name as node label in drawing */
				option_nodenames = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set force to use the node name as node label in drawing */
				option_nodenames = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --output filename.png or -o filename.png */
		case 'o':
			if (option_outfilename) {
				/* -o option has been used before */
				printf("%s(): changing output filename from \"%s\" to \"%s\"\n", __FUNCTION__, option_outfilename,
				       optarg);
				fflush(stdout);
			}
			/* set new output filename */
			option_outfilename = optarg;
			if (option_outfilename) {
				/* check for "" */
				if (strlen(option_outfilename) == 0) {
					option_outfilename = NULL;
				}
			}
			break;
			/* --prealgo name or -p name set start pre-algorithm */
		case 'p':
			option_algo = is_valid_prealgo(optarg);
			if (option_prealgo == NULL) {
				printf("%s(): ignored invalid algorithm name %s with option --prealgo or -p\n", __FUNCTION__,
				       optarg);
				fflush(stdout);
			}
			break;
			/* --splines yes/no or -s yes/no */
		case 's':
			if (strcmp(optarg, "yes") == 0) {
				/* draw splines if set */
				option_splines = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* draw splines if set */
				option_splines = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --testcode yes/no or -t yes/no */
		case 't':
			if (strcmp(optarg, "yes") == 0) {
				/* testcode if set */
				option_testcode = (option_testcode + 1);
			} else if (strcmp(optarg, "no") == 0) {
				/* testcode if set */
				option_testcode = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --version or -v */
		case 'v':
			print_version();
			break;
			/* --colorpage or -C */
		case 'C':
			option_colorpage = 1;
			break;
			/* --debug 0/1/2 */
		case 'D':
			if (strcmp(optarg, "0") == 0) {
				/* if set print memory usage */
				option_memlog = 0;
				/* if set print layouter debug */
				option_ldebug = 0;
				/* if set print gui debug */
				option_gdebug = 0;
			} else if (strcmp(optarg, "1") == 0) {
				/* if set print memory usage */
				option_memlog = 1;
				/* if set print layouter debug */
				option_ldebug = 1;
				/* if set print gui debug */
				option_gdebug = 1;
			} else if (strcmp(optarg, "2") == 0) {
				/* if set print memory usage */
				option_memlog = 2;
				/* if set print layouter debug */
				option_ldebug = 2;
				/* if set print gui debug */
				option_gdebug = 2;
			} else {
				/* use compiled-in defaults */
			}
			break;
			/* --init-unfolded yes/no -F yes/no */
		case 'F':
			if (strcmp(optarg, "yes") == 0) {
				/* if set force all subgraphs unfolded initially */
				option_init_unfolded = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set force all subgraphs unfolded initially */
				option_init_unfolded = 0;
			} else {
				/* use compiled in setting */
			}
			break;
			/* --parsedebug -P */
		case 'P':
			/* print parsing debug output if set */
			option_parsedebug = 1;
			break;
			/* --placedebug -L */
		case 'L':
			/* print place debug output if set */
			option_placedebug = 1;
			break;
			/* --selfedges yes/no -S yes/no */
		case 'S':
			if (strcmp(optarg, "yes") == 0) {
				/* draw self edges if set */
				option_selfedges = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* draw self edges if set */
				option_selfedges = 0;
			} else {
				/* use compiled-in default */
			}
			break;
			/* --toggle-altcolor yes/no -T yes/no */
		case 'T':
			if (strcmp(optarg, "yes") == 0) {
				/* if set toggle altcolor bit in altcolor drawing --toggle-altcolor yes/no -T yes/no */
				option_toggle_altcolor = 1;
			} else if (strcmp(optarg, "no") == 0) {
				/* if set toggle altcolor bit in altcolor drawing --toggle-altcolor yes/no -T yes/no */
				option_toggle_altcolor = 0;
			} else {
				/* use compiled-in default */
			}
			break;
			/* --ZZ or -Z dryrun mode for testing */
		case 'Z':
			option_dryrun = 1;
			break;
		case '?':
			break;
		default:
			printf("?? getopt returned character code 0x%02x ??\n", c);
			break;
		}
	}

	/* set filename to first entry, skip the others */
	if (optind < argc) {
		/* non-option ARGV-elements: */
		while (optind < argc) {
			/* optional graph filename at commandline to start with */
			if (option_filename == NULL) {
				option_filename = argv[optind++];
				if (option_filename) {
					/* check for "" */
					if (strlen(option_filename) == 0) {
						option_filename = NULL;
					}
				}
			} else {
				printf("skipped filename %s\n", argv[optind++]);
			}
		}
	}

	fflush(stdout);
	return;
}

/* end */
