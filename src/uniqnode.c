
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

/* a database for node data indexed on the node name */

#include <stdio.h>
#include <string.h>

#include "splay-tree.h"
#include "main.h"
#include "mem.h"
#include "uniqnode.h"
#include "options.h"

/* nodes database */
static splay_tree gunode = (splay_tree) 0;

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void uniqnode_splay_tree_delete_key_fn(splay_tree_key keydata);

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void uniqnode_splay_tree_delete_value_fn(splay_tree_value valuedata);

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void uniqnode_splay_tree_delete_key_fn(splay_tree_key keydata)
{
	/* key is a (char *) deallocated via uniqstring */
	if (keydata) {
	}
	return;
}

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void uniqnode_splay_tree_delete_value_fn(splay_tree_value valuedata)
{
	/* value is (struct unode *) deallocated in nes */
	if (valuedata) {
	}
	return;
}

/* clear database */
void uniqnode_clear(void)
{
	gunode = splay_tree_delete(gunode);
	return;
}

/* find node in database */
struct unode *uniqnode(char *name)
{

	splay_tree_node spn = (splay_tree_node) 0;

	if (name == NULL) {
		return (NULL);
	}

	/* name "" is valid for a node */

	if (gunode == NULL) {
		if (option_ldebug || 0) {
			printf("%s(): new tree\n", __FUNCTION__);
			fflush(stdout);
		}
		/* indexed on (char *) */
		gunode = splay_tree_new(splay_tree_compare_strings,	/* splay_tree_compare_fn */
					uniqnode_splay_tree_delete_key_fn,	/* splay_tree_delete_key_fn */
					uniqnode_splay_tree_delete_value_fn	/* splay_tree_delete_value_fn */
		    );
	}

	if (option_ldebug || 0) {
		printf("%s(): search \"%s\" in tree %p\n", __FUNCTION__, name, (void *)gunode);
		fflush(stdout);
	}

	spn = splay_tree_lookup((splay_tree) gunode, (splay_tree_key) name);

	if (option_ldebug || 0) {
		printf("%s(): search \"%s\" result %p\n", __FUNCTION__, name, (void *)spn);
		fflush(stdout);
	}

	if (spn) {
		return ((struct unode *)spn->value);
	} else {
		return ((struct unode *)0);
	}
}

/* add node to database */
void uniqnode_add(char *name, struct unode *un)
{

	if (name == NULL) {
		return;
	}

	if (un == (struct unode *)0) {
		return;
	}

	if (gunode == NULL) {
		/* indexed on (char *) */
		gunode = splay_tree_new(splay_tree_compare_strings,	/* splay_tree_compare_fn */
					uniqnode_splay_tree_delete_key_fn,	/* splay_tree_delete_key_fn */
					uniqnode_splay_tree_delete_value_fn	/* splay_tree_delete_value_fn */
		    );
	}

	splay_tree_insert(gunode,	/* splay_tree */
			  (splay_tree_key) name, (splay_tree_value) un);

	if (option_ldebug || 0) {
		printf("%s(): insert %s\n", __FUNCTION__, name);
		fflush(stdout);
	}

	return;
}

/* end */
