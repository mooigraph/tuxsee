
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include "splay-tree.h"
#include "main.h"
#include "parse.h"
#include "parsedot.h"
#include "uniqstring.h"
#include "color.h"
#include "options.h"

/* */
int is_a_dotedgedefault(struct usubg *subg)
{
	int color = 0;		/* edge line color */
	int color_set = 0;
	int fcolor = 0;		/* edge label text color */
	int fcolor_set = 0;
	int ffontname_set = 0;	/* font name */
	char *ffontname = NULL;
	char *penwstr = NULL;
	double thicknessd = 0.0;
	int thickness = 0;
	int thickness_set = 0;
	char *elabel = NULL;
	int elabel_set = 0;
	int estyle = ESTYLE_NORMAL;
	int estyle_set = 0;
	char *stylep = NULL;
	int efontsize_set = 0;
	char *efontsize = NULL;
	char *csstr = NULL;
	int cscode = 0;
	int cscode_set = 0;

	/* edge or EDGE allowed */
	if (token != DOT_EDGE) {
		return (0);
	}

	if (subg) {
		/* */
	} else {
		/* */
	}
	/* [ */
	token = dot_lex();
	if (token != DOT_BRACKETO) {
		parser_error_string = "expected [ at edge default";
		return (1);
	}

	color = 0;
	color_set = 0;
	fcolor = 0;
	fcolor_set = 0;
	ffontname_set = 0;	/* font name */
	ffontname = NULL;
	thickness = 0;
	thickness_set = 0;
	elabel = NULL;
	elabel_set = 0;
	estyle = ESTYLE_NORMAL;
	estyle_set = 0;
	stylep = NULL;
	efontsize_set = 0;
	efontsize = NULL;
	csstr = NULL;

	/* keep on reading the options */
	for (;;) {
		token = dot_lex();
		if (token == 0) {
			parser_error_string = "un-expected end-of-file at edge default";
			return (1);
		}
		if (token == DOT_COMMA) {
			/* skip optional comma */
			token = dot_lex();
		}
		if (token == DOT_SEMIC) {
			/* skip optional semicomma */
			token = dot_lex();
		}

		if (token == DOT_BRACKETC) {
			/* end of options */
			break;
		}

		/* 3) keep on reading the edge options
		 * URL arrowhead arrowsize arrowtail color colorscheme comment constraint
		 * decorate dir edgeURL edgehref edgetarget edgetooltip fontcolor fontname
		 * fontsize headURL head_lp headclip headhref headlabel headport headtarget
		 * headtooltip href id label labelURL labelangle labeldistance labelfloat
		 * labelfontcolor labelfontname labelfontsize labelhref labeltarget
		 * labeltooltip layer len lhead lp ltail minlen nojustify penwidth
		 * pos samehead sametail showboxes style tailURL tail_lp tailclip tailhref
		 * taillabel tailport tailtarget tailtooltip target tooltip weight xlabel xlp
		 */

		if (token == DOT_ID) {
			if (strcmp(lastid, "URL") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default URL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "arrowhead") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default arrowhead";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "arrowsize") == 0) {
				/* scale factor arrow, double, default 1.0 */
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default arrowsize";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "arrowtail") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default arrowtail";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "color") == 0) {
				/* color of edge line */
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default color";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					color = colorcode(laststring);
					if (color == -1) {
						color_set = 0;
					} else {
						color_set = 1;
					}
				} else if (token == DOT_ID) {
					color = colorcode(lastid);
					if (color == -1) {
						color_set = 0;
					} else {
						color_set = 1;
					}
				} else if (token == DOT_NUM) {
					/* number in a colorscheme */
					color_set = 0;
				} else {
					parser_error_string = "expected colorname at edge default color";
					return (1);
				}
			} else if (strcmp(lastid, "colorscheme") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default colorscheme";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					csstr = laststring;
				} else if (token == DOT_ID) {
					csstr = lastid;
				} else {
					parser_error_string = "expected string at edge default colorscheme";
					return (1);
				}
				cscode = colorschemecode(csstr);
				if (cscode == 0) {
					parser_error_string = "expected colorscheme at edge default colorscheme";
					return (1);
				}
				cscode_set = 1;
			} else if (strcmp(lastid, "comment") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default comment";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "constraint") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default constraint";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "decorate") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default decorate";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "dir") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default dir";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "edgeURL") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default edgeURL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "edgehref") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default edgehref";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "edgetarget") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default edgetarget";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "edgetooltip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default edgetooltip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "fontcolor") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default fontcolor";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					fcolor = colorcode(laststring);
					if (fcolor == -1) {
						fcolor_set = 0;
					} else {
						fcolor_set = 1;
					}
				} else if (token == DOT_ID) {
					fcolor = colorcode(lastid);
					if (fcolor == -1) {
						fcolor_set = 0;
					} else {
						fcolor_set = 1;
					}
				} else if (token == DOT_NUM) {
					/* number in a colorscheme */
					fcolor_set = 0;
				} else {
					parser_error_string = "expected colorname at edge default fontcolor";
					return (1);
				}
			} else if (strcmp(lastid, "fontname") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default fontname";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					ffontname = laststring;
					ffontname_set = 1;
				} else if (token == DOT_ID) {
					ffontname = lastid;
					ffontname_set = 1;
				} else {
					ffontname_set = 0;
					parser_error_string = "expected name at edge default fontname";
					return (1);
				}
			} else if (strcmp(lastid, "fontsize") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default fontsize";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					efontsize = laststring;
				} else if (token == DOT_NUM) {
					efontsize = lastnum;
				} else {
					parser_error_string = "expected number string at edge default fontsize";
					return (1);
				}
				if (strlen(efontsize) == 0) {
					efontsize = NULL;
				}
				efontsize_set = 1;
			} else if (strcmp(lastid, "headURL") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headURL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "head_lp") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default head_lp";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headclip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headclip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headhref") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headhref";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headlabel") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headlabel";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headport") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headport";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headtarget") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headtarget";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "headtooltip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default headtooltip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "href") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default href";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "id") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default id";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "label") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default label";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					elabel = laststring;
				} else if (token == DOT_ID) {
					elabel = lastid;
				} else if (token == DOT_NUM) {
					elabel = lastnum;
				} else if (token == DOT_HTML) {
					elabel = lasthtml;
				} else if (token == DOT_CHAR) {
					elabel = lastchar;
				} else {
					parser_error_string = "expected string at edge default label";
					return (1);
				}
				if (strlen(elabel) == 0) {
					elabel = NULL;
				}
				elabel_set = 1;
			} else if (strcmp(lastid, "labelURL") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelURL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labelangle") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelangle";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labeldistance") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labeldistance";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labelfloat") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelfloat";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labelfontcolor") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelfontcolor";
					return (1);
				}
				token = dot_lex();

			} else if (strcmp(lastid, "labelfontname") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelfontname";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labelfontsize") == 0) {
				/* fontsize for headlabel, taillabel */
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelfontsize";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					efontsize = laststring;
				} else if (token == DOT_NUM) {
					efontsize = lastnum;
				} else {
					parser_error_string = "expected number string at edge default labelfontsize";
					return (1);
				}
				if (strlen(efontsize) == 0) {
					efontsize = NULL;
				}
				efontsize_set = 1;
			} else if (strcmp(lastid, "labelhref") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labelhref";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labeltarget") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labeltarget";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "labeltooltip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default labeltooltip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "layer") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default layer";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "len") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default len";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "lhead") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default lhead";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "lp") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default lp";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "ltail") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default ltail";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "minlen") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default minlen";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "nojustify") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default nojustify";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "pencolor") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default pencolor";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "penwidth") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default penwidth";
					return (1);
				}
				token = dot_lex();
				if (token == DOT_STRING) {
					penwstr = laststring;
				} else if (token == DOT_NUM) {
					penwstr = lastnum;
				} else {
					parser_error_string = "expected double number at edge default penwidth";
					return (1);
				}
				if (penwstr == NULL) {
					parser_error_string = "unexpected double number at edge default penwidth nil string";
					return (1);
				}
				/* check for "" */
				if (strlen(penwstr) == 0) {
					thickness = 0;	/* default */
					thickness_set = 1;
				} else {
					/* can add extra check on chars left */
					thicknessd = strtod(penwstr, NULL);
					if (errno) {
						parser_error_string =
						    "unexpected double number at edge default penwidth cannot convert";
						return (1);
					}
					thicknessd = round(thicknessd);
					thickness = (int)thicknessd;
					/* limiter, see main.h */
					if (thickness < 0) {
						thickness = 0;
					}
					if (thickness > 15) {
						thickness = 15;
					}
					thickness_set = 1;
				}
			} else if (strcmp(lastid, "pos") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default pos";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "samehead") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default samehead";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "sametail") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default sametail";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "showboxes") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default showboxes";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "style") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default style";
					return (1);
				}
				token = dot_lex();
				/* default settings for edge line */
				estyle = ESTYLE_NORMAL;
				/* to update, check for DOT_ID and compare with lastid */
				/* fixme there are more edge styles */
				if (token == DOT_STRING) {
					/* check for "" empty string */
					if (strlen(laststring) == 0) {
						estyle = ESTYLE_NORMAL;
					} else if (strncmp(laststring, "bold", 4) == 0) {
						estyle = ESTYLE_NORMAL;
						thickness = 2;
						thickness_set = 1;
					} else if (strncmp(laststring, "normal", 6) == 0) {
						estyle = ESTYLE_NORMAL;
					} else if (strncmp(laststring, "solid", 5) == 0) {
						estyle = ESTYLE_SOLID;
					} else if (strncmp(laststring, "dotted", 6) == 0) {
						estyle = ESTYLE_DOTTED;
					} else if (strncmp(laststring, "dashed", 6) == 0) {
						estyle = ESTYLE_DASHED;
					} else if (strncmp(laststring, "invis", 5) == 0) {
						estyle = ESTYLE_INVIS;
					} else {
						/* something else */
						estyle = ESTYLE_NORMAL;
					}
					/* check if there is extra modify arg */
					stylep = strchr(laststring, ',');
					if (stylep) {
						if (strncmp(stylep, ",bold", 5) == 0) {
							thickness = 2;
							thickness_set = 1;
						}
					}
					estyle_set = 1;
				} else {
					/* or could issue error */
					estyle = ESTYLE_NORMAL;
					estyle_set = 0;
				}
			} else if (strcmp(lastid, "tailURL") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailURL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tail_lp") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailURL";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tailclip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailclip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tailhref") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailhref";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "taillabel") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default taillabel";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tailport") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailport";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tailtarget") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailtarget";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tailtooltip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tailtooltip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "target") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default target";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "tooltip") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default tooltip";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "weight") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default weight";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "xlabel") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default xlabel";
					return (1);
				}
				token = dot_lex();
			} else if (strcmp(lastid, "xlp") == 0) {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default xlp";
					return (1);
				}
				token = dot_lex();
			} else {
				token = dot_lex();
				if (token != DOT_IS) {
					parser_error_string = "expected = at edge default unknown-option";
					return (1);
				}
				token = dot_lex();
			}
		} else {
			/* not id, can be char, number, html, string */
			token = dot_lex();
			if (token != DOT_IS) {
				parser_error_string = "expected = at edge default unknown-option";
				return (1);
			}
			token = dot_lex();
		}

	}			/* end of for() */

	/* color of edge line */
	if (color_set) {
		if (subg == NULL) {
			ed_color = color;
		} else {
			subg->ed_color = color;
		}
	}

	/* text color of ege label */
	if (fcolor_set) {
		if (subg == NULL) {
			ed_fcolor = fcolor;
		} else {
			subg->ed_fcolor = fcolor;
		}
	}

	if (ffontname_set) {
		if (subg == NULL) {
			ed_ffontname = ffontname;
		} else {
			subg->ed_ffontname = ffontname;
		}
	}

	/* edge line thickness */
	if (thickness_set) {
		if (subg == NULL) {
			ed_thickness = thickness;
		} else {
			subg->ed_thickness = thickness;
		}
	}

	/* edge label */
	if (elabel_set) {
		if (subg == NULL) {
			ed_label = elabel;
		} else {
			subg->ed_label = elabel;
		}
	}

	/* edge line style */
	if (estyle_set) {
		if (subg == NULL) {
			ed_style = estyle;
		} else {
			subg->ed_style = estyle;
		}
	}

	/* font size */
	if (efontsize_set) {
		if (subg == NULL) {
			ed_fontsize = efontsize;
		} else {
			subg->ed_fontsize = efontsize;
		}
	}

	/* colorscheme */
	if (cscode_set) {
		if (subg == NULL) {
			ed_cs = cscode;
		} else {
			subg->ed_cs = cscode;
		}
	}

	/* oke */
	return (1);
}

/* end */
