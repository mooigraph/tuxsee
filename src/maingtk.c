
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

/* This can be configured here: the starting (x,y) size of the main window */
#define TOP_LEVEL_WINDOW_XSIZE 500
#define TOP_LEVEL_WINDOW_YSIZE 430

/* known bug: this uses cairo gtk lib which prints error assertions at big files */
/* This can be configured here: the default font paramaters
 *
 * a GTK font pattern. These have the syntax
 * fontname [properties] [fontsize]
 *
 * where fontname is the family name,
 * properties is a list of property values separated by spaces,
 * and fontsize is the point size.
 * The properties that you may specify for GTK font patterns are as follows:
 * Slant properties: ‘Italic’ or ‘Oblique’. If omitted, the default (roman) slant is implied.
 * Weight properties: ‘Bold’, ‘Book’, ‘Light’, ‘Medium’, ‘Semi-bold’, or ‘Ultra-light’. If omitted, ‘Medium’ weight is implied.
 * Width properties: ‘Semi-Condensed’ or ‘Condensed’. If omitted, a default width is used.
 * Here are some examples of GTK font patterns:
 * Monospace 12
 * Monospace Bold Italic 12
 */

#ifdef WIN32

/* pango on wine has issues with font options, so use only a font name */

/* name of font to use, example "Sans" */
#define DEFAULT_FONTNAME "Sans"

/* name of slant to use, example "Italic", "Oblique" or "Roman" */
#define DEFAULT_FONTSLANT " "

/* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
#define DEFAULT_FONTWEIGHT " "

/* name of condensed to use, example "Semi-Condensed", "Condensed" */
#define DEFAULT_FONTCONDENSED " "

/* font size to use, example "10", "18", "20" etc. */
#define DEFAULT_FONTSIZE " "

#else

/* Linux defaults */

/* name of font to use, example "Sans" */
#define DEFAULT_FONTNAME "Sans"

/* name of slant to use, example "Italic", "Oblique" or "Roman" */
#define DEFAULT_FONTSLANT "ROMAN"

/* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
#define DEFAULT_FONTWEIGHT "Light"

/* name of condensed to use, example "Semi-Condensed", "Condensed" */
#define DEFAULT_FONTCONDENSED "Condensed"

/* font size to use, example "10", "18", "20" etc. */
#define DEFAULT_FONTSIZE "10"

#endif

/* edge line thickness double value for example 0.5, 1.0, 1.5 */
#define DEFAULT_EDGE_THICKNESS 0.5

/* node shapes are created in the browser with svg-edit (MIT license)
 * https://github.com/SVG-Edit/svgedit
 * then converted the svg source with my small c program
 * in c source using the gtk+ cairo lib.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <math.h>
#include <sys/types.h>

/* "fix" issue with glib-2.0 uid_t definition when cross compile with mingw gcc on Linux */
#ifdef WIN32
#define uid_t long
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <cairo.h>

#include "splay-tree.h"
#include "main.h"
#include "mem.h"
#include "parse.h"
#include "options.h"
#include "lmain.h"
#include "nes.h"
#include "color.h"
#include "source.h"
#include "manual.h"
#include "folding.h"
#include "uniqstring.h"
#include "rhp.h"

/* global gtk routines here */
#include "maingtk.h"
#include "maingtk2.h"

#ifdef GDK_WINDOWING_X11
/* oke for use with X11 on GNU/Linux */
#endif

#ifdef GDK_WINDOWING_WIN32
#warning "not tested on windows"
#endif

/* top level window also used in maingtk2.c */
GtkWidget *mainwindow1 = (GtkWidget *) 0;

/* where to draw */
GtkWidget *drawingarea1 = (GtkWidget *) 0;

/* name of last dir where file was opened */
char *lastdir = NULL;

/* name of last dir where file was saved */
char *lastdir2 = NULL;

/* old mouse (x,y) left button clicked used for dragging drawing */
static int mouse_oldx = 0;
static int mouse_oldy = 0;

/* draw node and edge data if valid data */
struct drawn *drawnldata = NULL;
struct drawe *draweldata = NULL;

/* exposed size of draw area */
static int drawing_area_xsize = 0;
static int drawing_area_ysize = 0;

/* zoom scaling factor changed by zoom slider */
double zfactor = 1.0;

/* x offset changed by x slider */
int vxmin = 0;

/* y offset changed by y slider */
int vymin = 0;

/* sliders */
#if GTK_HAVE_API_VERSION_2 == 1
GtkObject *adjvscale1 = NULL;
GtkObject *adjvscale2 = NULL;
GtkObject *adjhscale1 = NULL;
#endif

#if GTK_HAVE_API_VERSION_3 == 1
GtkAdjustment *adjvscale1 = NULL;
GtkAdjustment *adjvscale2 = NULL;
GtkAdjustment *adjhscale1 = NULL;
#endif

/* small space around text of a node at drawing */
static int drawing_default_textborder = 12;

/* status line gtk buffers */
static GtkTextBuffer *entry1buffer = NULL;
static char charentry1buffer[64];

/* set when left shift key is down and used with mouse wheel */
static int l_s_k_d = 0;

/* draw in alt color grey when set */
int altcolor = 0;

/* r/g/b of drawing alt color "grey82" */
static int altr = 0xd1;
static int altg = 0xd1;
static int altb = 0xd1;

/* r/g/b of subgraphs area outline color in folded and unfolded state */
static int subgar_folded = 0;	/* darker green */
static int subgag_folded = 150;
static int subgab_folded = 0;

static int subgar_unfolded = 150;	/* lighter green */
static int subgag_unfolded = 255;
static int subgab_unfolded = 150;

/* finally stop the gui */
static void top_level_window_main_quit (void);

/* 'open' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void on_top_level_window_open1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* 'new' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void on_top_level_window_new1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* 'quit' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void on_top_level_window_quit1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* left slider */
static void on_vscale1_changed (GtkAdjustment * adj);

/* right slider */
static void on_vscale2_changed (GtkAdjustment * adj);

/* bottom slider */
static void on_hscale1_changed (GtkAdjustment * adj);

/* checkbox 1 is 'splines' */
static void check1_toggle (GtkWidget * widget, gpointer window);

/* checkbox 2 is 'singlenodes' */
static void check2_toggle (GtkWidget * widget, gpointer window);

/* checkbox 3 is 'selfedges' */
static void check3_toggle (GtkWidget * widget, gpointer window);

/* checkbox 4 is 'edgelabels' */
static void check4_toggle (GtkWidget * widget, gpointer window);

/* checkbox 5 is 'multi-arrows' */
static void check5_toggle (GtkWidget * widget, gpointer window);

/* checkbox 6 is 'subgraph-area' */
static void check6_toggle (GtkWidget * widget, gpointer window);

/* checkbox 7 is 'followedge' in prio */
static void check7_toggle (GtkWidget * widget, gpointer window);

/* checkbox 7 is 'up/down' in prio */
static void check8_clicked (GtkWidget * widget, gpointer window);

/* xspinbutton adjustment 'xspreading' changed */
static void xspin_changed (GtkWidget * widget, gpointer spinbutton);

/* yspinbutton adjustment 'yspreading' changed */
static void yspin_changed (GtkWidget * widget, gpointer spinbutton);

/* button1 fold all subgraphs at once */
static void button1_clicked (GtkWidget * widget, gpointer window);

/* button2 unfold all subgraphs at once */
static void button2_clicked (GtkWidget * widget, gpointer window);

/* fit drawing */
static void dofit (void);

/* button3 fit whole drawing in drawing area */
static void button3_clicked (GtkWidget * widget, gpointer window);

/* mouse click on drawing area */
static gboolean on_mouse_clicked (GtkWidget * widget, GdkEventButton * event, gpointer user_data);

/* in drawing clicked at (nx,ny) and toggle folding if there is a node or summary node */
static void on_mouse_clicked_button1 (int nx, int ny);

/* reset alt color draw mode */
static int reset_altcolor (void);

/* set alt color draw mode */
static int set_altcolor (void);

/* hilite outgoing edges */
static void fdummyout (struct unode *un);

/* hilite incoming edges */
static void fdummyin (struct unode *un);

/* dragging drawing when left button 1 is held down */
static gboolean on_motion_notify_event (GtkWidget * widget, GdkEventMotion * event);

/* in drawing clicked at (nx,ny) and toggle alt color drawing */
static void on_mouse_clicked_button2 (int nx, int ny);

/* in drawing clicked at (nx,ny) and move that point to center of screen if possible */
static void on_mouse_clicked_button3 (GdkEventButton * event, double dx, double dy);

/* change zoom factor */
static void on_mouse_clicked_button8 (void);

/* change zoom factor */
static void on_mouse_clicked_button9 (void);

/* wheel of mouse movement */
static gboolean on_mouse_wheel (GtkWidget * widget, GdkEventScroll * event, gpointer user_data);

/* key pressed */
static gboolean on_key_press (GtkWidget * widget, GdkEventKey * event, gpointer user_data);

/* key released */
static gboolean on_key_released (GtkWidget * widget, GdkEventKey * event, gpointer user_data);

#if GTK_HAVE_API_VERSION_2 == 1
/* redraw drawing area */
static gboolean on_top_level_window_drawingarea1_expose_event (GtkWidget * widget, GdkEventExpose * event, gpointer user_data);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
/* redraw drawing area */
static gboolean on_top_level_window_drawingarea1_draw_event (GtkWidget * widget, cairo_t * crdraw, gpointer user_data);
#endif

/* draw subgraph area */
void on_top_level_window_drawingarea1_expose_event_subgrapharea (cairo_t * crp);

/* draw all nodes */
void on_top_level_window_drawingarea1_expose_event_nodes (cairo_t * crp);

/* draw all nodes in one layer */
static void on_top_level_window_drawingarea1_expose_event_nodes_layer (cairo_t * crp, struct dli *lp);

/* draw all edges */
void on_top_level_window_drawingarea1_expose_event_edges (cairo_t * crp);

/* draw arrow for edge */
static void on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (cairo_t * crp, int start_x, int start_y, int end_x, int end_y);

/* draw all edges in one layer */
static void on_top_level_window_drawingarea1_expose_event_edges_layer (cairo_t * crp, int level);

/* check for valid save filename */
char *valid_save_filename (char *fname);

/* check for valid open filename */
static char *valid_open_filename (char *fname);

/* check for valid text label */
static char *valid_label_string (char *label);

/* set (x,y) text size of summary node in subgraph */
static void maingtk_textsizes_subg (struct usubg *subg, cairo_t * crdraw);

/* use full screen */
static void on_top_level_window_fullscreen1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* use normal screen in a window */
static void on_top_level_window_unfullscreen1_activate (GtkMenuItem * menuitem, gpointer user_data);

/* heuristics */
static void on_top_level_window_heuristicbary_activate (GtkMenuItem * menuitem, gpointer user_data);
static void on_top_level_window_heuristicadjustweightavg_activate (GtkMenuItem * menuitem, gpointer user_data);
static void on_top_level_window_heuristicadjustweightleft_activate (GtkMenuItem * menuitem, gpointer user_data);
static void on_top_level_window_heuristiclevelsnormal_activate (GtkMenuItem * menuitem, gpointer user_data);
static void on_top_level_window_heuristiclevelsextra_activate (GtkMenuItem * menuitem, gpointer user_data);

/* show about dialog with ok button */
static void on_top_level_window_show_about (GtkWidget * widget, gpointer data);

/* draw mode following edges */
static void on_top_level_window_followi (GtkWidget * widget, gpointer data);

/* draw mode following edges */
static void on_top_level_window_followl (GtkWidget * widget, gpointer data);

/* draw mode following edges */
static void on_top_level_window_followr (GtkWidget * widget, gpointer data);

/* draw mode following edges */
static void on_top_level_window_followo (GtkWidget * widget, gpointer data);

/* draw mode following edges */
static void on_top_level_window_left (GtkWidget * widget, gpointer data);

/* draw mode centered */
static void on_top_level_window_centered (GtkWidget * widget, gpointer data);

/* draw mode prio1 */
static void on_top_level_window_centerm (GtkWidget * widget, gpointer data);

/* draw mode prio2 */
static void on_top_level_window_centern (GtkWidget * widget, gpointer data);

/* show statistics dialog with ok button */
static void on_top_level_window_show_statistics (GtkWidget * widget, gpointer data);

/* show versions dialog with ok button */
static void on_top_level_window_show_versions (GtkWidget * widget, gpointer data);

/* draw node shape with background color */
static void draw_nodeshape_rcloud (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_cloud (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_card (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_diamond (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_mdiamond (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_hexagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_pentagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_folder (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_sbox (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_chevron (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_cylinder (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_cube (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_dialog (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_maximize (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_page (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_plaque (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_prep (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_punched (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_septagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_shield (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_stored (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_trapezoid (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_triangle (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_itriangle (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_vscroll (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un);
static void draw_nodeshape_box (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un);
static void draw_nodeshape_dbox (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un);
static void draw_nodeshape_rbox (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un);
static void draw_nodeshape_ellips (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un);
static void draw_nodeshape_circle (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un);

/* this are the 3 global routines wrapped to the statis versions */
static void static_update_status_text (char *text);
static void static_maingtk_textsizes (void);
static void static_maingtk (int argc, char *argv[]);

void
maingtk (int argc, char *argv[])
{
  static_maingtk (argc, argv);
  return;
}

void
maingtk_textsizes (void)
{
  static_maingtk_textsizes ();
  return;
}

/* */
void
update_status_text (char *text)
{
  static_update_status_text (text);
  return;
}


/* build and start gtk gui */
static void
static_maingtk (int argc, char *argv[])
{
  int status = 0;
  GtkWidget *vbox1;
  GtkWidget *menubar1;
  /* -- */
  GtkWidget *menuitem1;
  GtkWidget *menuitem1_menu;
  GtkWidget *open1;
  GtkWidget *new1;
  GtkWidget *saveaspng1;
  GtkWidget *saveaspdf1;
  GtkWidget *saveassvg1;
  GtkWidget *saveassvgjs1;
  GtkWidget *saveastulip1;
  GtkWidget *saveasdia1;
  GtkWidget *saveasels1;
  GtkWidget *saveasdot1;
  GtkWidget *saveasvrml1;
  GtkWidget *saveasx3d1;
  GtkWidget *saveasmanual1;
  GtkWidget *saveassource1;
  GtkWidget *quit1;
  /* -- */
  GtkWidget *menuitem2;
  GtkWidget *menuitem2_menu;
  GtkWidget *fullscreen1;
  GtkWidget *unfullscreen1;
  /* -- */
  GtkWidget *menuitem3;
  GtkWidget *menuitem3_menu;
  GtkWidget *heuristicbary;
  GtkWidget *heuristicadjustweightavg;
  GtkWidget *heuristicadjustweightleft;
  GtkWidget *heuristiclevelsnormal;
  GtkWidget *heuristiclevelsextra;
  /* -- */
  GtkWidget *menuitem4;
  GtkWidget *menuitem4_menu;
  GtkWidget *followi1;
  GtkWidget *followl1;
  GtkWidget *followr1;
  GtkWidget *followo1;
  GtkWidget *left1;
  GtkWidget *centered1;
  GtkWidget *centerm1;
  GtkWidget *centern1;
  /* -- */
  GtkWidget *menuitem5;
  GtkWidget *menuitem5_menu;
  GtkWidget *treeview1;
  GtkWidget *statistics1;
  GtkWidget *versions1;
  GtkWidget *about1;
  /* -- */
  GtkWidget *hbox1;
  GtkWidget *vscale1;
  GtkWidget *vscale2;
  GtkWidget *hscale1;
  /* --- */
  GtkWidget *hbox2;
  GtkWidget *check1;
  GtkWidget *check2;
  GtkWidget *check3;
  GtkWidget *button1;
  GtkWidget *button2;
  GtkWidget *button3;
  /* -- */
  GtkWidget *hbox3;
  GtkWidget *check4;
  GtkWidget *check5;
  GtkWidget *check6;
  GtkWidget *xspinlabel;
  GtkAdjustment *xspinadjustment;
  GtkWidget *xspinbutton;
  GtkWidget *yspinlabel;
  GtkAdjustment *yspinadjustment;
  GtkWidget *yspinbutton;
  GtkWidget *entry1;
  /* -- */
  GtkWidget *hbox4;
  GtkWidget *check7;
  GtkWidget *check8;

#if !GLIB_CHECK_VERSION (2, 36, 0)
  /* for GDBus */
  g_type_init ();
#endif

  /*
   *    gtk_init (&argc, &argv); in gkt2, gtk3 and gtk_init() in gtk4
   *
   * calls the function gtk_init(gint *argc, gchar ***argv) which will be called in all GTK applications. 
   * This sets up a few things for us such as the default visual and color map and then proceeds to call 
   * gdk_init(gint *argc, gchar ***argv). This function initializes the library for use, sets up default 
   * signal handlers, and checks the arguments passed to your application on the command line, 
   * looking for one of the following:
   *
   *    * --gtk-module
   *    * --g-fatal-warnings
   *    * --gtk-debug
   *    * --gtk-no-debug
   *    * --gdk-debug
   *    * --gdk-no-debug
   *    * --display
   *    * --sync
   *    * --no-xshm
   *    * --name
   *    * --class
   *
   * It removes these from the argument list, leaving anything it does not recognize for your application 
   * to parse or ignore. This creates a set of standard arguments accepted by all GTK applications.
   *
   */

  /* do gtk init, gtk will grab the gtk specific options on command line */
#if GTK_HAVE_API_VERSION_4 == 1
  (void) gtk_init ();
#else
  /* gtk 2, 3 */
  status = gtk_init_check (&argc, &argv);

  if (status == FALSE)
    {
      /* this does happen with sudo strace ./tuxsee for some reason */
      printf ("%s is in console mode and need in graphical mode to run\n", argv[0]);
      fflush (stdout);
      return;
    }
#endif

  /* top level outer window */
  mainwindow1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  /* make sure to exit oke. */
  g_signal_connect (G_OBJECT (mainwindow1), "destroy", G_CALLBACK (top_level_window_main_quit), NULL);

  /* needed for the cairo drawing */
  gtk_widget_set_app_paintable (mainwindow1, TRUE);

  /* use package string program name as set by configure in config.h */
  gtk_window_set_title (GTK_WINDOW (mainwindow1), PACKAGE_STRING);

  /* pre-set some size */
  gtk_window_set_default_size (GTK_WINDOW (mainwindow1), TOP_LEVEL_WINDOW_XSIZE, TOP_LEVEL_WINDOW_YSIZE);

  /* handle key pressed */
  g_signal_connect (G_OBJECT (mainwindow1), "key_press_event", G_CALLBACK (on_key_press), NULL);

  /* handle key released */
  g_signal_connect (G_OBJECT (mainwindow1), "key_release_event", G_CALLBACK (on_key_released), NULL);

  /* --- */

  /* vbox1 is a menu bar */
#if GTK_HAVE_API_VERSION_2 == 1
  vbox1 = gtk_vbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (mainwindow1), vbox1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vbox1 = gtk_box_new (GTK_ORIENTATION_VERTICAL, /* spacing */ 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (mainwindow1), vbox1);
#endif

  /* --- */

  /* menu bar in vbox1 */
  menubar1 = gtk_menu_bar_new ();
  gtk_widget_show (menubar1);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ menubar1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* --- */

  /* menu items in menu bar in vbox1 */
  menuitem1 = gtk_menu_item_new_with_mnemonic ("File");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
  gtk_widget_show (menuitem1);

  /* --- */

  /* 'file' sub menu in menu items in menu bar in vbox1 */
  menuitem1_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);

  /* --- */

  /* 'open' in 'file' sub menu in menu items in menu bar in vbox1 */
  open1 = gtk_menu_item_new_with_mnemonic ("Open");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), open1);
  gtk_widget_show (open1);

  /* run this routine when selected 'open' in 'file' menu */
  g_signal_connect (G_OBJECT (open1), "activate", G_CALLBACK (on_top_level_window_open1_activate), NULL);

  /* 'new' in 'file' sub menu in menu items in menu bar in vbox1 */
  new1 = gtk_menu_item_new_with_mnemonic ("Random");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), new1);
  gtk_widget_show (new1);

  /* run this routine when selected 'new' in 'file' menu */
  g_signal_connect (G_OBJECT (new1), "activate", G_CALLBACK (on_top_level_window_new1_activate), NULL);

  /* 'save as png' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveaspng1 = gtk_menu_item_new_with_mnemonic ("Save as png");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveaspng1);
  gtk_widget_show (saveaspng1);

  /* run this routine when selected 'save as png' in 'file' menu */
  g_signal_connect (G_OBJECT (saveaspng1), "activate", G_CALLBACK (on_top_level_window_saveaspng1_activate), NULL);

  /* 'save as pdf' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveaspdf1 = gtk_menu_item_new_with_mnemonic ("Save as pdf");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveaspdf1);
  gtk_widget_show (saveaspdf1);

  /* run this routine when selected 'save as pdf' in 'file' menu */
  g_signal_connect (G_OBJECT (saveaspdf1), "activate", G_CALLBACK (on_top_level_window_saveaspdf1_activate), NULL);

  /* 'save as 'svg' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveassvg1 = gtk_menu_item_new_with_mnemonic ("Save as svg (gtk)");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveassvg1);
  gtk_widget_show (saveassvg1);

  /* run this routine when selected 'save as svg' in 'file' menu */
  g_signal_connect (G_OBJECT (saveassvg1), "activate", G_CALLBACK (on_top_level_window_saveassvg1_activate), NULL);

  /* 'save as 'svg-js' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveassvgjs1 = gtk_menu_item_new_with_mnemonic ("Save as svg (+js)");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveassvgjs1);
  gtk_widget_show (saveassvgjs1);

  /* run this routine when selected 'save as svg-js' in 'file' menu */
  g_signal_connect (G_OBJECT (saveassvgjs1), "activate", G_CALLBACK (on_top_level_window_saveassvgjs1_activate), NULL);

  /* 'save as 'tlp' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveastulip1 = gtk_menu_item_new_with_mnemonic ("Save as tlp");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveastulip1);
  gtk_widget_show (saveastulip1);

  /* run this routine when selected 'save as tlp' in 'file' menu */
  g_signal_connect (G_OBJECT (saveastulip1), "activate", G_CALLBACK (on_top_level_window_saveastulip1_activate), NULL);

  /* 'save as 'dia' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveasdia1 = gtk_menu_item_new_with_mnemonic ("Save as dia");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasdia1);
  gtk_widget_show (saveasdia1);

  /* run this routine when selected 'save as dia' in 'file' menu */
  g_signal_connect (G_OBJECT (saveasdia1), "activate", G_CALLBACK (on_top_level_window_saveasdia1_activate), NULL);

  /* 'save as 'els' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveasels1 = gtk_menu_item_new_with_mnemonic ("Save as els");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasels1);
  gtk_widget_show (saveasels1);

  /* run this routine when selected 'save as els' in 'file' menu */
  g_signal_connect (G_OBJECT (saveasels1), "activate", G_CALLBACK (on_top_level_window_saveasels1_activate), NULL);

  /* 'save as 'dot' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveasdot1 = gtk_menu_item_new_with_mnemonic ("Save as dot");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasdot1);
  gtk_widget_show (saveasdot1);

  /* run this routine when selected 'save as dot' in 'file' menu */
  g_signal_connect (G_OBJECT (saveasdot1), "activate", G_CALLBACK (on_top_level_window_saveasdot1_activate), NULL);

  /* 'save as 'vrml' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveasvrml1 = gtk_menu_item_new_with_mnemonic ("Save as vrml");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasvrml1);
  gtk_widget_show (saveasvrml1);

  /* run this routine when selected 'save as vrml' in 'file' menu */
  g_signal_connect (G_OBJECT (saveasvrml1), "activate", G_CALLBACK (on_top_level_window_saveasvrml1_activate), NULL);

  /* 'save as 'x3d' in 'file' sub menu in menu items in menu bar in vbox1 */
  saveasx3d1 = gtk_menu_item_new_with_mnemonic ("Save as x3d");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasx3d1);
  gtk_widget_show (saveasx3d1);

  /* run this routine when selected 'save as x3d' in 'file' menu */
  g_signal_connect (G_OBJECT (saveasx3d1), "activate", G_CALLBACK (on_top_level_window_saveasx3d1_activate), NULL);

  /* optional save manual.pdf manual if compiled-in data */
  if (n_manual_bytes)
    {
      /* 'save manual' in 'file' sub menu in menu items in menu bar in vbox1 */
      saveasmanual1 = gtk_menu_item_new_with_mnemonic ("Save manual");
      gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveasmanual1);
      gtk_widget_show (saveasmanual1);

      /* run this routine when selected 'save manual' in 'file' menu */
      g_signal_connect (G_OBJECT (saveasmanual1), "activate", G_CALLBACK (on_top_level_window_saveasmanual1_activate), NULL);
    }

  /* optional save source.tgz sourcecode if compiled-in data */
  if (n_source_bytes)
    {
      /* 'save source' in 'file' sub menu in menu items in menu bar in vbox1 */
      saveassource1 = gtk_menu_item_new_with_mnemonic ("Save source");
      gtk_container_add (GTK_CONTAINER (menuitem1_menu), saveassource1);
      gtk_widget_show (saveassource1);

      /* run this routine when selected 'save source' in 'file' menu */
      g_signal_connect (G_OBJECT (saveassource1), "activate", G_CALLBACK (on_top_level_window_saveassource1_activate), NULL);
    }

  /* 'quit' in 'file' sub menu in menu items in menu bar in vbox1 */
  quit1 = gtk_menu_item_new_with_mnemonic ("Quit");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), quit1);
  gtk_widget_show (quit1);

  /* run this routine when selected 'quit' in 'file' menu */
  g_signal_connect (G_OBJECT (quit1), "activate", G_CALLBACK (on_top_level_window_quit1_activate), NULL);

  /* --- */

  /* menu items in menu bar in vbox1 */
  menuitem2 = gtk_menu_item_new_with_mnemonic ("Fullscreen");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem2);
  gtk_widget_show (menuitem2);

  /* 'fullscreen' sub menu in menu items in menu bar in vbox1 */
  menuitem2_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem2), menuitem2_menu);

  /* 'fullscreen->fullscreen' in 'fullscreen' sub menu in menu items in menu bar in vbox1 */
  fullscreen1 = gtk_menu_item_new_with_mnemonic ("Fullscreen");
  gtk_container_add (GTK_CONTAINER (menuitem2_menu), fullscreen1);
  gtk_widget_show (fullscreen1);

  /* run this routine when selected 'fullscreen->fullscreen in 'fullscreen' menu */
  g_signal_connect (G_OBJECT (fullscreen1), "activate", G_CALLBACK (on_top_level_window_fullscreen1_activate), NULL);

  /* 'fullscreen->normal' in 'fullscreen' sub menu in menu items in menu bar in vbox1 */
  unfullscreen1 = gtk_menu_item_new_with_mnemonic ("Normal");
  gtk_container_add (GTK_CONTAINER (menuitem2_menu), unfullscreen1);
  gtk_widget_show (unfullscreen1);

  /* run this routine when selected 'fullscreen->normal' in 'fullscreen' menu */
  g_signal_connect (G_OBJECT (unfullscreen1), "activate", G_CALLBACK (on_top_level_window_unfullscreen1_activate), NULL);

  /* -- */

  /* menu items in menu bar in vbox1 */
  menuitem3 = gtk_menu_item_new_with_mnemonic ("Heuristic");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem3);
  gtk_widget_show (menuitem3);

  /* sub menu in menu items in menu bar in vbox1 */
  menuitem3_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem3), menuitem3_menu);

  heuristicbary = gtk_menu_item_new_with_mnemonic ("Barycenter");
  gtk_container_add (GTK_CONTAINER (menuitem3_menu), heuristicbary);
  gtk_widget_show (heuristicbary);
  g_signal_connect (G_OBJECT (heuristicbary), "activate", G_CALLBACK (on_top_level_window_heuristicbary_activate), NULL);

  heuristicadjustweightavg = gtk_menu_item_new_with_mnemonic ("Adjust weight avg");
  gtk_container_add (GTK_CONTAINER (menuitem3_menu), heuristicadjustweightavg);
  gtk_widget_show (heuristicadjustweightavg);
  g_signal_connect (G_OBJECT (heuristicadjustweightavg), "activate", G_CALLBACK (on_top_level_window_heuristicadjustweightavg_activate), NULL);

  heuristicadjustweightleft = gtk_menu_item_new_with_mnemonic ("Adjust weight left");
  gtk_container_add (GTK_CONTAINER (menuitem3_menu), heuristicadjustweightleft);
  gtk_widget_show (heuristicadjustweightleft);
  g_signal_connect (G_OBJECT (heuristicadjustweightleft), "activate", G_CALLBACK (on_top_level_window_heuristicadjustweightleft_activate), NULL);

  heuristiclevelsnormal = gtk_menu_item_new_with_mnemonic ("Levels normal");
  gtk_container_add (GTK_CONTAINER (menuitem3_menu), heuristiclevelsnormal);
  gtk_widget_show (heuristiclevelsnormal);
  g_signal_connect (G_OBJECT (heuristiclevelsnormal), "activate", G_CALLBACK (on_top_level_window_heuristiclevelsnormal_activate), NULL);

  heuristiclevelsextra = gtk_menu_item_new_with_mnemonic ("Levels extra");
  gtk_container_add (GTK_CONTAINER (menuitem3_menu), heuristiclevelsextra);
  gtk_widget_show (heuristiclevelsextra);
  g_signal_connect (G_OBJECT (heuristiclevelsextra), "activate", G_CALLBACK (on_top_level_window_heuristiclevelsextra_activate), NULL);



  /* -- */

  /* menu items in menu bar in vbox1 */
  menuitem4 = gtk_menu_item_new_with_mnemonic ("Draw");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem4);
  gtk_widget_show (menuitem4);

  /* sub menu in menu items in menu bar in vbox1 */
  menuitem4_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem4), menuitem4_menu);

  /* 'followi' in 'draw' sub menu in menu items in menu bar in vbox1 */
  followi1 = gtk_menu_item_new_with_mnemonic ("Followi");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), followi1);
  gtk_widget_show (followi1);

  /* run this routine when selected 'followi' in 'draw' menu */
  g_signal_connect (G_OBJECT (followi1), "activate", G_CALLBACK (on_top_level_window_followi), NULL);

  /* 'followl' in 'draw' sub menu in menu items in menu bar in vbox1 */
  followl1 = gtk_menu_item_new_with_mnemonic ("Followl");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), followl1);
  gtk_widget_show (followl1);

  /* run this routine when selected 'followl' in 'draw' menu */
  g_signal_connect (G_OBJECT (followl1), "activate", G_CALLBACK (on_top_level_window_followl), NULL);

  /* 'followr' in 'draw' sub menu in menu items in menu bar in vbox1 */
  followr1 = gtk_menu_item_new_with_mnemonic ("Followr");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), followr1);
  gtk_widget_show (followr1);

  /* run this routine when selected 'followr' in 'draw' menu */
  g_signal_connect (G_OBJECT (followr1), "activate", G_CALLBACK (on_top_level_window_followr), NULL);

  /* 'followo' in 'draw' sub menu in menu items in menu bar in vbox1 */
  followo1 = gtk_menu_item_new_with_mnemonic ("Followo");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), followo1);
  gtk_widget_show (followo1);

  /* run this routine when selected 'followo' in 'draw' menu */
  g_signal_connect (G_OBJECT (followo1), "activate", G_CALLBACK (on_top_level_window_followo), NULL);

  /* 'left' in 'draw' sub menu in menu items in menu bar in vbox1 */
  left1 = gtk_menu_item_new_with_mnemonic ("Left");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), left1);
  gtk_widget_show (left1);

  /* run this routine when selected 'left' in 'draw' menu */
  g_signal_connect (G_OBJECT (left1), "activate", G_CALLBACK (on_top_level_window_left), NULL);

  /* 'centered' in 'draw' sub menu in menu items in menu bar in vbox1 */
  centered1 = gtk_menu_item_new_with_mnemonic ("Center");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), centered1);
  gtk_widget_show (centered1);

  /* run this routine when selected 'centered' in 'draw' menu */
  g_signal_connect (G_OBJECT (centered1), "activate", G_CALLBACK (on_top_level_window_centered), NULL);

  /* 'centerm' in 'draw' sub menu in menu items in menu bar in vbox1 */
  centerm1 = gtk_menu_item_new_with_mnemonic ("Centerm");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), centerm1);
  gtk_widget_show (centerm1);

  /* run this routine when selected 'centerm' in 'draw' menu */
  g_signal_connect (G_OBJECT (centerm1), "activate", G_CALLBACK (on_top_level_window_centerm), NULL);

  /* 'centern' in 'draw' sub menu in menu items in menu bar in vbox1 */
  centern1 = gtk_menu_item_new_with_mnemonic ("Centern");
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), centern1);
  gtk_widget_show (centern1);

  /* run this routine when selected 'centern' in 'draw' menu */
  g_signal_connect (G_OBJECT (centern1), "activate", G_CALLBACK (on_top_level_window_centern), NULL);

  /* -- */

  /* menu items in menu bar in vbox1 */
  menuitem5 = gtk_menu_item_new_with_mnemonic ("Misc");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem5);
  gtk_widget_show (menuitem5);

  /* sub menu in menu items in menu bar in vbox1 */
  menuitem5_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem5), menuitem5_menu);

  /* 'treeview' in 'misc' sub menu in menu items in menu bar in vbox1 */
  treeview1 = gtk_menu_item_new_with_mnemonic ("Treeview");
  gtk_container_add (GTK_CONTAINER (menuitem5_menu), treeview1);
  gtk_widget_show (treeview1);

  /* run this routine when selected 'treeview' in 'misc' menu */
  g_signal_connect (G_OBJECT (treeview1), "activate", G_CALLBACK (on_top_level_window_treeview), NULL);

  /* 'statistics' in 'misc' sub menu in menu items in menu bar in vbox1 */
  statistics1 = gtk_menu_item_new_with_mnemonic ("Statistics");
  gtk_container_add (GTK_CONTAINER (menuitem5_menu), statistics1);
  gtk_widget_show (statistics1);

  /* run this routine when selected 'statistics' in 'misc' menu */
  g_signal_connect (G_OBJECT (statistics1), "activate", G_CALLBACK (on_top_level_window_show_statistics), NULL);

  /* 'versions' in 'misc' sub menu in menu items in menu bar in vbox1 */
  versions1 = gtk_menu_item_new_with_mnemonic ("Versions");
  gtk_container_add (GTK_CONTAINER (menuitem5_menu), versions1);
  gtk_widget_show (versions1);

  /* run this routine when selected 'versions' in 'misc' menu */
  g_signal_connect (G_OBJECT (versions1), "activate", G_CALLBACK (on_top_level_window_show_versions), NULL);

  /* 'about' in 'misc' sub menu in menu items in menu bar in vbox1 */
  about1 = gtk_menu_item_new_with_mnemonic ("About");
  gtk_container_add (GTK_CONTAINER (menuitem5_menu), about1);
  gtk_widget_show (about1);

  /* run this routine when selected 'about' in 'misc' menu */
  g_signal_connect (G_OBJECT (about1), "activate", G_CALLBACK (on_top_level_window_show_about), NULL);

  /*
   * in hbox1
   * left zoom slider
   * drawing area
   * right y slider
   * below x slider
   */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox1 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox1);
#endif

  /* vertical slider in hbox1 for the zoom factor 50% is 1:1 */
  adjvscale1 = gtk_adjustment_new (50, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  vscale1 = gtk_vscale_new (GTK_ADJUSTMENT (adjvscale1));
  g_signal_connect (G_OBJECT (adjvscale1), "value_changed", GTK_SIGNAL_FUNC (on_vscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale1), FALSE);
  gtk_widget_show (vscale1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vscale1 = gtk_scale_new (GTK_ORIENTATION_VERTICAL, GTK_ADJUSTMENT (adjvscale1));
  g_signal_connect (G_OBJECT (adjvscale1), "value_changed", G_CALLBACK (on_vscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale1), FALSE);
  gtk_widget_show (vscale1);
#endif

  /* where to draw in hbox1 */
  drawingarea1 = gtk_drawing_area_new ();
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ drawingarea1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (drawingarea1);

#if GTK_HAVE_API_VERSION_2 == 1
  g_signal_connect (G_OBJECT (drawingarea1), "expose_event", G_CALLBACK (on_top_level_window_drawingarea1_expose_event), NULL);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  g_signal_connect (G_OBJECT (drawingarea1), "draw", G_CALLBACK (on_top_level_window_drawingarea1_draw_event), NULL);
#endif

  /* mouse buttons */
  g_signal_connect (G_OBJECT (drawingarea1), "button-press-event", G_CALLBACK (on_mouse_clicked), NULL);
  g_signal_connect (G_OBJECT (drawingarea1), "motion_notify_event", G_CALLBACK (on_motion_notify_event), NULL);

  /* */
  gtk_widget_set_events (drawingarea1, (GDK_BUTTON_PRESS_MASK | GDK_BUTTON1_MOTION_MASK));

  /* mouse wheel */
  g_signal_connect (G_OBJECT (drawingarea1), "scroll-event", G_CALLBACK (on_mouse_wheel), NULL);
  gtk_widget_add_events (drawingarea1, GDK_SCROLL_MASK);

  /* vertical slider in hbox1 for the y position range 0...100% of full image size */
  adjvscale2 = gtk_adjustment_new (0, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  vscale2 = gtk_vscale_new (GTK_ADJUSTMENT (adjvscale2));
  g_signal_connect (G_OBJECT (adjvscale2), "value_changed", GTK_SIGNAL_FUNC (on_vscale2_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale2,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale2), FALSE);
  gtk_widget_show (vscale2);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vscale2 = gtk_scale_new (GTK_ORIENTATION_VERTICAL, GTK_ADJUSTMENT (adjvscale2));
  g_signal_connect (G_OBJECT (adjvscale2), "value_changed", G_CALLBACK (on_vscale2_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale2,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale2), FALSE);
  gtk_widget_show (vscale2);
#endif

  /* horizontal scroller 0..100% of drawing size */
  adjhscale1 = gtk_adjustment_new (0, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  hscale1 = gtk_hscale_new (GTK_ADJUSTMENT (adjhscale1));
  g_signal_connect (G_OBJECT (adjhscale1), "value_changed", GTK_SIGNAL_FUNC (on_hscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (hscale1), FALSE);
  gtk_widget_show (hscale1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hscale1 = gtk_scale_new (GTK_ORIENTATION_HORIZONTAL, GTK_ADJUSTMENT (adjhscale1));
  g_signal_connect (G_OBJECT (adjhscale1), "value_changed", G_CALLBACK (on_hscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (hscale1), FALSE);
  gtk_widget_show (hscale1);
#endif

  /* --- */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox2 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox2);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox2 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox2);
#endif

  /* --- */

  /* draw spline edges or normal */
  check1 = gtk_check_button_new_with_label ("splines");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ check1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_splines)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check1), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check1), FALSE);
    }
  g_signal_connect (G_OBJECT (check1), "clicked", G_CALLBACK (check1_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check1);

  check2 = gtk_check_button_new_with_label ("singlenodes");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ check2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_no_singlenodes)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check2), FALSE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check2), TRUE);
    }
  g_signal_connect (G_OBJECT (check2), "clicked", G_CALLBACK (check2_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check2);

  check3 = gtk_check_button_new_with_label ("selfedges");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ check3,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_selfedges)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check3), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check3), FALSE);
    }
  g_signal_connect (G_OBJECT (check3), "clicked", G_CALLBACK (check3_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check3);

  /* */
  button1 = gtk_button_new_with_label ("fold");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ button1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  g_signal_connect (G_OBJECT (button1), "clicked", G_CALLBACK (button1_clicked), G_OBJECT (mainwindow1));
  gtk_widget_show (button1);

  /* */
  button2 = gtk_button_new_with_label ("unfold");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ button2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  g_signal_connect (G_OBJECT (button2), "clicked", G_CALLBACK (button2_clicked), G_OBJECT (mainwindow1));
  gtk_widget_show (button2);

  /* */
  button3 = gtk_button_new_with_label ("fit");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ button3,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  g_signal_connect (G_OBJECT (button3), "clicked", G_CALLBACK (button3_clicked), G_OBJECT (mainwindow1));
  gtk_widget_show (button3);

  /* */
  entry1 = gtk_text_view_new ();
  entry1buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry1));
  gtk_text_buffer_set_text (entry1buffer, "use file->open to open a graph file", -1);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (entry1), FALSE);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ entry1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (entry1);

  /* --- */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox3 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox3,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox3);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox3 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox3,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox3);
#endif

  /* --- */

  /* draw edgelabels or not */
  check4 = gtk_check_button_new_with_label ("edgelabels");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ check4,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_no_edgelabels == 0)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check4), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check4), FALSE);
    }
  g_signal_connect (G_OBJECT (check4), "clicked", G_CALLBACK (check4_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check4);

  /* draw multiple arrows at long edges or normal only one arrow */
  check5 = gtk_check_button_new_with_label ("multi-arrows");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ check5,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_multi_arrows)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check5), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check5), FALSE);
    }

  g_signal_connect (G_OBJECT (check5), "clicked", G_CALLBACK (check5_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check5);

  /* draw subgraph area rectangle if set */
  check6 = gtk_check_button_new_with_label ("subgraph-area");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ check6,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_subgraph_area)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check6), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check6), FALSE);
    }
  g_signal_connect (G_OBJECT (check6), "clicked", G_CALLBACK (check6_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check6);

  /* spin to change drawing spread factor. note that in gtk2 it is a (GtkObject *) and in gtk3 a (GtkAdjustment *) */
  xspinadjustment = (GtkAdjustment *) gtk_adjustment_new (xspreading /* initial value */ ,
							  0.5 /* minimum value */ ,
							  5.0 /* maximum value */ ,
							  0.1 /* step increment */ ,
							  0.1 /* page increment */ ,
							  0	/* page size now *must* be zero */
    );

  xspinbutton = gtk_spin_button_new (xspinadjustment, 0.1 /* climb rate */ ,
				     1 /* digits */ );

  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ xspinbutton,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  g_signal_connect (G_OBJECT (xspinadjustment), "value-changed", G_CALLBACK (xspin_changed), (gpointer) xspinbutton);
  gtk_widget_show (xspinbutton);

  xspinlabel = gtk_label_new ("x-spacing ");

  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ xspinlabel,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (xspinlabel);

  /* spin to change drawing spread factor. note that in gtk2 it is a (GtkObject *) and in gtk3 a (GtkAdjustment *) */
  yspinadjustment = (GtkAdjustment *) gtk_adjustment_new (yspreading /* initial value */ ,
							  0.5 /* minimum value */ ,
							  7.5 /* maximum value */ ,
							  0.1 /* step increment */ ,
							  0.1 /* page increment */ ,
							  0	/* page size now *must* be zero */
    );

  yspinbutton = gtk_spin_button_new (yspinadjustment, 0.1 /* climb rate */ ,
				     1 /* digits */ );

  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ yspinbutton,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  g_signal_connect (G_OBJECT (yspinadjustment), "value-changed", G_CALLBACK (yspin_changed), (gpointer) yspinbutton);
  gtk_widget_show (yspinbutton);

  yspinlabel = gtk_label_new ("y-spacing ");

  gtk_box_pack_start ( /* box */ GTK_BOX (hbox3), /* child */ yspinlabel,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (yspinlabel);

  /* --- */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox4 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox4,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox4);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox4 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox4,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox4);
#endif

  /* --- */

  /* follow edge in prio */
  check7 = gtk_check_button_new_with_label ("fe");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox4), /* child */ check7,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* follow edge in prio draw */
  if (option_feprio == 0)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check7), FALSE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check7), TRUE);
    }

  g_signal_connect (G_OBJECT (check7), "clicked", G_CALLBACK (check7_toggle), (gpointer) mainwindow1);
  gtk_widget_show (check7);

  /* up()/down() extra in prio */
  check8 = gtk_button_new_with_label ("pud");

//  check8 = gtk_check_button_new_with_label ("pud");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox4), /* child */ check8,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* follow edge in prio draw
  if (option_pud == 0)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check8), FALSE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check8), TRUE);
    }
*/

//  g_signal_connect (G_OBJECT (check8), "clicked", G_CALLBACK (check8_toggle), (gpointer) mainwindow1);

  g_signal_connect (G_OBJECT (check8), "clicked", G_CALLBACK (check8_clicked), G_OBJECT (mainwindow1));

  gtk_widget_show (check8);

  /* put on screen */
  gtk_widget_show (mainwindow1);

  /* clear old treeview window if any */
  on_treeview_window_destroy (NULL, NULL);

  /* draw node and edge data if valid data */
  drawnldata = NULL;
  draweldata = NULL;

  /* once layouter init at start */
  once_init ();

  /* clear all data just before parsing new data or random data */
  prepare4newdata ();

  /* optional graph filename to start with or a random generated graph */
  if (option_filename)
    {
      /* assume file name is oke to use from commandline */
      /* parse file as specified with commandline options */
      parse (option_filename);

      /* if initial file did not parse generate one */
      if (splay_tree_has_data (sp_parsednl) == 0)
	{
	  /* create random dag graph */
	  randomgraph ();
	}
    }
  else
    {
      /* create random dag graph at startup if no file */
      randomgraph ();
    }

  /* calculate on the initial graph */
  calculate_init ();

  /* calculate the graph */
  calculate ();

  /* update status text */
  update_status_text (NULL);

  /* set sliders to defaults */
  zfactor = 1.0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), 50);
  vxmin = 0;
  vymin = 0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);

  /* normal color drawing mode */
  altcolor = 0;

  /* draw node and edge data if valid data */
  drawnldata = drawnl;
  draweldata = drawel;

  /* interact gui */
  if (option_dryrun)
    {
      /* for testing do not start gui */
    }
  else
    {


      /* run the gui interaction */
      gtk_main ();
    }

  /* back to main() */

  return;
}

/* finally stop the gui */
static void
top_level_window_main_quit (void)
{

  /* run the gtk internal routine to stop gtk_main() which is a for(){} loop */
  gtk_main_quit ();

  return;
}

/* update status text */
static void
static_update_status_text (char *text)
{
  double zmaxx = 0.0;
  double zmaxy = 0.0;

  /* calculate scaled drawing size */
  zmaxx = zfactor * maxx;
  zmaxy = zfactor * maxy;

  /* update status text line */
  memset (charentry1buffer, 0, 64);

  /* */
  if (text)
    {
      snprintf (charentry1buffer, 64 - 1, "%s", text);
      printf ("%s(): %s\n", __FUNCTION__, text);
      fflush (stdout);
    }
  else
    {
      snprintf (charentry1buffer, 64 - 1, "%d nodes, %d edges, size (%d,%d)", n_drawing_nodes, n_drawing_edges, (int) zmaxx, (int) zmaxy);
    }

  /* when running in console mode there is no entry1buffer */
  if (entry1buffer)
    {
      gtk_text_buffer_set_text (entry1buffer, charentry1buffer, -1);
      /* it is visible in the gui */

      /* only a re-draw needed */
      gtk_widget_queue_draw (drawingarea1);

    }

  return;
}

/* 'open' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void
on_top_level_window_open1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  GtkWidget *dialog = (GtkWidget *) 0;
  char *file_chooser_filename = (char *) 0;
  char *file_chooser_dir = (char *) 0;
  GtkFileChooser *chooser = NULL;
  char *inputfilename = (char *) 0;
  char *sfn = NULL;

  if (menuitem)
    {
    }
  if (user_data)
    {
    }

#if GTK_HAVE_API_VERSION_2 == 1

  /* see gimp source code howto */
  dialog = gtk_file_chooser_dialog_new ("Select Data File", 0,	/* parent_window */
					GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

#endif

#if GTK_HAVE_API_VERSION_3 == 1

  /* see gimp source code howto */
  dialog = gtk_file_chooser_dialog_new ("Select Data File", GTK_WINDOW (mainwindow1) /* parent_window */ ,
					GTK_FILE_CHOOSER_ACTION_OPEN, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);

#endif

  chooser = GTK_FILE_CHOOSER (dialog);

  /* use same dir if opened in earlier dir */
  if (lastdir)
    {
      gtk_file_chooser_set_current_folder (chooser, lastdir);
    }

  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (mainwindow1));

  /* run the window to select a input file */
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
      /* open button */
      file_chooser_filename = (char *) gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
      file_chooser_dir = (char *) gtk_file_chooser_get_current_folder (chooser);
    }
  else
    {
      /* cancel button */
      (void) gtk_widget_destroy (dialog);
      return;
    }

  /* */
  (void) gtk_widget_destroy (dialog);

  /* update last-used-dir */
  if (file_chooser_dir)
    {
      if (lastdir)
	{
	  (void) g_free (lastdir);
	}
      lastdir = strdup (file_chooser_dir);
      (void) g_free (file_chooser_dir);
    }

  /* copy the input filename from gtk */
  if (file_chooser_filename)
    {
      inputfilename = strdup (file_chooser_filename);
      (void) g_free (file_chooser_filename);
    }
  else
    {
      return;
    }

  /* clear old treeview window if any */
  on_treeview_window_destroy (NULL, NULL);

  /* draw node and edge data if valid data */
  drawnldata = NULL;
  draweldata = NULL;

  /* clear all data just before parsing new data or random data */
  prepare4newdata ();

  /* check open file name */
  sfn = valid_open_filename (inputfilename);

  /* parse file as specified with the open dialog */
  parse (sfn);

  /* calculate on the initial graph */
  calculate_init ();

  /* calculate the graph */
  calculate ();

  /* update status text */
  update_status_text (NULL);

  /* set sliders to defaults */
  zfactor = 1.0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), 50);

  vxmin = 0;
  vymin = 0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);

  /* normal color drawing mode */
  altcolor = 0;

  /* draw node and edge data if valid data */
  drawnldata = drawnl;
  draweldata = drawel;

  /* filename is not saved */
  (void) g_free (inputfilename);

  /* re draw screen */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* 'new' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void
on_top_level_window_new1_activate (GtkMenuItem * menuitem, gpointer user_data)
{

  if (menuitem)
    {
    }

  if (user_data)
    {
    }

  /* clear old treeview window if any */
  on_treeview_window_destroy (NULL, NULL);

  /* draw node and edge data if valid data */
  drawnldata = NULL;
  draweldata = NULL;

  /* clear all data just before parsing new data or random data */
  prepare4newdata ();

  /* create random dag graph */
  randomgraph ();

  /* calculate on the initial graph */
  calculate_init ();

  /* calculate the graph */
  calculate ();

  /* update status text */
  update_status_text (NULL);

  /* set sliders to defaults */
  zfactor = 1.0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), 50);
  vxmin = 0;
  vymin = 0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);

  /* normal color drawing mode */
  altcolor = 0;

  /* draw node and edge data if valid data */
  drawnldata = drawnl;
  draweldata = drawel;

  /* fit drawing in window */
  dofit ();

  /* re draw screen */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* 'quit' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void
on_top_level_window_quit1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  top_level_window_main_quit ();
  return;
}

/* left slider zoom factor */
static void
on_vscale1_changed (GtkAdjustment * adj)
{
  gdouble val0 = 0.0;
  int val1 = 0;
  if (adj)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  val0 = gtk_adjustment_get_value (adj);
  val1 = (int) val0;
  zfactor = exp ((double) (3 * (val1 - 50)) / (double) 50);
  if (option_gdebug > 1)
    {
      printf ("%s(): zoomslider=%d zoomfactor=%f\n", __FUNCTION__, val1, zfactor);
      fflush (stdout);
    }

  /* status text depends on zfactor */
  update_status_text (NULL);

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* right slider y offset 0...100% */
static void
on_vscale2_changed (GtkAdjustment * adj)
{
  gdouble val = 0.0;
  if (adj)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  val = gtk_adjustment_get_value (adj);
  vymin = (int) ((val * maxy) / 100);
  if (option_gdebug > 1)
    {
      printf ("%s(): yslider=%d vymin=%d (maxy=%d)\n", __FUNCTION__, (int) val, vymin, maxy);
      fflush (stdout);
    }
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* bottom slider x offset 0...100% */
static void
on_hscale1_changed (GtkAdjustment * adj)
{
  gdouble val = 0.0;
  if (adj)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  val = gtk_adjustment_get_value (adj);
  vxmin = (int) ((val * maxx) / 100);
  if (option_gdebug > 1)
    {
      printf ("%s(): xslider=%d vxmin=%d (maxx=%d)\n", __FUNCTION__, (int) val, vxmin, maxx);
      fflush (stdout);
    }
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 1 is 'splines' */
static void
check1_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* toggle the splines option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_splines = 1;
    }
  else
    {
      option_splines = 0;
    }
  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 2 is 'singlenodes' */
static void
check2_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* toggle the selfedges option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_no_singlenodes = 0;
    }
  else
    {
      option_no_singlenodes = 1;
    }
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-layout */
  calculate ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 3 is 'selfedges' */
static void
check3_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* toggle the selfedges option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_selfedges = 1;
    }
  else
    {
      option_selfedges = 0;
    }
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-layout */
  calculate ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 4 is 'edgelabels' */
static void
check4_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* if no edgelabels in input then this has no effect */
  if (n_input_edgelabels == 0)
    {
      if (option_gdebug)
	{
	  printf ("%s(): %d edge labels\n", __FUNCTION__, n_input_edgelabels);
	  fflush (stdout);
	}
      return;
    }
  /* toggle the edgelabels option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_no_edgelabels = 0;
    }
  else
    {
      option_no_edgelabels = 1;
    }
  if (option_gdebug)
    {
      printf ("%s(): %d edge labels and option_no_edgelabels=%d\n", __FUNCTION__, n_input_edgelabels, option_no_edgelabels);
      fflush (stdout);
    }
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-layout */
  calculate ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 5 is 'multi-arrows' */
static void
check5_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* toggle the multi edges option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_multi_arrows = 1;
    }
  else
    {
      option_multi_arrows = 0;
    }
  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 6 is 'subgraph-area' */
static void
check6_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  /* toggle the subgraph-area option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_subgraph_area = 1;
    }
  else
    {
      option_subgraph_area = 0;
    }

  /* if no subgraphs in input then this has no effect */
  if (n_input_subgraphs == 0)
    {
      return;
    }

  /* set color drawing to normal */
  reset_altcolor ();

  /* re-layout */
  calculate ();

  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* checkbox 7 is 'followedge' */
static void
check7_toggle (GtkWidget * widget, gpointer window)
{

  if (widget)
    {
    }

  if (window)
    {
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  /* toggle the option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_feprio = 1;
    }
  else
    {
      option_feprio = 0;
    }

  /* set color drawing to normal */
  reset_altcolor ();

  /* re-layout */
  calculate ();

  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

static void
check8_clicked (GtkWidget * widget, gpointer window)
{

  if (widget)
    {
    }

  if (window)
    {
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

      option_pud++;

  if (option_pud > 3)
    {
      option_pud = 0;
    }

  printf ("%s(): pud=%d\n", __FUNCTION__, option_pud);

  /* set color drawing to normal */
  reset_altcolor ();

  /* re-layout */
  calculate ();

  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}


/* xspinbutton adjustment 'xspreading' changed */
static void
xspin_changed (GtkWidget * widget, gpointer spinbutton)
{
  gfloat val = 0.0;
  if (widget)
    {
    }
  if (spinbutton)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* get the value for the new drawing spreading factor */
  val = gtk_spin_button_get_value ((GtkSpinButton *) spinbutton);
  if (option_gdebug)
    {
      printf ("%s(): changing spreading factor from %f to val=%f\n", __FUNCTION__, (float) xspreading, val);
      fflush (stdout);
    }

  /* set new spreading factor as double */
  xspreading = (double) val;

  /* set color drawing to normal */
  reset_altcolor ();

  /* re-position the new graph */
  layouter2draw_pos ();

  /* graph properties update */
  static_update_status_text (NULL);

  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* yspinbutton adjustment 'yspreading' changed */
static void
yspin_changed (GtkWidget * widget, gpointer spinbutton)
{
  gfloat val = 0.0;
  if (widget)
    {
    }
  if (spinbutton)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* get the value for the new drawing spreading factor */
  val = gtk_spin_button_get_value ((GtkSpinButton *) spinbutton);
  if (option_gdebug)
    {
      printf ("%s(): changing spreading factor from %f to val=%f\n", __FUNCTION__, (float) yspreading, val);
      fflush (stdout);
    }

  /* set new spreading factor as double */
  yspreading = (double) val;

  /* set color drawing to normal */
  reset_altcolor ();

  /* re-position the new graph */
  layouter2draw_pos ();

  /* graph properties update */
  static_update_status_text (NULL);

  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* button1 fold all subgraphs at once */
static void
button1_clicked (GtkWidget * widget, gpointer window)
{

  if (widget)
    {
    }

  if (window)
    {
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  /* check subgraph data */
  if (splay_tree_has_data (sp_parsedsgl))
    {
      /* fold all */
      folding_all (1);
      /* set color drawing to normal */
      reset_altcolor ();
      /* re-layout */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }

  return;
}

/* button2 unfold all subgraphs at once */
static void
button2_clicked (GtkWidget * widget, gpointer window)
{

  if (widget)
    {
    }

  if (window)
    {
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  /* check subgraph data */
  if (splay_tree_has_data (sp_parsedsgl))
    {
      /* unfold all */
      folding_all (0);
      /* set color drawing to normal */
      reset_altcolor ();
      /* re-layout */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }

  return;
}

/* */
static void
dofit (void)
{
  double xzscale = 1.0;
  double yzscale = 1.0;
  double newzscale = 1.0;
  double dval = 1.0;
  int val = 0;

  xzscale = (double) (1000 * drawing_area_xsize / maxx);
  yzscale = (double) (1000 * drawing_area_ysize / maxy);
  xzscale = xzscale / 1000.0;
  yzscale = yzscale / 1000.0;
  if ((xzscale - yzscale) > 0)
    {
      newzscale = yzscale;
    }
  else
    {
      newzscale = xzscale;
    }

  if (option_gdebug)
    {
      printf ("%s(): fit zoom to %f from xscale=%f and yscale=%f drawingarea=(%d,%d) maxy=(%d,%d)\n", __FUNCTION__, newzscale,
	      yzscale, xzscale, drawing_area_xsize, drawing_area_ysize, maxx, maxy);
    }

  dval = log ((newzscale * (double) 50.0) / 3.0) - 50.0;
  dval = log (newzscale) / 3.0;
  dval = (dval * 50.0);
  dval = dval + 50.0;
  val = (int) dval;
  if (val < 0)
    {
      val = 0;
    }
  if (val > 100)
    {
      val = 100;
    }

  zfactor = exp ((double) (3 * (val - 50)) / (double) 50);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), val);
  if (option_gdebug)
    {
      printf ("%s(): new slider value is %d (dval=%f) zfactor=%f\n", __FUNCTION__, val, dval, zfactor);
    }

  /* reset v xy min */
  vxmin = 0;
  vymin = 0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);

  return;
}

/* button3 fit whole drawing in drawing area */
static void
button3_clicked (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  dofit ();
  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* dragging drawing when left button 1 is held down */
static gboolean
on_motion_notify_event (GtkWidget * widget, GdkEventMotion * event)
{
  int x = 0;
  int y = 0;
  int idx = 0;
  int idy = 0;
  double gdelta = 0.0;
  double gsld = 0.0;
  double val = 0.0;
  int ival = 0;
  double dhw = 0.0;
  double dx = 0.0;
  double dy = 0.0;
  double hw = 0.0;
  double hh = 0.0;
  GdkModifierType state;
  if (event)
    {
      /* */
    }

  /* where mouse click is on window and mouse status */
#if GTK_HAVE_API_VERSION_2 == 1
  gdk_window_get_pointer (widget->window, &x, &y, &state);
#endif
#if GTK_HAVE_API_VERSION_3 == 1
  gdk_window_get_device_position (gtk_widget_get_window (widget), event->device, &x, &y, &state);
#endif
  if ((state & GDK_BUTTON1_MASK) != 0)
    {
      idx = (mouse_oldx - x);
      idy = (mouse_oldy - y);
      if (option_gdebug)
	{
	  printf ("%s(): now at (%d,%d) delta is (%d,%d)\n", __FUNCTION__, x, y, idx, idy);
	  fflush (stdout);
	}

      if ((idx == 0) && (idy == 0))
	{
	  return (TRUE);
	}

      if (idx != 0)
	{
	  hw = (double) (x);
	  dx = (double) (mouse_oldx + vxmin) / zfactor;
	  dhw = ((double) (hw + vxmin) / zfactor);
	  gdelta = dx - dhw;
	  vxmin = vxmin + (int) gdelta;
	  if (vxmin < 0)
	    {
	      vxmin = 0;
	    }
	  if (vxmin > maxx)
	    {
	      vxmin = maxx;
	    }
	  gsld = gdelta;
	  gsld = (gdelta / maxx);
	  gsld = (gsld * 100);
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
	  ival = (int) val;
	  ival = ival + (int) gsld;
	  if (ival < 0)
	    {
	      ival = 0;
	    }
	  if (ival > 100)
	    {
	      ival = 100;
	    }
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), ival);
	}

      if (idy != 0)
	{
	  hh = (double) (y);
	  dy = (double) (mouse_oldy + vymin) / zfactor;
	  dhw = (hh + vymin) / zfactor;
	  gdelta = dy - dhw;
	  vymin = vymin + (int) gdelta;
	  if (vymin < 0)
	    {
	      vymin = 0;
	    }
	  if (vymin > maxy)
	    {
	      vymin = maxy;
	    }
	  gsld = gdelta;
	  gsld = gdelta / maxy;
	  gsld = gsld * 100;
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
	  ival = (int) val;
	  ival = ival + (int) gsld;
	  if (ival < 0)
	    {
	      ival = 0;
	    }
	  if (ival > 100)
	    {
	      ival = 100;
	    }
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), ival);
	}

      /* save */
      mouse_oldx = x;
      mouse_oldy = y;
      /* only redraw needed */
      gtk_widget_queue_draw (drawingarea1);
    }

  return (TRUE);
}

/* mouse click on drawing area
 *
 * guint event->type has this info
 * enum GdkEventType
 * {
 * .
 * GDK_BUTTON_PRESS	= 4, (single click)
 * GDK_2BUTTON_PRESS	= 5, (double click)
 * GDK_3BUTTON_PRESS	= 6, (triple click)
 * GDK_BUTTON_RELEASE	= 7, (released)
 * ...
 * };
 * which button is clicked is in guint event->button;
 * the button which was pressed or released, numbered from 1 to 5.
 * Normally button 1 is the left mouse button,
 * 2 is the middle button, and 3 is the right button.
 * On 2-button mice, the middle button can often be simulated
 * by pressing both mouse buttons together.
 * for dragging the mouse (x,y) is saved.
 */
static gboolean
on_mouse_clicked (GtkWidget * widget, GdkEventButton * event, gpointer user_data)
{
  int eventbutton = 0;
  double dx = 0.0;
  double dy = 0.0;
  int nx = 0;
  int ny = 0;
  int ex = 0;
  int ey = 0;
  if (widget)
    {
    }
  if (user_data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return (TRUE);
    }
  /* get where clicked in the drawing */
  ex = (event->x);
  ey = (event->y);
  dx = ((double) ex / zfactor);
  dy = ((double) ey / zfactor);
  nx = ((int) dx) + vxmin;
  ny = ((int) dy) + vymin;
  /* check the buttons */
  eventbutton = (int) (event->button);
  /* option to modify is left shift key is pressed */
  if (l_s_k_d)
    {
      /* change button */
      if (eventbutton == 1)
	{
	  /* change left button in upper side button */
	  eventbutton = 9;
	}
      else if (eventbutton == 2)
	{
	  /* change middle button in ... */
	}
      else if (eventbutton == 3)
	{
	  /* change right button in lower side button */
	  eventbutton = 8;
	}
      else
	{
	  /* other modifications here */
	}
    }
  /* check the buttons after modification */
  if (eventbutton == 1)
    {
      /* left button clicked fold/unfold */
      /* left button drag drawing using motion_notify_event */
      mouse_oldx = ex;
      mouse_oldy = ey;
      /* fold/unfold subgraph if clicked at node */
      on_mouse_clicked_button1 (nx, ny);
      return (TRUE);
    }
  if (eventbutton == 2)
    {
      /* middle button clicked draw in grey and hi-light node */
      on_mouse_clicked_button2 (nx, ny);
      return (TRUE);
    }
  if (eventbutton == 3)
    {
      /* right button clicked center point on screen */
      on_mouse_clicked_button3 (event, dx, dy);
      return (TRUE);
    }
  if (eventbutton == 4)
    {
      /* button 4 clicked */
      return (TRUE);
    }
  if (eventbutton == 5)
    {
      /* button 5 clicked */
      return (TRUE);
    }
  if (eventbutton == 6)
    {
      return TRUE;
    }
  if (eventbutton == 7)
    {
      return TRUE;
    }
  if (eventbutton == 8)
    {
      /* lower side button */
      on_mouse_clicked_button8 ();
      return TRUE;
    }
  if (eventbutton == 9)
    {
      /* upper side button */
      on_mouse_clicked_button9 ();
      return TRUE;
    }
  /* unknown button */
  return TRUE;
}

/* in drawing clicked at (nx,ny) and toggle folding if there is a node or summary node */
static void
on_mouse_clicked_button1 (int nx, int ny)
{
  int i;
  struct dli *lp = NULL;
  struct ddn *nptr = NULL;
  struct unode *un = NULL;
  const char *rootname = "";
  const char *fs = "";
  if (option_gdebug)
    {
      printf ("%s(): checking for node at (%d,%d) in drawing\n", __FUNCTION__, nx, ny);
      fflush (stdout);
    }

  /* found node */
  un = NULL;
  /* search for node at location (nx,ny) */
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* check only drawed levels */
      if (lp->draw)
	{
	  /* check y within nodes in layer */
	  if ((ny >= lp->y0) && (ny <= (lp->y0 + lp->hn)))
	    {
	      /* scan nodes in this layer */
	      nptr = lp->nl;
	      while (nptr)
		{
		  if (option_gdebug > 1)
		    {
		      printf ("%s(): checking node \"%s\"\n", __FUNCTION__, nptr->dn->un->name);
		      fflush (stdout);
		    }
		  /* check node coordinate */
		  if ((ny >= nptr->dn->un->y1) && (ny <= nptr->dn->un->y2))
		    {
		      if ((nx >= nptr->dn->un->x0) && (nx <= (nptr->dn->un->x0 + nptr->dn->un->bbx)))
			{
			  un = nptr->dn->un;
			  break;
			}
		    }
		  nptr = nptr->next;
		}
	    }
	}
      /* if found do not search more */
      if (un)
	{
	  break;
	}
    }

  /* ignore if dummy node */
  if (un)
    {
      if (un->bitflags.dummynode)
	{
	  return;
	}
    }

  /* ignore if edge label node */
  if (un)
    {
      if (un->bitflags.edgelabel)
	{
	  return;
	}
    }

  /* ignore if regular node in root graph */
  if (un)
    {
      if ((un->bitflags.sumnode == 0) && (un->rootedon == NULL))
	{
	  return;
	}
    }

  /* */
  if (un)
    {
      if (option_gdebug)
	{
	  if (un->rootedon == NULL)
	    {
	      rootname = "rootgraph";
	    }
	  else
	    {
	      rootname = un->rootedon->name;
	    }
	  printf ("%s(): found node \"%s\" label \"%s\" at (%d,%d) in drawing rooted in %s\n", __FUNCTION__, un->name, un->label, nx, ny, rootname);
	  fflush (stdout);
	}
    }

  /* toggle folding. node is rooted on a subgraph. */
  if (un)
    {
      /* check if summary node of subgraph */
      if (un->bitflags.sumnode == 1)
	{
	  /* this is a summary node */
	  if (option_gdebug)
	    {
	      printf ("%s(): this is a summarynode of subgraph \"%s\"\n", __FUNCTION__, un->rootedon->name);
	      fflush (stdout);
	    }
	}
      /* toggle folding */
      if (un->rootedon->bitflags.folded == 1)
	{
	  /* unfolded */
	  un->rootedon->bitflags.folded = 0;
	}
      else
	{
	  /* folded */
	  un->rootedon->bitflags.folded = 1;
	}
      if (option_gdebug)
	{
	  if (un->rootedon->bitflags.folded)
	    {
	      fs = "folded";
	    }
	  else
	    {
	      fs = "unfolded";
	    }
	  printf ("%s(): changed folding status of subgraph \"%s\" to \"%s\"\n", __FUNCTION__, un->rootedon->name, fs);
	  fflush (stdout);
	}
      /* set color drawing to normal */
      reset_altcolor ();
      /* calculate the new graph */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }

  return;
}

/* reset alt color draw mode */
static int
reset_altcolor (void)
{
  int i = 0;
  struct dli *lp = NULL;
  struct ddn *nptr = NULL;
  struct dde *eptr = NULL;
  if (option_gdebug)
    {
      printf ("%s(): altcolor=%d and setting to 0\n", __FUNCTION__, altcolor);
      fflush (stdout);
    }

  /* ready if not in alt mode */
  if (altcolor == 0)
    {
      /* no changes */
      return (0);
    }

  /* clear alt color mode */
  altcolor = 0;
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* scan nodes in this layer */
      nptr = lp->nl;
      while (nptr)
	{
	  nptr->dn->un->bitflags.altcolor = 0;
	  nptr = nptr->next;
	}
      /* scan edges in this layer */
      eptr = lp->el;
      while (eptr)
	{
	  eptr->de->ue->bitflags.altcolor = 0;
	  eptr = eptr->next;
	}
    }

  /* changed drawing */
  return (1);
}

/* set alt color draw mode */
static int
set_altcolor (void)
{
  int i = 0;
  struct dli *lp = NULL;
  struct ddn *nptr = NULL;
  struct dde *eptr = NULL;
  if (option_gdebug)
    {
      printf ("%s(): altcolor=%d and setting to 1\n", __FUNCTION__, altcolor);
      fflush (stdout);
    }

  /* ready if already in alt mode */
  if (altcolor == 1)
    {
      /* no changes */
      return (0);
    }

  /* set alt color mode */
  altcolor = 1;
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* scan nodes in this layer */
      nptr = lp->nl;
      while (nptr)
	{
	  nptr->dn->un->bitflags.altcolor = 1;
	  nptr = nptr->next;
	}
      /* scan edges in this layer */
      eptr = lp->el;
      while (eptr)
	{
	  eptr->de->ue->bitflags.altcolor = 1;
	  eptr = eptr->next;
	}
    }

  /* changed drawing */
  return (1);
}

/* hilite outgoing edges */
static void
fdummyout (struct unode *un)
{
  splay_tree_node spn = (splay_tree_node) 0;
  struct uedge *ue = (struct uedge *) 0;

  if (un == NULL)
    {
      return;
    }

  if ((un->bitflags.dummynode == 0) || (un->bitflags.edgelabel == 0))
    {
      return;
    }

  if (splay_tree_has_data (un->sp_poe) == 0)
    {
      return;
    }

  spn = splay_tree_min (un->sp_poe);

  while (spn)
    {
      ue = (struct uedge *) spn->value;

      if (ue->fn == un)
	{
	  if (option_toggle_altcolor)
	    {
	      if (ue->bitflags.altcolor)
		{
		  ue->bitflags.altcolor = 0;
		}
	      else
		{
		  ue->bitflags.altcolor = 1;
		}
	    }
	  else
	    {
	      ue->bitflags.altcolor = 0;
	    }
	  /* */
	  if (ue->tn->bitflags.dummynode == 1)
	    {
	      fdummyout (ue->tn);
	    }
	  else
	    {
	      if (option_toggle_altcolor)
		{
		  if (ue->tn->bitflags.altcolor)
		    {
		      ue->tn->bitflags.altcolor = 0;
		    }
		  else
		    {
		      ue->tn->bitflags.altcolor = 1;
		    }
		}
	      else
		{
		  ue->tn->bitflags.altcolor = 0;
		}
	      /* follow edge label */
	      if (ue->tn->bitflags.edgelabel == 1)
		{
		  fdummyout (ue->tn);
		  fdummyin (ue->tn);
		}
	    }
	}

      spn = splay_tree_successor (un->sp_poe, spn->key);
    }

  return;
}

/* hilite incoming edges */
static void
fdummyin (struct unode *un)
{
  splay_tree_node spn = (splay_tree_node) 0;
  struct uedge *ue = (struct uedge *) 0;

  if (un == NULL)
    {
      return;
    }

  if ((un->bitflags.dummynode == 0) || (un->bitflags.edgelabel == 0))
    {
      return;
    }

  if (splay_tree_has_data (un->sp_poe) == 0)
    {
      return;
    }

  spn = splay_tree_min (un->sp_poe);

  while (spn)
    {
      ue = (struct uedge *) spn->value;

      if (ue->tn == un)
	{
	  if (option_toggle_altcolor)
	    {
	      if (ue->bitflags.altcolor)
		{
		  ue->bitflags.altcolor = 0;
		}
	      else
		{
		  ue->bitflags.altcolor = 1;
		}
	    }
	  else
	    {
	      ue->bitflags.altcolor = 0;
	    }

	  if (ue->fn->bitflags.dummynode == 1)
	    {
	      fdummyin (ue->fn);
	    }
	  else
	    {
	      if (option_toggle_altcolor)
		{
		  if (ue->fn->bitflags.altcolor)
		    {
		      ue->fn->bitflags.altcolor = 0;
		    }
		  else
		    {
		      ue->fn->bitflags.altcolor = 1;
		    }
		}
	      else
		{
		  ue->fn->bitflags.altcolor = 0;
		}
	      /* follow edge label */
	      if (ue->fn->bitflags.edgelabel == 1)
		{
		  fdummyin (ue->fn);
		  fdummyout (ue->fn);
		}
	    }
	}

      spn = splay_tree_successor (un->sp_poe, spn->key);
    }

  return;
}

/* in drawing clicked at (nx,ny) and toggle alt color drawing */
static void
on_mouse_clicked_button2 (int nx, int ny)
{
  int i;
  struct dli *lp = NULL;
  struct ddn *nptr = NULL;
  struct unode *un = NULL;
  struct uedge *ue = (struct uedge *) 0;
  splay_tree_node spn = (splay_tree_node) 0;

  if (option_gdebug)
    {
      printf ("%s(): checking for node at (%d,%d) in drawing max (%d,%d) vxmin=%d vymin=%d zfactor=%f\n", __FUNCTION__, nx, ny,
	      maxx, maxy, vxmin, vymin, zfactor);
      fflush (stdout);
    }

  /* found node */
  un = NULL;
  /* search for node at location (nx,ny) */
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* check only drawed levels */
      if (lp->draw)
	{
	  /* check y within nodes in layer */
	  if ((ny >= lp->y0) && (ny <= (lp->y0 + lp->hn)))
	    {
	      /* scan nodes in this layer */
	      nptr = lp->nl;
	      while (nptr)
		{
		  /* check node coordinate */
		  if ((ny >= nptr->dn->un->y1) && (ny <= nptr->dn->un->y2))
		    {
		      if ((nx >= nptr->dn->un->x0) && (nx <= (nptr->dn->un->x0 + nptr->dn->un->bbx)))
			{
			  un = nptr->dn->un;
			  break;
			}
		    }
		  nptr = nptr->next;
		}
	    }
	}
      /* if found do not search more */
      if (un)
	{
	  break;
	}
    }

  if (option_gdebug)
    {
      if (un)
	{
	  printf ("%s(): checking for node at (%d,%d) in drawing found node %s label %s\n", __FUNCTION__, nx, ny, un->name, un->label);
	  fflush (stdout);
	}
      else
	{
	  printf ("%s(): not found node\n", __FUNCTION__);
	  fflush (stdout);
	}
    }

  /* ignore if dummy node */
  if (un)
    {
      if (un->bitflags.dummynode)
	{
	  if (reset_altcolor ())
	    {
	      /* re-draw needed */
	      gtk_widget_queue_draw (drawingarea1);
	    }
	  return;
	}
    }

  /* ignore if edge label node */
  if (un)
    {
      if (un->bitflags.edgelabel)
	{
	  if (reset_altcolor ())
	    {
	      /* re-draw needed */
	      gtk_widget_queue_draw (drawingarea1);
	    }
	  return;
	}
    }

  /* if not clicked on a node then clear altcolor mode */
  if (un == NULL)
    {
      if (reset_altcolor ())
	{
	  /* re-draw needed */
	  gtk_widget_queue_draw (drawingarea1);
	}
      return;
    }

  /* switch to alt color drawing mode */
  if (set_altcolor ())
    {
      /* re-draw needed */
    }

  /* option here to toggle altcolor bit everywhere instead of setting to 0 */

  /* option here to copy all highlight nodes and edges and subgraphs
   * as stand-alone separate graph and layout that in other window.
   * Multiple layout engines at once would be best solution for this.
   */

  /* set this node in highlite color */
  if (option_toggle_altcolor)
    {
      if (un->bitflags.altcolor)
	{
	  un->bitflags.altcolor = 0;
	}
      else
	{
	  un->bitflags.altcolor = 1;
	}
    }
  else
    {
      un->bitflags.altcolor = 0;
    }

  /* check which edges connect to this node */
  if (splay_tree_has_data (un->sp_poe))
    {

      spn = splay_tree_min (un->sp_poe);

      while (spn)
	{
	  ue = (struct uedge *) spn->value;

	  /* draw edge in normal color */
	  if (option_toggle_altcolor)
	    {
	      if (ue->bitflags.altcolor)
		{
		  ue->bitflags.altcolor = 0;
		}
	      else
		{
		  ue->bitflags.altcolor = 1;
		}
	    }
	  else
	    {
	      ue->bitflags.altcolor = 0;
	    }
	  /* draw node connecting in normal color */
	  if (option_toggle_altcolor)
	    {
	      if (ue->fn->bitflags.altcolor)
		{
		  ue->fn->bitflags.altcolor = 0;
		}
	      else
		{
		  ue->fn->bitflags.altcolor = 1;
		}
	    }
	  else
	    {
	      ue->fn->bitflags.altcolor = 0;
	    }
	  if (option_toggle_altcolor)
	    {
	      if (ue->tn->bitflags.altcolor)
		{
		  ue->tn->bitflags.altcolor = 0;
		}
	      else
		{
		  ue->tn->bitflags.altcolor = 1;
		}
	    }
	  else
	    {
	      ue->tn->bitflags.altcolor = 0;
	    }
	  /* follow dummy nodes and edge labels */
	  if ((ue->fn->bitflags.dummynode == 1) || (ue->fn->bitflags.edgelabel == 1))
	    {
	      fdummyout (ue->fn);
	      fdummyin (ue->fn);
	    }
	  if ((ue->tn->bitflags.dummynode == 1) || (ue->tn->bitflags.edgelabel == 1))
	    {
	      fdummyout (ue->tn);
	      fdummyin (ue->tn);
	    }

	  spn = splay_tree_successor (un->sp_poe, spn->key);
	}

    }

  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* in drawing clicked at (nx,ny) and move that point to center of screen if possible */
static void
on_mouse_clicked_button3 (GdkEventButton * event, double dx, double dy)
{
  gint w = 0;
  gint h = 0;
  int hw = 0;
  int hh = 0;
  int nx = 0;
  int ny = 0;
  double dhw = 0.0;
  double gdelta = 0.0;
  double gsld = 0.0;
  double val = 0.0;
  int ival = 0;
  nx = (int) dx;
  ny = (int) dy;
  if (event)
    {
      /* */
    }

  /* how large drawing area is */
#if GTK_HAVE_API_VERSION_2 == 1
  (void) gdk_drawable_get_size (event->window, &w, &h);
#endif
#if GTK_HAVE_API_VERSION_3 == 1
  w = gtk_widget_get_allocated_width (drawingarea1);
  h = gtk_widget_get_allocated_height (drawingarea1);
#endif
  /* center of drawing area */
  hw = w / 2;
  hh = h / 2;
  if (nx < maxx)
    {
      dhw = (hw + vxmin) / zfactor;
      gdelta = dx - dhw;
      vxmin = vxmin + (int) gdelta;
      if (vxmin < 0)
	{
	  vxmin = 0;
	}
      gsld = gdelta;
      gsld = gdelta / maxx;
      gsld = gsld * 100;
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
      ival = (int) val;
      ival = ival + (int) gsld;
      if (ival < 0)
	{
	  ival = 0;
	}
      if (ival > 100)
	{
	  ival = 100;
	}
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), ival);
    }

  if (ny < maxy)
    {
      dhw = (hh + vymin) / zfactor;
      gdelta = dy - dhw;
      vymin = vymin + (int) gdelta;
      if (vymin < 0)
	{
	  vymin = 0;
	}
      gsld = gdelta;
      gsld = gdelta / maxy;
      gsld = gsld * 100;
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
      ival = (int) val;
      ival = ival + (int) gsld;
      if (ival < 0)
	{
	  ival = 0;
	}
      if (ival > 100)
	{
	  ival = 100;
	}
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), ival);
    }

  /* only redraw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* change zoom factor */
static void
on_mouse_clicked_button8 (void)
{
  gdouble val0 = 0.0;
  int val1 = 0;
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  val0 = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale1));
  val1 = (int) val0;
  if (val1 > 99)
    {
      return;
    }
  val1 = val1 + 1;
  zfactor = exp ((double) (3 * (val1 - 50)) / (double) 50);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), val1);

  /* status text depends on zfactor */
  update_status_text (NULL);

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* change zoom factor */
static void
on_mouse_clicked_button9 (void)
{
  gdouble val0 = 0.0;
  int val1 = 0;
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  val0 = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale1));
  val1 = (int) val0;
  if (val1 < 1)
    {
      return;
    }
  val1 = val1 - 1;
  zfactor = exp ((double) (3 * (val1 - 50)) / (double) 50);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), val1);

  /* status text depends on zfactor */
  update_status_text (NULL);

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/*
 * wheel of mouse movement
 * enum GdkScrollDirection
 * Specifies the direction for GdkEventScroll.
 * Members
 * GDK_SCROLL_UP the window is scrolled up.
 * GDK_SCROLL_DOWN the window is scrolled down.
 * GDK_SCROLL_LEFT the window is scrolled to the left.
 * GDK_SCROLL_RIGHT the window is scrolled to the right.
 * GDK_SCROLL_SMOOTH the scrolling is determined by the
 * delta values in GdkEventScroll. See gdk_event_get_scroll_deltas(). Since: 3.4
 */
static gboolean
on_mouse_wheel (GtkWidget * widget, GdkEventScroll * event, gpointer user_data)
{
  gdouble val = 0.0;
  if (widget)
    {
    }
  if (event)
    {
    }
  if (user_data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return (TRUE);
    }
  /* check for left shift key down to use scroll up/down as left/right */
  if (l_s_k_d)
    {
      /* wheel up/down changed in left/right scroll because left shift key is pressed */
      if (event->direction == GDK_SCROLL_UP)
	{
	  /* upward direction changed into rigth direction */
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
	  if (val == 100)
	    {
	      return TRUE;
	    }
	  val = val + 1;
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
	  vxmin = (int) ((val * maxx) / 100);
	  gtk_widget_queue_draw (drawingarea1);
	  return (TRUE);
	}
      /* */
      if (event->direction == GDK_SCROLL_DOWN)
	{
	  /* upward direction changed into left direction */
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
	  if (val == 0)
	    {
	      return TRUE;
	    }
	  val = val - 1;
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
	  vxmin = (int) ((val * maxx) / 100);
	  gtk_widget_queue_draw (drawingarea1);
	  return (TRUE);
	}
    }
  else
    {
      /* up down scroll */
      if (event->direction == GDK_SCROLL_UP)
	{
	  /* upward direction */
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
	  if (val == 0)
	    {
	      return TRUE;
	    }
	  val = val - 1;
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), val);
	  vymin = (int) ((val * maxy) / 100);
	  gtk_widget_queue_draw (drawingarea1);
	  return (TRUE);
	}
      /* */
      if (event->direction == GDK_SCROLL_DOWN)
	{
	  /* downward direction */
	  val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
	  if (val == 100)
	    {
	      return TRUE;
	    }
	  val = val + 1;
	  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), val);
	  vymin = (int) ((val * maxy) / 100);
	  gtk_widget_queue_draw (drawingarea1);
	  return (TRUE);
	}
    }
  /* */
  if (event->direction == GDK_SCROLL_LEFT)
    {
      /* left direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
      if (val == 0)
	{
	  return TRUE;
	}
      val = val - 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
      vxmin = (int) ((val * maxx) / 100);
      gtk_widget_queue_draw (drawingarea1);
      return (TRUE);
    }
  /* */
  if (event->direction == GDK_SCROLL_RIGHT)
    {
      /* rigth direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
      if (val == 100)
	{
	  return TRUE;
	}
      val = val + 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
      vxmin = (int) ((val * maxx) / 100);
      gtk_widget_queue_draw (drawingarea1);
      return (TRUE);
    }
  /* GDK_SCROLL_SMOOTH not handled */
  return TRUE;
}

/*
 * key is pressed
 * https://developer.gnome.org/gtk3/stable/checklist-modifiers.html
 * Test for modifier keys correctly
 *
 * Why.  With gtk_accelerator_get_default_mod_mask() you can test for modifier keys reliably; this way your key event handlers will work correctly even if NumLock or CapsLock are activated.
 *
 * In a GdkEventKey, the state field is a bit mask which indicates the modifier state at the time the key was pressed. Modifiers are keys like Control and NumLock. When implementing a “key-press-event” handler, you should use gtk_accelerator_get_default_mod_mask() to test against modifier keys. This function returns a bit mask which encompasses all the modifiers which the user may be actively pressing, such as Control, Shift, and Alt, but ignores "innocuous" modifiers such as NumLock and CapsLock.
 *
 * Say you want to see if Control+F10 was pressed. Doing a simple test like event->keysym == GDK_F10 && event->state == GDK_CONTROL_MASK is not enough. If CapsLock is pressed, then event->state will be equal to GDK_CONTROL_MASK | GDK_LOCK_MASK, and the simple test will fail. By taking the logical-and of event->state and gtk_accelerator_get_default_mod_mask(), you can ignore the modifiers which are not actively pressed by the user at the same time as the base key.
 *
 * The following example correctly tests for Control+F10 being pressed.
 *
 * static gboolean
 * my_widget_key_press_event_handler (GtkWidget *widget, GdkEventKey *event)
 * {
 *  GdkModifierType modifiers;
 *  modifiers = gtk_accelerator_get_default_mod_mask ();
 *  if (event->keysym == GDK_F10
 *      && (event->state & modifiers) == GDK_CONTROL_MASK)
 *    {
 *      g_print ("Control-F10 was pressed\n");
 *      return TRUE;
 *    }
 *  return FALSE;
 * }
 */
static gboolean
on_key_press (GtkWidget * widget, GdkEventKey * event, gpointer user_data)
{
  gdouble val = 0.0;
  if (widget)
    {
    }
  if (event)
    {
    }
  if (user_data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return (FALSE);
    }
  if (option_gdebug)
    {
      printf ("%s(): key \"%s\" pressed\n", __FUNCTION__, gdk_keyval_name (event->keyval));
    }

  /* check for left shift key used with mouse wheel */
#if GTK_HAVE_API_VERSION_2 == 1
  if (event->keyval == GDK_Shift_L)
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    if (event->keyval == GDK_KEY_Shift_L)
#endif
      {
	l_s_k_d = 1;
      }

  switch (event->keyval)
    {
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_Up:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_Up:
#endif
      /* upward direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
      if (val < 1)
	{
	  break;
	}
      val = val - 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), val);
      vymin = (int) ((val * maxy) / 100);
      gtk_widget_queue_draw (drawingarea1);
      break;
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_Down:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_Down:
#endif
      /* downward direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjvscale2));
      if (val > 99)
	{
	  break;
	}
      val = val + 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), val);
      vymin = (int) ((val * maxy) / 100);
      gtk_widget_queue_draw (drawingarea1);
      break;
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_Left:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_Left:
#endif
      /* left direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
      if (val < 1)
	{
	  break;
	}
      val = val - 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
      vxmin = (int) ((val * maxx) / 100);
      gtk_widget_queue_draw (drawingarea1);
      break;
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_Right:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_Right:
#endif
      /* rigth direction */
      val = gtk_adjustment_get_value (GTK_ADJUSTMENT (adjhscale1));
      if (val > 99)
	{
	  break;
	}
      val = val + 1;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), val);
      vxmin = (int) ((val * maxx) / 100);
      gtk_widget_queue_draw (drawingarea1);
      break;
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_p:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_p:
#endif
      /* printf ("key pressed: %s\n", "p"); */
      break;
#if GTK_HAVE_API_VERSION_2 == 1
    case GDK_S:
    case GDK_s:
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    case GDK_KEY_S:
    case GDK_KEY_s:
#endif
      if (event->state & GDK_SHIFT_MASK)
	{
	  /* printf ("key pressed: %s\n", "shift + s"); */
	}
      else if (event->state & GDK_CONTROL_MASK)
	{
	  /* printf ("key pressed: %s\n", "ctrl + s"); */
	}
      else
	{
	  /* printf ("pressed s\n"); */
	}
      break;
    default:
      return FALSE;
      break;
    }

  return FALSE;
}

/* key released */
static gboolean
on_key_released (GtkWidget * widget, GdkEventKey * event, gpointer user_data)
{
  if (widget)
    {
    }
  if (event)
    {
    }
  if (user_data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return (FALSE);
    }
  if (option_gdebug)
    {
      printf ("%s(): key \"%s\" released\n", __FUNCTION__, gdk_keyval_name (event->keyval));
    }
  /* check for left shift key used with mouse wheel */
#if GTK_HAVE_API_VERSION_2 == 1
  if (event->keyval == GDK_Shift_L)
#endif
#if GTK_HAVE_API_VERSION_3 == 1
    if (event->keyval == GDK_KEY_Shift_L)
#endif
      {
	l_s_k_d = 0;
      }
  return (FALSE);
}

#if GTK_HAVE_API_VERSION_2 == 1
/* redraw drawing area */
static gboolean
on_top_level_window_drawingarea1_expose_event (GtkWidget * widget, GdkEventExpose * event, gpointer user_data)
{
  cairo_t *crdraw = NULL;
  gint w = 0;			/* xsize of drawing area */
  gint h = 0;			/* ysize of drawing area */
  int i = 0;
  int ld = 0;
  int hd = 0;
  int fit = 0;

  if (widget)
    {
    }
  if (event)
    {
    }
  if (user_data)
    {
    }

  /* only at first expose */
  if (drawing_area_xsize == 0 && drawing_area_ysize == 0)
    {
      fit = 1;
    }
  else
    {
      fit = 0;
    }

  /* get cairo drawing context */
  crdraw = gdk_cairo_create (event->window);

  /* how large drawing area is */
  (void) gdk_drawable_get_size (event->window, &w, &h);

  if (option_gdebug > 1 || 0)
    {
      printf ("%s(): drawing area size is (%d,%d)\n", __FUNCTION__, w, h);
      fflush (stdout);
    }

  /* save a copy of current size */
  drawing_area_xsize = w;
  drawing_area_ysize = h;

  if (fit)
    {
      /* fit drawing in window */
      dofit ();
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      /* white fill drawing area */
      cairo_set_source_rgb (crdraw, 1.0, 1.0, 1.0);
      cairo_rectangle (crdraw, 0, 0, w, h);
      cairo_fill (crdraw);
      cairo_stroke (crdraw);
      cairo_destroy (crdraw);
      /* no data */
      return (FALSE);
    }

  /* fill drawing background with background color */
  if (altcolor)
    {
      /* set background to white in alt color mode */
      cairo_set_source_rgb (crdraw, 0x00ffffff / 255.0, 0x00ffffff / 255.0, 0x00ffffff / 255.0);
    }
  else
    {
      cairo_set_source_rgb (crdraw, bgcr / 255.0, bgcg / 255.0, bgcb / 255.0);
    }

  /* select whole screen to fill with background color */
  cairo_rectangle (crdraw, 0, 0, w, h);
  cairo_fill (crdraw);
  cairo_stroke (crdraw);
  /* use zoom slider drawing scale */
  cairo_scale (crdraw, zfactor, zfactor);
  /* mark which layers to draw */
  for (i = 0; i < dli_nlevels; i++)
    {
      ld = dlip[i]->y0 - vymin;
      hd = dlip[i]->y0 + dlip[i]->ht - vymin;
      if (((int) (ld * zfactor) >= 0 && (int) (hd * zfactor) < drawing_area_ysize)
	  || ((int) (ld * zfactor) <= 0 && (int) (hd * zfactor) >= 0) || ((int) (ld * zfactor) < drawing_area_ysize
									  && (int) (hd * zfactor) >= drawing_area_ysize))
	{
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): draw layer[%d]\n", __FUNCTION__, i);
	      fflush (stdout);
	    }
	  dlip[i]->draw = 1;
	}
      else
	{
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): skip layer[%d]\n", __FUNCTION__, i);
	      fflush (stdout);
	    }
	  dlip[i]->draw = 0;
	}
    }

  /* draw subgraph-area/node/edge data in the draw lists */
  on_top_level_window_drawingarea1_expose_event_subgrapharea (crdraw);
  on_top_level_window_drawingarea1_expose_event_nodes (crdraw);
  on_top_level_window_drawingarea1_expose_event_edges (crdraw);
  /* ready drawing */
  cairo_destroy (crdraw);
  return (FALSE);
}
#endif

#if GTK_HAVE_API_VERSION_3
/* redraw drawing area */
static gboolean
on_top_level_window_drawingarea1_draw_event (GtkWidget * widget, cairo_t * crdraw, gpointer user_data)
{
  gint w = 0;			/* xsize of drawing area */
  gint h = 0;			/* ysize of drawing area */
  int i = 0;
  int ld = 0;
  int hd = 0;
  cairo_t *crp = NULL;

  /* this is a workaround for issue in cairo-lib 1.14.0 with gtk3,
   * cairo.c cairo_destroy() line 305 assert(), (with gtk2 no problem) */
  crp = cairo_reference (crdraw);

  if (widget)
    {
    }
  if (user_data)
    {
    }

  /* how large drawing area is */
  w = gtk_widget_get_allocated_width (drawingarea1);
  h = gtk_widget_get_allocated_height (drawingarea1);

  if (option_gdebug > 1)
    {
      printf ("%s(): drawing area size is (%d,%d)\n", __FUNCTION__, w, h);
      fflush (stdout);
    }

  /* save a copy of current size */
  drawing_area_xsize = w;
  drawing_area_ysize = h;
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      /* white fill drawing area */
      cairo_set_source_rgb (crdraw, 1.0, 1.0, 1.0);
      cairo_rectangle (crdraw, 0, 0, w, h);
      cairo_fill (crdraw);
      cairo_stroke (crdraw);
      cairo_destroy (crdraw);
      /* no data */
      return (FALSE);
    }

  /* fill drawing background with background color */
  if (altcolor)
    {
      /* set background to white in alt color mode */
      cairo_set_source_rgb (crdraw, 0x00ffffff / 255.0, 0x00ffffff / 255.0, 0x00ffffff / 255.0);
    }
  else
    {
      cairo_set_source_rgb (crdraw, bgcr / 255.0, bgcg / 255.0, bgcb / 255.0);
    }

  /* select whole screen to fill with background color */
  cairo_rectangle (crdraw, 0, 0, w, h);
  cairo_fill (crdraw);
  cairo_stroke (crdraw);
  /* use zoom slider drawing scale */
  cairo_scale (crdraw, zfactor, zfactor);
  /* mark which layers to draw */
  for (i = 0; i < dli_nlevels; i++)
    {
      ld = dlip[i]->y0 - vymin;
      hd = dlip[i]->y0 + dlip[i]->ht - vymin;
      if (((int) (ld * zfactor) >= 0 && (int) (hd * zfactor) < drawing_area_ysize)
	  || ((int) (ld * zfactor) <= 0 && (int) (hd * zfactor) >= 0) || ((int) (ld * zfactor) < drawing_area_ysize
									  && (int) (hd * zfactor) >= drawing_area_ysize))
	{
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): draw layer[%d]\n", __FUNCTION__, i);
	      fflush (stdout);
	    }
	  dlip[i]->draw = 1;
	}
      else
	{
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): skip layer[%d]\n", __FUNCTION__, i);
	      fflush (stdout);
	    }
	  dlip[i]->draw = 0;
	}
    }

  /* draw subgraph-area/node/edge data in the draw lists */
  on_top_level_window_drawingarea1_expose_event_subgrapharea (crdraw);
  on_top_level_window_drawingarea1_expose_event_nodes (crdraw);
  on_top_level_window_drawingarea1_expose_event_edges (crdraw);

  cairo_destroy (crp);

  return (FALSE);
}
#endif

/* draw subgraph area */
void
on_top_level_window_drawingarea1_expose_event_subgrapharea (cairo_t * crp)
{
  int i = 0;
  struct dli *lp = NULL;
  struct ddn *dnptr = NULL;
  int aminx = 0;
  int aminy = 0;
  int amaxx = 0;
  int amaxy = 0;
  int aw = 0;
  int ah = 0;
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  if (option_gdebug || 0)
    {
      printf ("%s(): %d subgraphs and option to draw is %d\n", __FUNCTION__, n_input_subgraphs, option_subgraph_area);
      fflush (stdout);
    }
  /* if no subgraphs in input then this has no effect */
  if (n_input_subgraphs == 0)
    {
      return;
    }
  /* check the subgraph-area option */
  if (option_subgraph_area == 0)
    {
      return;
    }

  /* scan layers and reset subgraph bit */
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* check the nodes */
      dnptr = lp->nl;
      while (dnptr)
	{
	  /* check nodes that are in drawing */
	  if (dlip[dnptr->dn->un->level]->draw)
	    {
	      /* check nodes in a subgraph */
	      if (dnptr->dn->un->rootedon)
		{
		  dnptr->dn->un->rootedon->bitflags.adone = 0;
		}
	    }
	  dnptr = dnptr->next;
	}
    }

  /* scan layers */
  for (i = 0; i < dli_nlevels; i++)
    {
      lp = dlip[i];
      /* check the nodes */
      dnptr = lp->nl;
      while (dnptr)
	{
	  /* check nodes that are in drawing */
	  if (dlip[dnptr->dn->un->level]->draw)
	    {
	      /* check nodes in a subgraph */
	      if (dnptr->dn->un->rootedon)
		{
		  /* check not yet drawn subgraph area */
		  if (dnptr->dn->un->rootedon->bitflags.adone == 0)
		    {
		      /* get the area of the subgraph */
		      aminx = (dnptr->dn->un->rootedon->aminx);
		      aminy = (dnptr->dn->un->rootedon->aminy);
		      amaxx = (dnptr->dn->un->rootedon->amaxx);
		      amaxy = (dnptr->dn->un->rootedon->amaxy);
		      /* the size of the area */
		      aw = amaxx - aminx;
		      ah = amaxy - aminy;

		      /* set drawing color */
		      if (altcolor)
			{
			  /* in alt color mode use the grey color */
			  cairo_set_source_rgb (crp, altr / 255.0, altg / 255.0, altb / 255.0);
			}
		      else
			{
			  /* in normal color mode use dark/light green */
			  if (dnptr->dn->un->rootedon->bitflags.folded == 1)
			    {
			      /* folded state is darker color */
			      cairo_set_source_rgb (crp, subgar_folded / 255.0, subgag_folded / 255.0, subgab_folded / 255.0);
			    }
			  else
			    {
			      /* unfolded state is lighter color */
			      cairo_set_source_rgb (crp, subgar_unfolded / 255.0, subgag_unfolded / 255.0, subgab_unfolded / 255.0);
			    }
			}

		      /* draw rectangle with few pixels border */
		      cairo_rectangle (crp, aminx - vxmin - 3, aminy - vymin - 3, aw + 3 + 3, ah + 3 + 3);
		      cairo_stroke (crp);

		      /* subgraph area is in drawing */
		      dnptr->dn->un->rootedon->bitflags.adone = 1;
		    }
		}
	    }
	  dnptr = dnptr->next;
	}

    }
  /* zz11 should draw the label text of the subgraph in upper left corner 
   * but there can be a node at that location. todo, see dot output.
   */
  return;
}

/* draw all nodes */
void
on_top_level_window_drawingarea1_expose_event_nodes (cairo_t * crp)
{
  int i = 0;

  for (i = 0; i < dli_nlevels; i++)
    {
      if (dlip[i]->draw)
	{
	  on_top_level_window_drawingarea1_expose_event_nodes_layer (crp, dlip[i]);
	}
    }

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_RCLOUD_XSIZE 99
#define SVG_RCLOUD_YSIZE 98
#define SVG_RCLOUD_TXSIZE 74
#define SVG_RCLOUD_TYSIZE 65
#define SVG_RCLOUD_TOX 12
#define SVG_RCLOUD_TOY 6
static void
draw_nodeshape_rcloud (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 60.114407, 0.006556);
  cairo_rel_curve_to (crp, -0.213806, 0.008659, -0.420288, 0.032109, -0.634998, 0.059386);
  cairo_rel_curve_to (crp, -3.434525, 0.441165, -6.466038, 3.249925, -8.006042, 7.435385);
  cairo_rel_line_to (crp, -0.816212, 3.167355);
  cairo_rel_curve_to (crp, 0.176144, -1.097842, 0.446274, -2.162435, 0.816212, -3.167355);
  cairo_rel_curve_to (crp, -2.746223, -3.666813, -6.601109, -5.359171, -10.410061, -4.565099);
  cairo_rel_curve_to (crp, -3.808773, 0.79394, -7.116539, 3.978897, -8.924324, 8.595032);
  cairo_rel_curve_to (crp, -5.103456, -4.050243, -11.464666, -3.805185, -16.386162, 0.639329);
  cairo_rel_curve_to (crp, -4.92126, 4.444792, -7.544572, 12.314121, -6.747226, 20.253136);
  cairo_rel_line_to (crp, 0.521333, 3.227032);
  cairo_rel_curve_to (crp, -0.235372, -1.057217, -0.411522, -2.132614, -0.521333, -3.227032);
  cairo_rel_line_to (crp, -0.079452, 0.297844);
  cairo_rel_curve_to (crp, -4.278068, 0.600639, -7.78648, 4.877758, -8.697742, 10.602245);
  cairo_rel_curve_to (crp, -0.91123, 5.7244, 0.98365, 11.598282, 4.694823, 14.558445);
  cairo_rel_line_to (crp, 5.794535, 1.813869);
  cairo_rel_curve_to (crp, -2.023178, 0.214554, -4.043263, -0.417213, -5.794535, -1.813869);
  cairo_rel_curve_to (crp, -2.862571, 4.132015, -3.521214, 10.249004, -1.632942, 15.301205);
  cairo_rel_curve_to (crp, 1.888159, 5.051971, 5.920859, 7.961197, 10.080995, 7.301476);
  cairo_rel_line_to (crp, 2.528938, -0.862541);
  cairo_rel_curve_to (crp, -0.813581, 0.438713, -1.658731, 0.724243, -2.528938, 0.862541);
  cairo_rel_curve_to (crp, 2.361656, 5.602791, 6.277996, 9.690086, 10.875023, 11.316223);
  cairo_rel_curve_to (crp, 4.597223, 1.626366, 9.480057, 0.665276, 13.55109, -2.661736);
  cairo_rel_curve_to (crp, 3.32135, 6.749748, 9.216442, 10.294731, 15.127407, 9.100761);
  cairo_rel_curve_to (crp, 5.911259, -1.194733, 10.787376, -6.921494, 12.519302, -14.691986);
  cairo_rel_line_to (crp, 0.601295, -4.342033);
  cairo_rel_curve_to (crp, -0.090012, 1.475296, -0.285217, 2.925392, -0.601295, 4.342033);
  cairo_rel_curve_to (crp, 4.065918, 3.43576, 9.18045, 3.629791, 13.38134, 0.505569);
  cairo_rel_curve_to (crp, 4.200874, -3.124069, 6.82222, -9.081108, 6.860428, -15.569542);
  cairo_rel_line_to (crp, -1.506065, -9.693867);
  cairo_rel_line_to (crp, -5.932755, -6.559532);
  cairo_rel_curve_to (crp, 4.587593, 3.703781, 7.479973, 9.346199, 7.43882, 16.253399);
  cairo_rel_curve_to (crp, 5.455009, 0.073708, 10.088623, -5.949726, 12.167877, -12.862373);
  cairo_rel_curve_to (crp, 2.079109, -6.913162, 1.292641, -14.794456, -2.063759, -20.714363);
  cairo_rel_curve_to (crp, 1.392235, -4.408096, 1.309433, -9.421864, -0.226791, -13.740435);
  cairo_rel_curve_to (crp, -1.536659, -4.318542, -4.370598, -7.528096, -7.779228, -8.78835);
  cairo_rel_curve_to (crp, -0.763, -5.7239, -3.907257, -10.327765, -8.062592, -11.836588);
  cairo_rel_curve_to (crp, -4.155602, -1.508994, -8.580711, 0.351683, -11.36274, 4.788343);
  cairo_rel_line_to (crp, -1.689865, 3.672541);
  cairo_rel_curve_to (crp, 0.4319, -1.327701, 0.995712, -2.565982, 1.689865, -3.672541);
  cairo_rel_curve_to (crp, -1.961258, -3.486849, -5.037308, -5.45433, -8.244228, -5.323907);
  cairo_rel_line_to (crp, 0, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_CLOUD_XSIZE 98
#define SVG_CLOUD_YSIZE 99
#define SVG_CLOUD_TXSIZE 75
#define SVG_CLOUD_TYSIZE 45
#define SVG_CLOUD_TOX 12
#define SVG_CLOUD_TOY 35
static void
draw_nodeshape_cloud (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 63.348282, 0.001168);
  cairo_rel_curve_to (crp, -9.399273, 0, -17.467506, 8.893135, -21.416458, 21.731334);
  cairo_rel_curve_to (crp, -1.577335, -1.606083, -3.316921, -2.578817, -5.173904, -2.578817);
  cairo_rel_curve_to (crp, -5.614603, 0, -10.286917, 8.240553, -11.731003, 19.426733);
  cairo_rel_curve_to (crp, -2.464453, -1.777088, -5.330633, -2.853733, -8.391932, -2.853733);
  cairo_rel_curve_to (crp, -9.184048, 0, -16.634377, 9.168007, -16.634377, 20.469582);
  cairo_rel_curve_to (crp, 0, 10.202042, 6.110494, 18.588936, 14.047218, 20.140221);
  cairo_rel_curve_to (crp, -0.355889, 1.100929, -0.631549, 2.19165, -0.631549, 3.347824);
  cairo_rel_curve_to (crp, 0, 10.820984, 16.249491, 19.645836, 36.276072, 19.645836);
  cairo_rel_curve_to (crp, 20.026363, 0, 36.275944, -8.824852, 36.275944, -19.645836);
  cairo_rel_curve_to (crp, 0, -0.727852, -0.24762, -1.37841, -0.391121, -2.085678);
  cairo_rel_curve_to (crp, 0.877411, 0.397583, 1.745781, 0.823402, 2.707474, 0.823402);
  cairo_rel_curve_to (crp, 5.357185, 0, 9.715912, -6.641228, 9.715912, -14.817135);
  cairo_rel_curve_to (crp, 0, -8.176449, -4.358727, -14.817108, -9.715912, -14.817108);
  cairo_rel_curve_to (crp, -0.656502, 0, -1.21508, 0.413635, -1.834984, 0.603809);
  cairo_rel_curve_to (crp, 0.542259, -3.268784, 0.902336, -6.604771, 0.902336, -10.152184);
  cairo_rel_curve_to (crp, 0, -21.641949, -10.737564, -39.23825, -24.003716, -39.23825);
  cairo_rel_line_to (crp, 0, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_CARD_XSIZE 104
#define SVG_CARD_YSIZE 101
#define SVG_CARD_TXSIZE 96
#define SVG_CARD_TYSIZE 90
#define SVG_CARD_TOX 5
#define SVG_CARD_TOY 6
static void
draw_nodeshape_card (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.001461, 20.164606);
  cairo_rel_line_to (crp, 20.765173, -20.166117);
  cairo_rel_line_to (crp, 83.234846, 0);
  cairo_rel_line_to (crp, 0, 100.999985);
  cairo_rel_line_to (crp, -104.000019, 0);
  cairo_rel_line_to (crp, 0, -80.833868);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_DIAMOND_XSIZE 99
#define SVG_DIAMOND_YSIZE 98
#define SVG_DIAMOND_TXSIZE 50
#define SVG_DIAMOND_TYSIZE 55
#define SVG_DIAMOND_TOX 24
#define SVG_DIAMOND_TOY 18
static void
draw_nodeshape_diamond (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.000018, 49.006966);
  cairo_rel_line_to (crp, 49.49491, -49.00731);
  cairo_rel_line_to (crp, 49.495213, 49.00731);
  cairo_rel_line_to (crp, -49.495213, 49.007332);
  cairo_rel_line_to (crp, -49.49491, -49.007332);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_MDIAMOND_XSIZE 99
#define SVG_MDIAMOND_YSIZE 98
#define SVG_MDIAMOND_TXSIZE 45
#define SVG_MDIAMOND_TYSIZE 50
#define SVG_MDIAMOND_TOX 29
#define SVG_MDIAMOND_TOY 18
static void
draw_nodeshape_mdiamond (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;
  gdouble lw = 0.0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.000018, 49.006966);
  cairo_rel_line_to (crp, 49.49491, -49.00731);
  cairo_rel_line_to (crp, 49.495213, 49.00731);
  cairo_rel_line_to (crp, -49.495213, 49.007332);
  cairo_rel_line_to (crp, -49.49491, -49.007332);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  /* inset with subtle rect with black outline color */

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  lw = cairo_get_line_width (crp);
  cairo_set_line_width (crp, 0.4);
  cairo_rectangle (crp, x0 + ((SVG_MDIAMOND_TOX + 0) * xs), y0 + ((SVG_MDIAMOND_TOY + 4) * ys), (SVG_MDIAMOND_TXSIZE - 1) * xs, (SVG_MDIAMOND_TYSIZE + 2) * ys);
  cairo_stroke (crp);
  cairo_set_line_width (crp, lw);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_HEXAGON_XSIZE 98
#define SVG_HEXAGON_YSIZE 100
#define SVG_HEXAGON_TXSIZE 69
#define SVG_HEXAGON_TYSIZE 79
#define SVG_HEXAGON_TOX 14
#define SVG_HEXAGON_TOY 6
static void
draw_nodeshape_hexagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.001638, 49.928436);
  cairo_rel_line_to (crp, 20.998971, -49.928161);
  cairo_rel_line_to (crp, 55.996859, 0);
  cairo_rel_line_to (crp, 20.998947, 49.928161);
  cairo_rel_line_to (crp, -20.998947, 49.92823);
  cairo_rel_line_to (crp, -55.996859, 0);
  cairo_rel_line_to (crp, -20.998971, -49.92823);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_PENTAGON_XSIZE 100
#define SVG_PENTAGON_YSIZE 100
#define SVG_PENTAGON_TXSIZE 65
#define SVG_PENTAGON_TYSIZE 80
#define SVG_PENTAGON_TOX 18
#define SVG_PENTAGON_TOY 16
static void
draw_nodeshape_pentagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.000271, 38.075241);
  cairo_rel_line_to (crp, 49.755894, -38.074757);
  cairo_rel_line_to (crp, 49.755653, 38.074757);
  cairo_rel_line_to (crp, -19.0047, 61.606644);
  cairo_rel_line_to (crp, -61.501194, 0);
  cairo_rel_line_to (crp, -19.005654, -61.606644);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_FOLDER_XSIZE 100
#define SVG_FOLDER_YSIZE 100
#define SVG_FOLDER_TXSIZE 96
#define SVG_FOLDER_TYSIZE 85
#define SVG_FOLDER_TOX 2
#define SVG_FOLDER_TOY 6
static void
draw_nodeshape_folder (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 1.561662, 9.424889);
  cairo_rel_line_to (crp, 21.865706, 0);
  cairo_rel_line_to (crp, 9.371037, -9.423186);
  cairo_rel_line_to (crp, 59.349812, 0);
  cairo_rel_line_to (crp, 9.371033, 9.423186);
  cairo_rel_line_to (crp, 0, 91.090736);
  cairo_rel_line_to (crp, -99.957587, 0);
  cairo_rel_line_to (crp, 0, -91);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  /* fill color */
  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_SBOX_XSIZE 99
#define SVG_SBOX_YSIZE 99
#define SVG_SBOX_TXSIZE 86
#define SVG_SBOX_TYSIZE 89
#define SVG_SBOX_TOX 6
#define SVG_SBOX_TOY 1
static void
draw_nodeshape_sbox (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 15.925787, -0.00012);
  cairo_rel_line_to (crp, 67.148417, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 8.796623, 0, 15.927696, 22.184023, 15.927696, 49.550524);
  cairo_rel_curve_to (crp, 0, 27.365482, -7.131073, 49.550312, -15.927696, 49.550312);
  cairo_rel_line_to (crp, -67.148417, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, -8.796594, 0, -15.927676, -22.18483, -15.927676, -49.550312);
  cairo_rel_curve_to (crp, 0, -27.366501, 7.131082, -49.550524, 15.927676, -49.550524);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_CHEVRON_XSIZE 98
#define SVG_CHEVRON_YSIZE 100
#define SVG_CHEVRON_TXSIZE 52
#define SVG_CHEVRON_TYSIZE 90
#define SVG_CHEVRON_TOX 24
#define SVG_CHEVRON_TOY 0
static void
draw_nodeshape_chevron (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.000138, 0.000443);
  cairo_rel_line_to (crp, 73.500252, 0);
  cairo_rel_line_to (crp, 24.499855, 49.998314);
  cairo_rel_line_to (crp, -24.499855, 49.998306);
  cairo_rel_line_to (crp, -73.500252, 0);
  cairo_rel_line_to (crp, 24.499852, -49.998306);
  cairo_rel_line_to (crp, -24.499852, -49.998314);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_CYLINDER_XSIZE 100
#define SVG_CYLINDER_YSIZE 99
#define SVG_CYLINDER_TXSIZE 95
#define SVG_CYLINDER_TYSIZE 52
#define SVG_CYLINDER_TOX 2
#define SVG_CYLINDER_TOY 33
static void
draw_nodeshape_cylinder (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  gdouble lw = 0.0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in alt if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);

  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 99.499092, 16.830883);
  cairo_rel_curve_to (crp, 0, 9.018671, -22.16198, 16.330723, -49.499546, 16.330723);
  cairo_rel_move_to (crp, 49.499546, -16.330723);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, 9.018671, -22.16198, 16.330723, -49.499546, 16.330723);
  cairo_rel_curve_to (crp, -27.338341, 0, -49.500297, -7.312052, -49.500297, -16.330723);
  cairo_rel_move_to (crp, 0, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, -9.019766, 22.161956, -16.331818, 49.500297, -16.331818);
  cairo_rel_curve_to (crp, 27.337566, 0, 49.499546, 7.312052, 49.499546, 16.331818);
  cairo_rel_line_to (crp, 0, 65.325695);
  cairo_rel_curve_to (crp, 0, 9.020439, -22.16198, 16.332382, -49.499546, 16.332382);
  cairo_rel_curve_to (crp, -27.338341, 0, -49.500297, -7.311943, -49.500297, -16.332382);
  cairo_rel_line_to (crp, 0, -65.325695);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  /* additional lines */

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

  lw = cairo_get_line_width (crp);
  cairo_set_line_width (crp, 0.2);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);

  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 99.499092, 16.830883);
  cairo_rel_curve_to (crp, 0, 9.018671, -22.16198, 16.330723, -49.499546, 16.330723);
  cairo_rel_move_to (crp, 49.499546, -16.330723);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, 9.018671, -22.16198, 16.330723, -49.499546, 16.330723);
  cairo_rel_curve_to (crp, -27.338341, 0, -49.500297, -7.312052, -49.500297, -16.330723);
  cairo_rel_move_to (crp, 0, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, -9.019766, 22.161956, -16.331818, 49.500297, -16.331818);
  cairo_rel_curve_to (crp, 27.337566, 0, 49.499546, 7.312052, 49.499546, 16.331818);
  cairo_rel_line_to (crp, 0, 65.325695);
  cairo_rel_curve_to (crp, 0, 9.020439, -22.16198, 16.332382, -49.499546, 16.332382);
  cairo_rel_curve_to (crp, -27.338341, 0, -49.500297, -7.311943, -49.500297, -16.332382);
  cairo_rel_line_to (crp, 0, -65.325695);
  /* */
  cairo_close_path (crp);
  cairo_stroke (crp);
  cairo_set_line_width (crp, lw);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_CUBE_XSIZE 100
#define SVG_CUBE_YSIZE 99
#define SVG_CUBE_TXSIZE 85
#define SVG_CUBE_TYSIZE 80
#define SVG_CUBE_TOX 0
#define SVG_CUBE_TOY 6
static void
draw_nodeshape_cube (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  gdouble lw = 0.0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 10.23709, 0.68179);
  cairo_rel_line_to (crp, -9.735158, 9.637667);
  cairo_rel_line_to (crp, 0, 87.842156);
  cairo_rel_line_to (crp, 87.430715, 0.223083);
  cairo_rel_line_to (crp, 11.034882, -9.860947);
  cairo_rel_line_to (crp, 0, -87.84196);
  cairo_rel_line_to (crp, -88.730439, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  /* additional lines */

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

  lw = cairo_get_line_width (crp);
  cairo_set_line_width (crp, 0.2);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* */
  cairo_rel_move_to (crp, 88.226052, 9.480106);
  cairo_rel_line_to (crp, 0, 88.23587);
  /* */
  cairo_rel_move_to (crp, 0, -88.23587);
  cairo_rel_line_to (crp, 11.034943, -9.663845);
  /* */
  cairo_rel_move_to (crp, -11.034943, 10.084081);
  cairo_rel_line_to (crp, -87.855095, -0.420237);
  /* */
  cairo_close_path (crp);
  cairo_stroke (crp);
  cairo_set_line_width (crp, lw);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_DIALOG_XSIZE 99
#define SVG_DIALOG_YSIZE 99
#define SVG_DIALOG_TXSIZE 95
#define SVG_DIALOG_TYSIZE 60
#define SVG_DIALOG_TOX 0
#define SVG_DIALOG_TOY 3
static void
draw_nodeshape_dialog (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.00109, 11.592129);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, -6.403424, 5.078078, -11.593685, 11.342891, -11.593685);
  cairo_rel_line_to (crp, 5.154982, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_line_to (crp, 24.74761, 0);
  cairo_rel_line_to (crp, 46.401276, 0);
  cairo_rel_curve_to (crp, 3.008354, 0, 5.893707, 1.220773, 8.020607, 3.394865);
  cairo_rel_curve_to (crp, 2.127235, 2.174743, 3.321899, 5.123672, 3.321899, 8.19882);
  cairo_rel_line_to (crp, 0, 28.983955);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_line_to (crp, 0, 17.390137);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, 6.402706, -5.077744, 11.593616, -11.342506, 11.593616);
  cairo_rel_line_to (crp, -46.401276, 0);
  cairo_rel_line_to (crp, -32.328972, 29.479828);
  cairo_rel_line_to (crp, 7.581362, -29.479828);
  cairo_rel_line_to (crp, -5.154982, 0);
  cairo_rel_curve_to (crp, -6.264812, 0, -11.342891, -5.19091, -11.342891, -11.593616);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_line_to (crp, 0, -17.390137);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_line_to (crp, 0, -28.983955);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_MAXIMIZE_XSIZE 99
#define SVG_MAXIMIZE_YSIZE 99
#define SVG_MAXIMIZE_TXSIZE 47
#define SVG_MAXIMIZE_TYSIZE 47
#define SVG_MAXIMIZE_TOX 26
#define SVG_MAXIMIZE_TOY 23
static void
draw_nodeshape_maximize (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.001357, 49.501163);
  cairo_rel_line_to (crp, 18.329869, -18.329813);
  cairo_rel_line_to (crp, 0, 9.164827);
  cairo_rel_line_to (crp, 7.348885, 0);
  cairo_rel_line_to (crp, 0, -14.655876);
  cairo_rel_line_to (crp, 14.656042, 0);
  cairo_rel_line_to (crp, 0, -7.349089);
  cairo_rel_line_to (crp, -9.164415, 0);
  cairo_rel_line_to (crp, 18.329206, -18.329856);
  cairo_rel_line_to (crp, 18.330406, 18.329856);
  cairo_rel_line_to (crp, -9.165314, 0);
  cairo_rel_line_to (crp, 0, 7.349089);
  cairo_rel_line_to (crp, 14.656075, 0);
  cairo_rel_line_to (crp, 0, 14.655876);
  cairo_rel_line_to (crp, 7.349083, 0);
  cairo_rel_line_to (crp, 0, -9.164827);
  cairo_rel_line_to (crp, 18.330284, 18.329813);
  cairo_rel_line_to (crp, -18.330284, 18.329853);
  cairo_rel_line_to (crp, 0, -9.16481);
  cairo_rel_line_to (crp, -7.349083, 0);
  cairo_rel_line_to (crp, 0, 14.65593);
  cairo_rel_line_to (crp, -14.656075, 0);
  cairo_rel_line_to (crp, 0, 7.34948);
  cairo_rel_line_to (crp, 9.165314, 0);
  cairo_rel_line_to (crp, -18.330406, 18.329605);
  cairo_rel_line_to (crp, -18.329206, -18.329605);
  cairo_rel_line_to (crp, 9.164415, 0);
  cairo_rel_line_to (crp, 0, -7.34948);
  cairo_rel_line_to (crp, -14.656042, 0);
  cairo_rel_line_to (crp, 0, -14.65593);
  cairo_rel_line_to (crp, -7.348885, 0);
  cairo_rel_line_to (crp, 0, 9.16481);
  cairo_rel_line_to (crp, -18.329869, -18.329853);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_stroke (crp);
  /* fill color into path */
  cairo_append_path (crp, cpt);

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_PAGE_XSIZE 100
#define SVG_PAGE_YSIZE 100
#define SVG_PAGE_TXSIZE 95
#define SVG_PAGE_TYSIZE 80
#define SVG_PAGE_TOX 0
#define SVG_PAGE_TOY 0
static void
draw_nodeshape_page (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 83.004883, 99.502281);
  cairo_rel_line_to (crp, 3.299919, -13.200714);
  cairo_rel_line_to (crp, 13.200981, -3.30024);
  cairo_rel_line_to (crp, -16.5009, 16.500954);
  cairo_rel_line_to (crp, -82.503042, 0);
  cairo_rel_line_to (crp, 0, -99.003847);
  cairo_rel_line_to (crp, 99.003943, 0);
  cairo_rel_line_to (crp, 0, 82.502894);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_PLAQUE_XSIZE 100
#define SVG_PLAQUE_YSIZE 101
#define SVG_PLAQUE_TXSIZE 65
#define SVG_PLAQUE_TYSIZE 65
#define SVG_PLAQUE_TOX 16
#define SVG_PLAQUE_TOY 16
static void
draw_nodeshape_plaque (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.00115, 16.831707);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 9.205069, 0, 16.667118, -7.536782, 16.667118, -16.833599);
  cairo_rel_line_to (crp, 66.66725, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, 9.296817, 7.462189, 16.833599, 16.667236, 16.833599);
  cairo_rel_line_to (crp, 0, 67.332577);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, -9.205048, 0, -16.667236, 7.53685, -16.667236, 16.833748);
  cairo_rel_line_to (crp, -66.66725, 0);
  cairo_rel_curve_to (crp, 0, -9.296898, -7.462049, -16.833748, -16.667118, -16.833748);
  cairo_rel_line_to (crp, 0, -67.332577);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_PREP_XSIZE 97
#define SVG_PREP_YSIZE 98
#define SVG_PREP_TXSIZE 56
#define SVG_PREP_TYSIZE 96
#define SVG_PREP_TOX 20
#define SVG_PREP_TOY 0
static void
draw_nodeshape_prep (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.001692, 49.148949);
  cairo_rel_line_to (crp, 19.399959, -49.150682);
  cairo_rel_line_to (crp, 58.200041, 0);
  cairo_rel_line_to (crp, 19.400024, 49.150682);
  cairo_rel_line_to (crp, -19.400024, 49.151901);
  cairo_rel_line_to (crp, -58.200041, 0);
  cairo_rel_line_to (crp, -19.399959, -49.151901);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_PUNCHED_XSIZE 100
#define SVG_PUNCHED_YSIZE 99
#define SVG_PUNCHED_TXSIZE 96
#define SVG_PUNCHED_TYSIZE 58
#define SVG_PUNCHED_TOX 1
#define SVG_PUNCHED_TOY 16
static void
draw_nodeshape_punched (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.00148, 9.901799);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, 5.467584, 11.192809, 9.899932, 25.000056, 9.899932);
  cairo_rel_curve_to (crp, 13.807085, 0, 24.999887, -4.432348, 24.999887, -9.899932);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, -5.467794, 11.192982, -9.900014, 25.00008, -9.900014);
  cairo_rel_curve_to (crp, 13.807037, 0, 24.999985, 4.43222, 24.999985, 9.900014);
  cairo_rel_line_to (crp, 0, 79.200038);
  cairo_rel_curve_to (crp, 0, -5.467606, -11.192947, -9.900093, -24.999985, -9.900093);
  cairo_rel_curve_to (crp, -13.807098, 0, -25.00008, 4.432487, -25.00008, 9.900093);
  cairo_rel_curve_to (crp, 0, 5.467575, -11.192802, 9.89994, -24.999887, 9.89994);
  cairo_rel_curve_to (crp, -13.807246, 0, -25.000056, -4.432365, -25.000056, -9.89994);
  cairo_rel_line_to (crp, 0, -79.200038);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_SEPTAGON_XSIZE 99
#define SVG_SEPTAGON_YSIZE 99
#define SVG_SEPTAGON_TXSIZE 76
#define SVG_SEPTAGON_TYSIZE 58
#define SVG_SEPTAGON_TOX 10
#define SVG_SEPTAGON_TOY 16
static void
draw_nodeshape_septagon (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, -0.000907, 63.427032);
  cairo_rel_line_to (crp, 9.805389, -43.891584);
  cairo_rel_line_to (crp, 39.70071, -19.533644);
  cairo_rel_line_to (crp, 39.700115, 19.533644);
  cairo_rel_line_to (crp, 9.805885, 43.891584);
  cairo_rel_line_to (crp, -27.474617, 35.198563);
  cairo_rel_line_to (crp, -44.063589, 0);
  cairo_rel_line_to (crp, -27.473893, -35.198563);
  cairo_rel_line_to (crp, 0, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_SHIELD_XSIZE 98
#define SVG_SHIELD_YSIZE 99
#define SVG_SHIELD_TXSIZE 76
#define SVG_SHIELD_TYSIZE 58
#define SVG_SHIELD_TOX 10
#define SVG_SHIELD_TOY 16
static void
draw_nodeshape_shield (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 79.009117, -0.001529);
  cairo_rel_curve_to (crp, -9.076843, 6.929099, -21.622234, 6.996226, -30.767879, 0.206579);
  cairo_rel_curve_to (crp, -8.79594, 6.532634, -20.742065, 6.719698, -29.71876, 0.560733);
  cairo_rel_line_to (crp, -16.140413, 16.371392);
  cairo_rel_curve_to (crp, 9.928576, 10.25285, 9.871199, 26.725782, -0.169675, 36.908358);
  cairo_rel_curve_to (crp, -1.332554, 3.476341, -2.030967, 3.308296, -2.176368, 6.809643);
  cairo_rel_curve_to (crp, -0.341962, 8.42931, 1.660188, 16.292034, 7.93156, 22.214062);
  cairo_rel_curve_to (crp, 5.602369, 5.290565, 13.204741, 7.534203, 20.507411, 6.733421);
  cairo_rel_curve_to (crp, 12.194452, 0.639015, 18.041018, 5.995636, 20.494036, 9.199387);
  cairo_rel_curve_to (crp, 3.312458, -3.422798, 8.463505, -8.926666, 24.119671, -9.61261);
  cairo_rel_curve_to (crp, 9.34568, -1.368088, 12.72332, -3.578445, 17.10936, -7.66478);
  cairo_rel_curve_to (crp, 5.904022, -5.574982, 7.968132, -12.634323, 7.793991, -20.477718);
  cairo_rel_curve_to (crp, -0.072159, -3.25666, -0.464989, -4.388638, -2.404633, -8.546455);
  cairo_rel_curve_to (crp, -10.087418, -10.229786, -10.094841, -26.809584, -0.026947, -37.051482);
  cairo_rel_line_to (crp, -16.551353, -15.650528);
  cairo_rel_line_to (crp, 0, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_STORED_XSIZE 100
#define SVG_STORED_YSIZE 100
#define SVG_STORED_TXSIZE 70
#define SVG_STORED_TYSIZE 85
#define SVG_STORED_TOX 10
#define SVG_STORED_TOY 0
static void
draw_nodeshape_stored (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 16.668457, -0.001147);
  cairo_rel_line_to (crp, 83.333855, 0);
  cairo_rel_curve_to (crp, -9.204796, 0, -16.666779, 22.361598, -16.666779, 49.94583);
  cairo_rel_curve_to (crp, 0, 27.584614, 7.461983, 49.946049, 16.666779, 49.946049);
  cairo_rel_line_to (crp, -83.333855, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, -9.204898, 0, -16.666863, -22.361435, -16.666863, -49.946049);
  cairo_rel_curve_to (crp, 0, -27.584232, 7.461965, -49.94583, 16.666863, -49.94583);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_TRAPEZOID_XSIZE 98
#define SVG_TRAPEZOID_YSIZE 100
#define SVG_TRAPEZOID_TXSIZE 62
#define SVG_TRAPEZOID_TYSIZE 86
#define SVG_TRAPEZOID_TOX 18
#define SVG_TRAPEZOID_TOY 0
static void
draw_nodeshape_trapezoid (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.001794, 99.995079);
  cairo_rel_line_to (crp, 18.375489, -99.995748);
  cairo_rel_line_to (crp, 61.253256, 0);
  cairo_rel_line_to (crp, 18.375465, 99.995748);
  cairo_rel_line_to (crp, -98.00421, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_TRIANGLE_XSIZE 99
#define SVG_TRIANGLE_YSIZE 101
#define SVG_TRIANGLE_TXSIZE 45
#define SVG_TRIANGLE_TYSIZE 50
#define SVG_TRIANGLE_TOX 28
#define SVG_TRIANGLE_TOY 46
static void
draw_nodeshape_triangle (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }
  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0.000313, 100.500099);
  cairo_rel_line_to (crp, 49.500019, -100.500094);
  cairo_rel_line_to (crp, 49.499897, 100.500094);
  cairo_rel_line_to (crp, -98.999916, 0);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_ITRIANGLE_XSIZE 100
#define SVG_ITRIANGLE_YSIZE 99
#define SVG_ITRIANGLE_TXSIZE 45
#define SVG_ITRIANGLE_TYSIZE 50
#define SVG_ITRIANGLE_TOX 28
#define SVG_ITRIANGLE_TOY 0
static void
draw_nodeshape_itriangle (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 0, 0);
  cairo_rel_line_to (crp, 100, 0);
  cairo_rel_line_to (crp, -50, 100);
  cairo_rel_line_to (crp, -50, -100);
  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);
  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* sizes and points at 1:1 100% scaling */
#define SVG_VSCROLL_XSIZE 100
#define SVG_VSCROLL_YSIZE 100
#define SVG_VSCROLL_TXSIZE 65
#define SVG_VSCROLL_TYSIZE 80
#define SVG_VSCROLL_TOX 20
#define SVG_VSCROLL_TOY 15
static void
draw_nodeshape_vscroll (cairo_t * crp, gdouble x0, gdouble y0, gdouble xs, gdouble ys, int rc, int gc, int bc, struct unode *un)
{
  cairo_path_t *cpt = (cairo_path_t *) 0;
  int r = 0;
  int g = 0;
  int b = 0;

  r = rc;
  g = gc;
  b = bc;

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  /* upperleft start point */
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_move_to (crp, x0, y0);
  /* */
  cairo_scale (crp, xs, ys);
  /* generated by svg to cairo-lib c tool start */
  cairo_rel_move_to (crp, 13.249548, 86.750206);
  cairo_rel_line_to (crp, 0, -79.623695);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 0, -3.382666, 2.742153, -6.124827, 6.124796, -6.124827);
  cairo_rel_line_to (crp, 73.498451, 0);
  cairo_rel_curve_to (crp, 3.382607, 0, 6.124748, 2.74216, 6.124748, 6.124827);
  cairo_rel_curve_to (crp, 0, 3.382668, -2.742142, 6.125021, -6.124748, 6.125021);
  cairo_rel_line_to (crp, -6.124779, 0);
  cairo_rel_line_to (crp, 0, 79.623545);
  cairo_rel_curve_to (crp, 0, 3.38266, -2.742508, 6.124779, -6.124802, 6.124779);
  cairo_rel_line_to (crp, -73.498432, 0);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, -3.382653, 0, -6.124778, -2.742119, -6.124778, -6.124779);
  cairo_rel_curve_to (crp, 0, -3.382736, 2.742125, -6.12487, 6.124778, -6.12487);
  cairo_rel_line_to (crp, 6.124765, 0);
  /* */
  cairo_rel_move_to (crp, 12.249941, -85.748522);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 3.382637, 0, 6.124767, 2.74216, 6.124767, 6.124827);
  cairo_rel_curve_to (crp, 0, 3.382668, -2.74213, 6.125021, -6.124767, 6.125021);
  cairo_rel_curve_to (crp, -1.69132, 0, -3.062399, -1.371089, -3.062399, -3.062605);
  cairo_rel_curve_to (crp, 0, -1.691236, 1.371078, -3.062416, 3.062399, -3.062416);
  cairo_rel_line_to (crp, 6.124767, 0);
  cairo_rel_move_to (crp, 61.248539, 6.125021);
  cairo_rel_line_to (crp, -67.373306, 0);
  cairo_rel_move_to (crp, -12.249941, 73.498674);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 1.691322, 0, 3.062391, 1.371117, 3.062391, 3.062431);
  cairo_rel_curve_to (crp, 0, 1.691261, -1.371069, 3.062439, -3.062391, 3.062439);
  cairo_rel_line_to (crp, 6.124796, 0);
  cairo_rel_move_to (crp, -6.124796, 6.124779);
  cairo_rel_line_to (crp, 0, 0);
  cairo_rel_curve_to (crp, 3.382662, 0, 6.124796, -2.742119, 6.124796, -6.124779);
  cairo_rel_line_to (crp, 0, -6.12487);

  /* generated by svg to cairo-lib c tool end */
  cairo_close_path (crp);
  /* save copy of this path to use for fill below */
  cpt = cairo_copy_path (crp);
  /* */
  cairo_fill (crp);
  cairo_stroke (crp);

  /* outline color into path */
  cairo_append_path (crp, cpt);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_stroke (crp);
  cairo_path_destroy (cpt);
  cairo_restore (crp);

  return;
}

/* */
static void
draw_nodeshape_box (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  /* node fill color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);
  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_rectangle (crp, x0, y0, un->bbx, un->bby);
  cairo_fill (crp);
  cairo_stroke (crp);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  /* outline color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_rectangle (crp, x0, y0, un->bbx, un->bby);
  cairo_stroke (crp);

  return;
}

/* */
static void
draw_nodeshape_dbox (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;

  /* node fill color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_rectangle (crp, x0, y0, un->bbx, un->bby);
  cairo_fill (crp);
  cairo_stroke (crp);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_rectangle (crp, x0, y0, un->bbx, un->bby);
  cairo_rectangle (crp, (x0 + 3), (y0 + 3), (un->bbx - 3 - 3), (un->bby - 3 - 3));
  cairo_stroke (crp);

  return;
}

/* */
static void
draw_nodeshape_rbox (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  double rbaspect = 0.0;	/* aspect ratio */
  double rbcorner_radius = 0.0;	/* and corner curvature radius */
  double rbradius = 0.0;
  double rbdegrees = 0.0;

  /* node fill color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  rbaspect = 1.0;		/* aspect ratio */
  rbcorner_radius = un->bby / 10.0;	/* and corner curvature radius */
  rbradius = rbcorner_radius / rbaspect;
  rbdegrees = M_PI / 180.0;
  cairo_new_sub_path (crp);
  cairo_arc (crp, (x0 + un->bbx - rbradius), (y0 + rbradius), (rbradius), (-90 * rbdegrees), (0 * rbdegrees));
  cairo_arc (crp, (x0 + un->bbx - rbradius), (y0 + un->bby - rbradius), (rbradius), (0 * rbdegrees), (90 * rbdegrees));
  cairo_arc (crp, (x0 + rbradius), (y0 + un->bby - rbradius), (rbradius), (90 * rbdegrees), (180 * rbdegrees));
  cairo_arc (crp, (x0 + rbradius), (y0 + rbradius), (rbradius), (180 * rbdegrees), (270 * rbdegrees));
  cairo_close_path (crp);
  cairo_fill (crp);
  cairo_stroke (crp);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  rbaspect = 1.0;		/* aspect ratio */
  rbcorner_radius = un->bby / 10.0;	/* and corner curvature radius */
  rbradius = rbcorner_radius / rbaspect;
  rbdegrees = M_PI / 180.0;
  cairo_new_sub_path (crp);
  cairo_arc (crp, (x0 + un->bbx - rbradius), (y0 + rbradius), (rbradius), (-90 * rbdegrees), (0 * rbdegrees));
  cairo_arc (crp, (x0 + un->bbx - rbradius), (y0 + un->bby - rbradius), (rbradius), (0 * rbdegrees), (90 * rbdegrees));
  cairo_arc (crp, (x0 + rbradius), (y0 + un->bby - rbradius), (rbradius), (90 * rbdegrees), (180 * rbdegrees));
  cairo_arc (crp, (x0 + rbradius), (y0 + rbradius), (rbradius), (180 * rbdegrees), (270 * rbdegrees));
  cairo_close_path (crp);
  cairo_stroke (crp);

  return;
}

/* */
static void
draw_nodeshape_ellips (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  gint dpwi = 0;
  gint dphi = 0;
  gdouble lw = 0.0;
  /* start at */
  dpwi = un->tx;
  dphi = un->ty;

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  /* start at */
  dpwi = un->tx;
  dphi = un->ty;

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  lw = cairo_get_line_width (crp);
  cairo_set_line_width (crp, 0.1);
  cairo_save (crp);
  cairo_new_path (crp);
  cairo_translate (crp,		/* ellipse->center_x */
		   x0 + (un->bbx / 2),
		   /* ellipse->center_y */
		   y0 + (un->bby / 2));
  cairo_scale (crp, /* ellipse->radius_x */ dpwi,
	       /* ellipse->radius_y */ dphi * 0.8);
  cairo_arc (crp, 0.0, 0.0, 1.0, 0.0, 2.0 * M_PI);
  cairo_close_path (crp);
  cairo_stroke (crp);
  cairo_restore (crp);
  cairo_set_line_width (crp, lw);

  /* node fill color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_save (crp);
  cairo_new_path (crp);
  cairo_translate (crp,		/* ellipse->center_x */
		   x0 + (un->bbx / 2),
		   /* ellipse->center_y */
		   y0 + (un->bby / 2));
  cairo_scale (crp, /* ellipse->radius_x */ dpwi,
	       /* ellipse->radius_y */ dphi * 0.8);
  cairo_arc (crp, 0.0, 0.0, 1.0, 0.0, 2.0 * M_PI);
  cairo_fill (crp);
  cairo_close_path (crp);
  cairo_stroke (crp);
  cairo_restore (crp);

  return;
}

/* */
static void
draw_nodeshape_circle (cairo_t * crp, gdouble x0, gdouble y0, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  gdouble lw = 0.0;
  /* node fill color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);
  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = 0xff;
	  g = 0xff;
	  b = 0xff;
	}
    }

  /* fill color */
  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_translate (crp,		/* ellipse->center_x */
		   x0 + (un->bbx / 2),
		   /* ellipse->center_y */
		   y0 + (un->bby / 2));
  cairo_scale (crp,		/* ellipse->radius_x */
	       un->bbx / 2,
	       /* ellipse->radius_y */
	       un->bby / 2);
  cairo_arc (crp, 0.0, 0.0, 1.0, 0.0, 2.0 * M_PI);
  cairo_fill (crp);
  cairo_stroke (crp);
  cairo_restore (crp);

  /* outline color */
  r = (un->bcolor & 0x00ff0000) >> 16;
  g = (un->bcolor & 0x0000ff00) >> 8;
  b = (un->bcolor & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  /* change in white if drawing as altcolor */
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
  lw = cairo_get_line_width (crp);
  cairo_set_line_width (crp, 0.1);
  cairo_new_path (crp);
  cairo_save (crp);
  cairo_translate (crp,		/* ellipse->center_x */
		   x0 + (un->bbx / 2),
		   /* ellipse->center_y */
		   y0 + (un->bby / 2));
  cairo_scale (crp,		/* ellipse->radius_x */
	       un->bbx / 2,
	       /* ellipse->radius_y */
	       un->bby / 2);
  cairo_arc (crp, 0.0, 0.0, 1.0, 0.0, 2.0 * M_PI);
  cairo_stroke (crp);
  cairo_restore (crp);
  cairo_set_line_width (crp, lw);

  return;
}

/* draw all record nodes in one layer */
static void
on_top_level_window_drawingarea1_expose_event_nodes_layer_record_r (cairo_t * crp, struct unode *un, struct fld *root)
{
  int ix = 0;
  struct fld *tmpf = NULL;
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  gint x0 = 0;
  gint y0 = 0;
  int xo = 0;
  int yo = 0;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;

  /* outer box start at */
  x0 = (un->x0 - vxmin);
  if (x0)
    {
    }
  y0 = (un->y1 - vymin);
  if (y0)
    {
    }

  ix = 0;
  tmpf = NULL;
  /* should not happen */
  if (root == NULL)
    {
      return;
    }
  if (root->f == NULL)
    {
      return;
    }

  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}

      if (tmpf->label)
	{

	  /* use text color */
	  color = un->textcolor;
	  r = (color & 0x00ff0000) >> 16;
	  g = (color & 0x0000ff00) >> 8;
	  b = (color & 0x000000ff);
	  /* change color if in alt color drawing mode */
	  if (altcolor)
	    {
	      if (un->bitflags.altcolor == 1)
		{
		  r = altr;
		  g = altg;
		  b = altb;
		}
	    }
	  /* draw in text color of node */
	  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

	  /* zz9 text box outline rect. */
	  if (tmpf->xsiz0 && tmpf->ysiz0)
	    {
	      cairo_rectangle (crp, un->x0 - vxmin + tmpf->x0, un->y1 - vymin + tmpf->y0, tmpf->xsiz0, tmpf->ysiz0);
	    }

	  /* */
	  xo = (drawing_default_textborder / 4);
	  if (xo)
	    {
	    }
	  yo = (drawing_default_textborder / 4);
	  if (yo)
	    {
	    }

	  /* set start position of text */
	  cairo_move_to (crp, un->x0 - vxmin + 0 /* xo */  + tmpf->x0, un->y1 - vymin + 0 /* yo */  + tmpf->y0);

	  /* */
	  layout = pango_cairo_create_layout (crp);
	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, tmpf->label, -1);
	  /* set font parameters */

	  /* create the fontname description */
	  memset (buf, 0, 128);
	  /* check if node has a specified fontname */
	  if (un->fontname)
	    {
	      default_fontname = un->fontname;
	    }
	  else
	    {
	      default_fontname = DEFAULT_FONTNAME;
	    }
	  /* check if node has a specified font slant */
	  default_fontslant = DEFAULT_FONTSLANT;
	  if (un->bitflags.textoblique)
	    {
	      default_fontslant = "Oblique";
	    }
	  if (un->bitflags.textitalic)
	    {
	      default_fontslant = "Italic";
	    }
	  /* check if node has a specified font weight */
	  if (un->bitflags.textbold)
	    {
	      default_fontweight = "Bold";
	    }
	  else
	    {
	      default_fontweight = DEFAULT_FONTWEIGHT;
	    }
	  /* check if node has a specified font size */
	  if (un->fontsize)
	    {
	      default_fontsize = un->fontsize;
	    }
	  else
	    {
	      default_fontsize = DEFAULT_FONTSIZE;
	    }
	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	  /* copy string buffer */
	  s = uniqstring (buf);
	  /* */
	  desc = pango_font_description_from_string (s);
	  /* check and re-try with defaults */
	  if (desc == NULL)
	    {
	      /* change node to use defaults */
	      un->fontname = NULL;
	      un->bitflags.textoblique = 0;
	      un->bitflags.textitalic = 0;
	      un->bitflags.textbold = 0;
	      un->fontsize = NULL;
	      /* name of font to use, example "Sans" */
	      default_fontname = DEFAULT_FONTNAME;
	      /* name of slant to use, example "Italic", "Oblique", "Roman" */
	      default_fontslant = DEFAULT_FONTSLANT;
	      /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
	      default_fontweight = DEFAULT_FONTWEIGHT;
	      /* name of condensed to use, example "Semi-Condensed", "Condensed" */
	      default_fontcondensed = DEFAULT_FONTCONDENSED;
	      /* font size to use, example "10", "18", "20" etc. */
	      default_fontsize = DEFAULT_FONTSIZE;
	      /* create the fontname description */
	      memset (buf, 0, 128);
	      /* create the font name string */
	      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	      /* copy string buffer */
	      s = uniqstring (buf);
	      /* */
	      desc = pango_font_description_from_string (s);
	    }
	  /* check */
	  if (desc == NULL)
	    {
	      printf ("%s(): desc is NULL\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  /* */
	  pango_layout_set_font_description (layout, desc);
	  /* */
	  pango_font_description_free (desc);
	  /* */
	  pango_cairo_update_layout (crp, layout);
	  /* draw the text */
	  pango_cairo_show_layout (crp, layout);
	  /* */
	  cairo_stroke (crp);
	}

      ix++;
      on_top_level_window_drawingarea1_expose_event_nodes_layer_record_r (crp, un, tmpf);
    }
  return;
}

/* draw all record nodes in one layer */
static void
on_top_level_window_drawingarea1_expose_event_nodes_layer_record (cairo_t * crp, struct unode *un)
{
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  gint x0 = 0;
  gint y0 = 0;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  double dfs = 0.0;
  int fs = 0;

  /* node has a border and background */

  /* fill the background color */
  color = un->fillcolor;
  r = (color & 0x00ff0000) >> 16;
  g = (color & 0x0000ff00) >> 8;
  b = (color & 0x000000ff);

  /* change color if in alt color drawing mode */
  if (altcolor)
    {
      if (un->bitflags.altcolor == 1)
	{
	  r = altr;
	  g = altg;
	  b = altb;
	}
    }

  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

  /* start at */
  x0 = (un->x0 - vxmin);
  y0 = (un->y1 - vymin);

  draw_nodeshape_box (crp, x0, y0, un);

  /* check if node has a specified font size */
  if (un->fontsize)
    {
      default_fontsize = un->fontsize;
    }
  else
    {
      default_fontsize = DEFAULT_FONTSIZE;
    }

  /* calculate the scaled font size */
  dfs = (zfactor * atoi (default_fontsize));
  /* somewhat rounded version */
  fs = (int) dfs;
  /* too small to draw readable text then skip the following text drawing area */
  if (fs < 3)
    {
      /* next node in this layer */
      return;
    }

  /* go draw the text parts */
  on_top_level_window_drawingarea1_expose_event_nodes_layer_record_r (crp, un, un->f);

  return;
}

/* draw all nodes in one layer */
static void
on_top_level_window_drawingarea1_expose_event_nodes_layer (cairo_t * crp, struct dli *lp)
{
  struct ddn *dnptr = NULL;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  gint x0 = 0;
  gint y0 = 0;
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  int xo = 0;
  int yo = 0;
  double dfs = 0.0;
  int fs = 0;
  double dxo = 0.0;
  double dyo = 0.0;
  double svgtsx = 0.0;
  double svgtsy = 0.0;

  /* scan the node list of layer */
  dnptr = lp->nl;

  while (dnptr)
    {

      /* check and there should be a edge label */
      if (dnptr->dn->un->bitflags.edgelabel == 1)
	{
	  if ((dnptr->dn->un->label == NULL) || (dnptr->dn->un->utf8label == NULL))
	    {
	      dnptr->dn->un->utf8label = valid_label_string (dnptr->dn->un->label);
	      /* to update this must be fixed */
	      if (1)
		{
		  printf ("%s(): label is \"%s\" and utf8label is \"%s\" (unusual)\n", __FUNCTION__, dnptr->dn->un->label, dnptr->dn->un->utf8label);
		  fflush (stdout);
		  dnptr = dnptr->next;
		  continue;
		}
	    }
	}

      /* draw record node if has data */
      if (dnptr->dn->un->bitflags.shape == NSHAPE_RECORD && dnptr->dn->un->f != NULL && dnptr->dn->un->bitflags.dummynode == 0
	  && dnptr->dn->un->bitflags.edgelabel == 0)
	{
	  on_top_level_window_drawingarea1_expose_event_nodes_layer_record (crp, dnptr->dn->un);
	  dnptr = dnptr->next;
	  continue;
	}

      /* dummy nodes and edgelabel do not have a border */
      if (dnptr->dn->un->bitflags.dummynode == 0 && dnptr->dn->un->bitflags.edgelabel == 0)
	{
	  /* regular node has a border and background */

	  /* fill the background color */
	  color = dnptr->dn->un->fillcolor;
	  r = (color & 0x00ff0000) >> 16;
	  g = (color & 0x0000ff00) >> 8;
	  b = (color & 0x000000ff);

	  /* change color if in alt color drawing mode */
	  if (altcolor)
	    {
	      if (dnptr->dn->un->bitflags.altcolor == 1)
		{
		  r = altr;
		  g = altg;
		  b = altb;
		}
	    }

	  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

	  /* start at */
	  x0 = (dnptr->dn->un->x0 - vxmin);
	  y0 = (dnptr->dn->un->y1 - vymin);

	  /* zz9 node line witdh for border thickness
	   * cairo_set_line_width(crp,9);
	   */

	  /* extra check */
	  if (dnptr->dn->un->fillcolor == 0)
	    {
	      printf ("%s(): warning black fillcolor\n", __FUNCTION__);
	    }

	  if (0)
	    {
	      printf ("%s(): shape %d\n", __FUNCTION__, dnptr->dn->un->bitflags.shape);
	    }

	  /* node shapes */
	  switch (dnptr->dn->un->bitflags.shape)
	    {
	    case NSHAPE_ELLIPS:
	      draw_nodeshape_ellips (crp, x0, y0, dnptr->dn->un);
	      break;
	    case NSHAPE_CIRCLE:
	      draw_nodeshape_circle (crp, x0, y0, dnptr->dn->un);
	      break;
	    case NSHAPE_RBOX:
	      draw_nodeshape_rbox (crp, x0, y0, dnptr->dn->un);
	      break;
	    case NSHAPE_BOX:
	      draw_nodeshape_box (crp, x0, y0, dnptr->dn->un);
	      break;
	    case NSHAPE_DBOX:	/* box with double border */
	      draw_nodeshape_dbox (crp, x0, y0, dnptr->dn->un);
	      break;
	    case NSHAPE_CARD:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CARD_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CARD_TYSIZE);
	      draw_nodeshape_card (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_CLOUD:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CLOUD_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CLOUD_TYSIZE);
	      draw_nodeshape_cloud (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_DIAMOND:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_DIAMOND_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_DIAMOND_TYSIZE);
	      draw_nodeshape_diamond (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_MDIAMOND:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_MDIAMOND_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_MDIAMOND_TYSIZE);
	      draw_nodeshape_mdiamond (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_HEXAGON:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_HEXAGON_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_HEXAGON_TYSIZE);
	      draw_nodeshape_hexagon (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_PENTAGON:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PENTAGON_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PENTAGON_TYSIZE);
	      draw_nodeshape_pentagon (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_FOLDER:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_FOLDER_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_FOLDER_TYSIZE);
	      draw_nodeshape_folder (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_SBOX:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SBOX_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SBOX_TYSIZE);
	      draw_nodeshape_sbox (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_CUBE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CUBE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CUBE_TYSIZE);
	      draw_nodeshape_cube (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_RCLOUD:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_RCLOUD_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_RCLOUD_TYSIZE);
	      draw_nodeshape_rcloud (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_CHEVRON:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CHEVRON_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CHEVRON_TYSIZE);
	      draw_nodeshape_chevron (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_CYLINDER:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CYLINDER_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CYLINDER_TYSIZE);
	      draw_nodeshape_cylinder (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_DIALOG:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_DIALOG_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_DIALOG_TYSIZE);
	      draw_nodeshape_dialog (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_MAXIMIZE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_MAXIMIZE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_MAXIMIZE_TYSIZE);
	      draw_nodeshape_maximize (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_PAGE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PAGE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PAGE_TYSIZE);
	      draw_nodeshape_page (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_PLAQUE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PLAQUE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PLAQUE_TYSIZE);
	      draw_nodeshape_plaque (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_PREP:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PREP_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PREP_TYSIZE);
	      draw_nodeshape_prep (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_PUNCHED:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PUNCHED_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PUNCHED_TYSIZE);
	      draw_nodeshape_punched (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_SEPTAGON:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SEPTAGON_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SEPTAGON_TYSIZE);
	      draw_nodeshape_septagon (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_SHIELD:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SHIELD_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SHIELD_TYSIZE);
	      draw_nodeshape_shield (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_STORED:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_STORED_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_STORED_TYSIZE);
	      draw_nodeshape_stored (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_TRAPEZOID:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_TRAPEZOID_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_TRAPEZOID_TYSIZE);
	      draw_nodeshape_trapezoid (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_TRIANGLE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_TRIANGLE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_TRIANGLE_TYSIZE);
	      draw_nodeshape_triangle (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_ITRIANGLE:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_ITRIANGLE_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_ITRIANGLE_TYSIZE);
	      draw_nodeshape_itriangle (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_VSCROLL:
	      svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_VSCROLL_TXSIZE);
	      svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_VSCROLL_TYSIZE);
	      draw_nodeshape_vscroll (crp, x0, y0, svgtsx, svgtsy, r, g, b, dnptr->dn->un);
	      break;
	    case NSHAPE_RECORD:	/* record shape is a box with sub boxes, hre if f==NULL at no-data */
	      svgtsx = 1.0;
	      svgtsy = 1.0;
	      draw_nodeshape_box (crp, x0, y0, dnptr->dn->un);
	      break;
	    default:
	      /* should not happen */
	      printf ("%s(): unknown shape %d\n", __FUNCTION__, dnptr->dn->un->bitflags.shape);
	      fflush (stdout);
	      svgtsx = 1.0;
	      svgtsy = 1.0;
	      draw_nodeshape_box (crp, x0, y0, dnptr->dn->un);
	      break;
	    }

	  /* endof if (edgelabel... */
	}

      /* now the shape is in drawing with background color
       * draw text of label and check scaling and fontsize
       */

      /* check if node has a specified font size */
      if (dnptr->dn->un->fontsize)
	{
	  default_fontsize = dnptr->dn->un->fontsize;
	}
      else
	{
	  default_fontsize = DEFAULT_FONTSIZE;
	}

      /* calculate the scaled font size */
      dfs = (zfactor * atoi (default_fontsize));
      /* somewhat rounded version */
      fs = (int) dfs;
      /* too small to draw readable text then skip the following text drawing area */
      if (fs < 3)
	{
	  /* next node in this layer */
	  dnptr = dnptr->next;
	  continue;
	}

      /* check for dummy node without text */
      if ((dnptr->dn->un->bitflags.dummynode == 0) && (dnptr->dn->un->label != NULL))
	{
	  /* check for "" because of gtk/cairo assert */
	  if (strlen (dnptr->dn->un->label) == 0)
	    {
	      /* skip text */
	    }
	  else if (strlen (dnptr->dn->un->utf8label) == 0)
	    {
	      /* skip text */
	    }
	  else
	    {
	      /* regular or edgelabel node with a text color */
	      color = dnptr->dn->un->textcolor;
	      r = (color & 0x00ff0000) >> 16;
	      g = (color & 0x0000ff00) >> 8;
	      b = (color & 0x000000ff);
	      /* change color if in alt color drawing mode */
	      if (altcolor)
		{
		  if (dnptr->dn->un->bitflags.altcolor == 1)
		    {
		      r = altr;
		      g = altg;
		      b = altb;
		    }
		}
	      /* draw in text color of node */
	      cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
	      /* adjust text position depending on node shapes */
	      switch (dnptr->dn->un->bitflags.shape)
		{
		case NSHAPE_ELLIPS:
		  xo = dnptr->dn->un->bbx / 4;
		  yo = dnptr->dn->un->ty / 4;
		  break;
		case NSHAPE_CIRCLE:
		  xo = (dnptr->dn->un->bbx / 2) - (dnptr->dn->un->tx / 2);
		  yo = (dnptr->dn->un->bby / 2) - (dnptr->dn->un->ty / 2);
		  break;
		case NSHAPE_BOX:
		  xo = (drawing_default_textborder / 4);
		  yo = (drawing_default_textborder / 4);
		  break;
		case NSHAPE_DBOX:
		  xo = (3 + 2 + (drawing_default_textborder / 4));
		  yo = (3 + 2 + (drawing_default_textborder / 4));
		  break;
		case NSHAPE_RBOX:
		  /* rbox is little bigger because of rounding */
		  xo = (drawing_default_textborder / 4) + 5;
		  yo = (drawing_default_textborder / 4) + 5;
		  break;
		case NSHAPE_CARD:
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CARD_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CARD_TYSIZE);
		  dxo = (svgtsx * (double) SVG_CARD_TOX);
		  dyo = (svgtsy * (double) SVG_CARD_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_CLOUD:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CLOUD_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CLOUD_TYSIZE);
		  dxo = (svgtsx * (double) SVG_CLOUD_TOX);
		  dyo = (svgtsy * (double) SVG_CLOUD_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_DIAMOND:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_DIAMOND_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_DIAMOND_TYSIZE);
		  dxo = (svgtsx * (double) SVG_DIAMOND_TOX);
		  dyo = (svgtsy * (double) SVG_DIAMOND_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_MDIAMOND:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_MDIAMOND_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_MDIAMOND_TYSIZE);
		  dxo = (svgtsx * (double) SVG_MDIAMOND_TOX);
		  dyo = (svgtsy * (double) SVG_MDIAMOND_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_HEXAGON:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_HEXAGON_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_HEXAGON_TYSIZE);
		  dxo = (svgtsx * (double) SVG_HEXAGON_TOX);
		  dyo = (svgtsy * (double) SVG_HEXAGON_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_PENTAGON:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PENTAGON_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PENTAGON_TYSIZE);
		  dxo = (svgtsx * (double) SVG_PENTAGON_TOX);
		  dyo = (svgtsy * (double) SVG_PENTAGON_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_FOLDER:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_FOLDER_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_FOLDER_TYSIZE);
		  dxo = (svgtsx * (double) SVG_FOLDER_TOX);
		  dyo = (svgtsy * (double) SVG_FOLDER_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_SBOX:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SBOX_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SBOX_TYSIZE);
		  dxo = (svgtsx * (double) SVG_SBOX_TOX);
		  dyo = (svgtsy * (double) SVG_SBOX_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_CUBE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CUBE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CUBE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_CUBE_TOX);
		  dyo = (svgtsy * (double) SVG_CUBE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_RCLOUD:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_RCLOUD_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_RCLOUD_TYSIZE);
		  dxo = (svgtsx * (double) SVG_RCLOUD_TOX);
		  dyo = (svgtsy * (double) SVG_RCLOUD_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_CHEVRON:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CHEVRON_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CHEVRON_TYSIZE);
		  dxo = (svgtsx * (double) SVG_CHEVRON_TOX);
		  dyo = (svgtsy * (double) SVG_CHEVRON_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_CYLINDER:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_CYLINDER_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_CYLINDER_TYSIZE);
		  dxo = (svgtsx * (double) SVG_CYLINDER_TOX);
		  dyo = (svgtsy * (double) SVG_CYLINDER_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_DIALOG:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_DIALOG_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_DIALOG_TYSIZE);
		  dxo = (svgtsx * (double) SVG_DIALOG_TOX);
		  dyo = (svgtsy * (double) SVG_DIALOG_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_MAXIMIZE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_MAXIMIZE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_MAXIMIZE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_MAXIMIZE_TOX);
		  dyo = (svgtsy * (double) SVG_MAXIMIZE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_PAGE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PAGE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PAGE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_PAGE_TOX);
		  dyo = (svgtsy * (double) SVG_PAGE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_PLAQUE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PLAQUE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PLAQUE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_PLAQUE_TOX);
		  dyo = (svgtsy * (double) SVG_PLAQUE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_PREP:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PREP_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PREP_TYSIZE);
		  dxo = (svgtsx * (double) SVG_PREP_TOX);
		  dyo = (svgtsy * (double) SVG_PREP_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_PUNCHED:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_PUNCHED_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_PUNCHED_TYSIZE);
		  dxo = (svgtsx * (double) SVG_PUNCHED_TOX);
		  dyo = (svgtsy * (double) SVG_PUNCHED_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_SEPTAGON:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SEPTAGON_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SEPTAGON_TYSIZE);
		  dxo = (svgtsx * (double) SVG_SEPTAGON_TOX);
		  dyo = (svgtsy * (double) SVG_SEPTAGON_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_SHIELD:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_SHIELD_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_SHIELD_TYSIZE);
		  dxo = (svgtsx * (double) SVG_SHIELD_TOX);
		  dyo = (svgtsy * (double) SVG_SHIELD_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_STORED:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_STORED_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_STORED_TYSIZE);
		  dxo = (svgtsx * (double) SVG_STORED_TOX);
		  dyo = (svgtsy * (double) SVG_STORED_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_TRAPEZOID:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_TRAPEZOID_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_TRAPEZOID_TYSIZE);
		  dxo = (svgtsx * (double) SVG_TRAPEZOID_TOX);
		  dyo = (svgtsy * (double) SVG_TRAPEZOID_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_TRIANGLE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_TRIANGLE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_TRIANGLE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_TRIANGLE_TOX);
		  dyo = (svgtsy * (double) SVG_TRIANGLE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_ITRIANGLE:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_ITRIANGLE_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_ITRIANGLE_TYSIZE);
		  dxo = (svgtsx * (double) SVG_ITRIANGLE_TOX);
		  dyo = (svgtsy * (double) SVG_ITRIANGLE_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_VSCROLL:
		  /* */
		  svgtsx = ((double) dnptr->dn->un->tx / (double) SVG_VSCROLL_TXSIZE);
		  svgtsy = ((double) dnptr->dn->un->ty / (double) SVG_VSCROLL_TYSIZE);
		  dxo = (svgtsx * (double) SVG_VSCROLL_TOX);
		  dyo = (svgtsy * (double) SVG_VSCROLL_TOY);
		  dxo = dxo + (double) (drawing_default_textborder / 4);
		  dyo = dyo + (double) (drawing_default_textborder / 4);
		  xo = (int) dxo;
		  yo = (int) dyo;
		  break;
		case NSHAPE_RECORD:	/* fall thru if record node has no data */
		default:
		  /* as box */
		  xo = (drawing_default_textborder / 4);
		  yo = (drawing_default_textborder / 4);
		  break;
		}
	      /* set start position of text */
	      cairo_move_to (crp, dnptr->dn->un->x0 - vxmin + xo, dnptr->dn->un->y1 - vymin + yo);
	      /* */
	      layout = pango_cairo_create_layout (crp);
	      /* set the text to draw which is 0 terminated */
	      pango_layout_set_text (layout, dnptr->dn->un->utf8label, -1);
	      /* set font parameters */
	      /* create the fontname description */
	      memset (buf, 0, 128);
	      /* check if node has a specified fontname */
	      if (dnptr->dn->un->fontname)
		{
		  default_fontname = dnptr->dn->un->fontname;
		}
	      else
		{
		  default_fontname = DEFAULT_FONTNAME;
		}
	      /* check if node has a specified font slant */
	      default_fontslant = DEFAULT_FONTSLANT;
	      if (dnptr->dn->un->bitflags.textoblique)
		{
		  default_fontslant = "Oblique";
		}
	      if (dnptr->dn->un->bitflags.textitalic)
		{
		  default_fontslant = "Italic";
		}
	      /* check if node has a specified font weight */
	      if (dnptr->dn->un->bitflags.textbold)
		{
		  default_fontweight = "Bold";
		}
	      else
		{
		  default_fontweight = DEFAULT_FONTWEIGHT;
		}
	      /* check if node has a specified font size */
	      if (dnptr->dn->un->fontsize)
		{
		  default_fontsize = dnptr->dn->un->fontsize;
		}
	      else
		{
		  default_fontsize = DEFAULT_FONTSIZE;
		}
	      /* create the font name string */
	      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	      /* copy string buffer */
	      s = uniqstring (buf);
	      /* */
	      desc = pango_font_description_from_string (s);
	      /* check and re-try with defaults */
	      if (desc == NULL)
		{
		  /* change node to use defaults */
		  dnptr->dn->un->fontname = NULL;
		  dnptr->dn->un->bitflags.textoblique = 0;
		  dnptr->dn->un->bitflags.textitalic = 0;
		  dnptr->dn->un->bitflags.textbold = 0;
		  dnptr->dn->un->fontsize = NULL;
		  /* name of font to use, example "Sans" */
		  default_fontname = DEFAULT_FONTNAME;
		  /* name of slant to use, example "Italic", "Oblique", "Roman" */
		  default_fontslant = DEFAULT_FONTSLANT;
		  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
		  default_fontweight = DEFAULT_FONTWEIGHT;
		  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
		  default_fontcondensed = DEFAULT_FONTCONDENSED;
		  /* font size to use, example "10", "18", "20" etc. */
		  default_fontsize = DEFAULT_FONTSIZE;
		  /* create the fontname description */
		  memset (buf, 0, 128);
		  /* create the font name string */
		  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
		  /* copy string buffer */
		  s = uniqstring (buf);
		  /* */
		  desc = pango_font_description_from_string (s);
		}
	      /* check */
	      if (desc == NULL)
		{
		  printf ("%s(): desc is NULL\n", __FUNCTION__);
		  fflush (stdout);
		}
	      /* */
	      pango_layout_set_font_description (layout, desc);
	      /* */
	      pango_font_description_free (desc);
	      /* */
	      pango_cairo_update_layout (crp, layout);
	      /* draw the text */
	      pango_cairo_show_layout (crp, layout);
	      /* */
	      cairo_stroke (crp);
	      /* if label is draw as shadowed text for summary nodes */
	      if (dnptr->dn->un->bitflags.textshadow)
		{
		  /* change color if in normal color drawing mode */
		  if (altcolor == 0)
		    {
		      if (dnptr->dn->un->bitflags.altcolor == 0)
			{
			  /* draw in text color of node */
			  cairo_set_source_rgb (crp, (r + 0x5f) / 255.0, (g + 0x5f) / 255.0, (b + 0x5f) / 255.0);
			}
		    }
		  /* set start position of text */
		  cairo_move_to (crp, dnptr->dn->un->x0 - vxmin + xo + 1, dnptr->dn->un->y1 - vymin + yo + 1);
		  /* */
		  pango_cairo_update_layout (crp, layout);
		  /* draw the text */
		  pango_cairo_show_layout (crp, layout);
		  /* */
		  cairo_stroke (crp);
		}
	      /* */
	      g_object_unref (G_OBJECT (layout));
	    }
	}
      /* next node in this layer */
      dnptr = dnptr->next;
    }

  return;
}

/* draw all edges */
void
on_top_level_window_drawingarea1_expose_event_edges (cairo_t * crp)
{
  int i = 0;

  /* start level can be 0 or 1 */
  for (i = dfsstartlevel; i < dli_nlevels; i = i + 2)
    {

      /* level i+1 has connecting dummynodes */
      if ((option_gdebug > 1) || 0)
	{
	  printf ("%s(): layer[%d] starts at y0=%d hn=%d he=%d total height=%d\n", __FUNCTION__, i, dlip[i]->y0, dlip[i]->hn, dlip[i]->ht, dlip[i]->ht);
	  fflush (stdout);
	}

      /* edge route from level n to level n+2 via dummynode level n+1 */
      on_top_level_window_drawingarea1_expose_event_edges_layer (crp, i);
    }

  return;
}

/* draw arrow for edge.
 * fixme only one arrow and dot has many arrow types.
 */
static void
on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (cairo_t * crp, int start_x, int start_y, int end_x, int end_y)
{
  double angle = 0.0;
  double arrow_lenght = 8.0;
  double arrow_degrees = ( /* degrees */ 30 * (M_PI / 180));
  double x1 = 0.0;
  double y1 = 0.0;
  double x2 = 0.0;
  double y2 = 0.0;

  /* gtk has a gtk_render_arrow() function as alternative to use here */
  angle = atan2 (end_y - start_y, end_x - start_x) + M_PI;
  x1 = end_x + arrow_lenght * cos (angle - arrow_degrees);
  y1 = end_y + arrow_lenght * sin (angle - arrow_degrees);
  x2 = end_x + arrow_lenght * cos (angle + arrow_degrees);
  y2 = end_y + arrow_lenght * sin (angle + arrow_degrees);

  /* set start position of arrow part 1 */
  cairo_move_to (crp, end_x, end_y);
  cairo_line_to (crp, x1, y1);

  /* set start position of arrow part 2 */
  cairo_move_to (crp, end_x, end_y);
  cairo_line_to (crp, x2, y2);

  return;
}

/*
 * draw all edges in one layer
 *
 * in draw layer info and node info are these (x,y) points:
 *
 * node coordinates are:
 *
 * dlip[layer]->x0
 * dlip[layer]->y0         un->x0 un->x1
 *             +--------------+----+---------------------+
 *             |                   |
 *             |              +---------+ un->y1
 *      dlip[layer]->hn       | node un |
 *             |              +---------+ un->y2
 *             |                   |
 *             +-------------------+----------------------+ dlip->layer[i]->wn x-size
 *             | y=(dlip[layer]->y0+dlip[layer]->hn)
 *             |
 *     dlip[layer]->he   edges area / \ or | lines
 *             |
 *             |
 *             +------------------------------------------+
 *             y=(dlip[layer]->y0+dlip[layer]ht)
 *
 * node x size is un->bbx
 * node y size is un->bby
 */
/* fixme this edge routing is too basic and needs whole rewrite for multiple connection point. src is prepared for that. */
static void
on_top_level_window_drawingarea1_expose_event_edges_layer (cairo_t * crp, int ylevel)
{
  int dunbit = 0;
  struct unode *dun = NULL;
  struct uedge *dunue = NULL;
  splay_tree dunsp = NULL;
  splay_tree_node dunspn = NULL;
  struct dli *lp = NULL;
  struct dde *deptr = NULL;
  const double dashed[] = {
    14.0, 2.0			/* 6.0 */
  };
  int lendashed = sizeof (dashed) / sizeof (dashed[0]);
  const double dotted[] = {
    1.0				/* 2.0, 6.0 */
  };
  int lendotted = sizeof (dotted) / sizeof (dotted[0]);
  int color = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  int fnx1 = 0;
  int fny2 = 0;
  int flhny = 0;
  int tny0 = 0;
  int tnx1 = 0;
  int tny1 = 0;
  int hx = 0;
  int hy = 0;
  int multarrows = 0;
  struct unode *deptr_de_ue_tn = NULL;

  /* choose level */
  lp = dlip[ylevel];

  /* scan edges in layer */
  deptr = lp->el;

  if ((option_gdebug > 1) || 0)
    {
      printf ("%s(): level %d edgelist el=%p nedges=%d nnodes=%d\n", __FUNCTION__, ylevel, (void *) lp->el, lp->nedges, lp->nnodes);
      fflush (stdout);
    }

  /* scan the drawing edge list */
  while (deptr)
    {

      /* */
      if ((option_gdebug > 1) || 0)
	{
	  printf ("%s(): edge from \"%s\" (level=%d) to \"%s\" (level=%d)\n",
		  __FUNCTION__, deptr->de->ue->fn->name, deptr->de->ue->fn->level, deptr->de->ue->tn->name, deptr->de->ue->tn->level);
	  fflush (stdout);
	}

      /* skip edge if both nodes are not draw */
      if ((dlip[deptr->de->ue->fn->level]->draw == 0) && (dlip[deptr->de->ue->tn->level]->draw == 0))
	{
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): skip edge both nodes not visible\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  deptr = deptr->next;
	  continue;
	}

      /* check for invisible edge */
      if (deptr->de->ue->bitflags.style == ESTYLE_INVIS)
	{
	  /* invisible edge and do not draw edge */
	  if ((option_gdebug > 1) || 0)
	    {
	      printf ("%s(): skip edge is invisible\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  deptr = deptr->next;
	  continue;
	}

      /* set edge line thickness */
      if (deptr->de->ue->bitflags.thickness == 0)
	{
	  cairo_set_line_width (crp, DEFAULT_EDGE_THICKNESS);
	}
      else
	{
	  cairo_set_line_width (crp, (double) deptr->de->ue->bitflags.thickness);
	}

      /* edge line color */
      color = deptr->de->ue->color;

      r = (color & 0x00ff0000) >> 16;
      g = (color & 0x0000ff00) >> 8;
      b = (color & 0x000000ff);

      /* change color if in alt color drawing mode */
      if (altcolor)
	{
	  if (deptr->de->ue->bitflags.altcolor == 1)
	    {
	      r = altr;
	      g = altg;
	      b = altb;
	    }
	}

      /* draw in line color of edge */
      cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

      /* check edge line style */
      if (deptr->de->ue->bitflags.style == ESTYLE_DASHED)
	{
	  /* dashed edge line */
	  cairo_set_dash (crp, dashed, lendashed, 1);
	}
      else if (deptr->de->ue->bitflags.style == ESTYLE_DOTTED)
	{
	  /* dotted edge line */
	  cairo_set_dash (crp, dotted, lendotted, 0);
	}
      else
	{
	  /* solid edge line */
	  cairo_set_dash (crp, dotted, 0, 0);
	}

      /* multiple arrows for long edges */
      multarrows = deptr->de->ue->bitflags.multarrows;

      /* override if option is set */
      if (option_multi_arrows)
	{
	  multarrows = 1;
	}

      if ((option_gdebug > 1) || 0)
	{
	  printf ("%s(): option_splines=%d\n", __FUNCTION__, option_splines);
	  fflush (stdout);
	}

      /* draw spline or regular edges */
      if (option_splines)
	{
	  /* soft bended edge lines */

/* XXX fix needed here in levels extra */
	  /* skip past */
	  deptr_de_ue_tn = deptr->de->ue->tn->dout->tn;

	  /* calculate some points */
	  fnx1 = deptr->de->ue->fn->x1 - vxmin;
	  fny2 = deptr->de->ue->fn->y2 - vymin;
	  flhny = (dlip[deptr->de->ue->fn->level]->y0 + dlip[deptr->de->ue->fn->level]->hn) - vymin;
	  tny0 = dlip[deptr_de_ue_tn->level]->y0 - vymin;
	  tnx1 = deptr_de_ue_tn->x1 - vxmin;
	  tny1 = deptr_de_ue_tn->y1 - vymin;

	  /* spline edges
	   * from dummy->dummy
	   * dummy->real
	   * real->dummy
	   * real->real
	   */
	  if (deptr->de->ue->fn->bitflags.dummynode == 0 && deptr->de->ue->tn->bitflags.dummynode == 0)
	    {
	      /* from real node to real node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 1) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at from node but not at edge label node */
		  if (deptr->de->ue->fn->bitflags.edgelabel == 0)
		    {
		      /* if the from-line is too short use longer */
		      if ((flhny - fny2) < 3)
			{
			  /* angle of spline line is not known */
			  if (fnx1 < tnx1)
			    {
			      /* line from left to right */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1 + 3, fny2 + 5, fnx1, fny2);
			    }
			  else
			    {
			      /* line from right to left */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1 - 3, fny2 + 5, fnx1, fny2);
			    }
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
			}
		    }
		}
	      /* draw edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      /* halfway */
	      hx = (tnx1 - fnx1) / 2 + fnx1;
	      hy = (tny0 - flhny) / 2 + flhny;
	      if (fnx1 < tnx1)
		{
		  /* line from left to right */
		  cairo_curve_to (crp, (fnx1 + 1), (flhny + 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      else
		{
		  /* line from right to left */
		  cairo_curve_to (crp, (fnx1 - 1), (flhny - 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      cairo_line_to (crp, tnx1, tny1);
	      /* draw arrow at to node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 0) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at to node but not at edge label node */
		  if (deptr->de->ue->tn->bitflags.edgelabel == 0)
		    {
		      if ((tny1 - tny0) < 3)
			{
			  /* angle of spline line is not known */
			  if (fnx1 < tnx1)
			    {
			      /* line from left to right */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1 - 3, tny1 - 5, tnx1, tny1);
			    }
			  else
			    {
			      /* line from right to left */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1 + 3, tny1 - 5, tnx1, tny1);
			    }
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}
	    }
	  else if (deptr->de->ue->fn->bitflags.dummynode == 0 && deptr->de->ue->tn->bitflags.dummynode == 1)
	    {
	      /* from real node to dummy node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 1) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at from node but not at edge label node */
		  if (deptr->de->ue->fn->bitflags.edgelabel == 0)
		    {
		      /* if the from-line is too short use longer */
		      if ((flhny - fny2) < 3)
			{
			  /* angle of spline line is not known */
			  if (fnx1 < tnx1)
			    {
			      /* line from left to right */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1 + 3, fny2 + 5, fnx1, fny2);
			    }
			  else
			    {
			      /* line from right to left */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1 - 3, fny2 + 5, fnx1, fny2);
			    }
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
			}
		    }
		}
	      /* edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      /* halfway */
	      hx = (((tnx1 - fnx1) / 2) + fnx1);
	      hy = (((tny0 - flhny) / 2) + flhny);
	      /* check horzontal edge */
	      if (deptr->de->ue->bitflags.horedge)
		{
		  /* angle how the edge line approaches the dummy node is different here. to update and tune more. */
		  if (fnx1 < tnx1)
		    {
		      /* line from left to right */
		      cairo_curve_to (crp, (fnx1 + 1), (flhny + 1), fnx1, (flhny + 10), hx, hy);
		      cairo_curve_to (crp, hx, hy, (tnx1 - 10), (tny1 - 5), tnx1, tny0);
		    }
		  else
		    {
		      /* line from right to left */
		      cairo_curve_to (crp, (fnx1 - 1), (flhny - 1), fnx1, (flhny + 10), hx, hy);
		      cairo_curve_to (crp, hx, hy, (tnx1 - 10), (tny1 - 5), tnx1, tny0);
		    }
		}
	      else
		{
		  if (fnx1 < tnx1)
		    {
		      /* line from left to right */
		      cairo_curve_to (crp, (fnx1 + 1), (flhny + 1), fnx1, (flhny + 10), hx, hy);
		      cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		    }
		  else
		    {
		      /* line from right to left */
		      cairo_curve_to (crp, (fnx1 - 1), (flhny - 1), fnx1, (flhny + 10), hx, hy);
		      cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		    }
		}
	      /* vertical line to center of dummy node to skip in some situations.
	       * need to check the edges connected with the to-node which is a dummy node.
	       * only draw this small line when both edges at dummy node are downward drawing.
	       * a dummy node is supposed to have always exactly two edges because it is
	       * only a splitting in a longer edge or redirecting a horizontal edge.
	       * indegree is 1 and outdegree is 1 at a dummy node or 2 at reversed edges.
	       */
	      dun = deptr->de->ue->tn;
	      /* params check */
	      if ((dun->indegree == 1) || (dun->outdegree == 1) || (dun->indegree == 2) || (dun->outdegree == 2))
		{
		  /* check the edges at this dummy node */
		  dunbit = 0;
		  dunsp = dun->sp_poe;
		  dunspn = splay_tree_min (dunsp);
		  while (dunspn)
		    {
		      dunue = (struct uedge *) dunspn->value;
		      dunbit = (dunbit + (int) dunue->bitflags.reversed);
		      dunspn = splay_tree_successor (dunsp, dunspn->key);
		    }
		}
	      else
		{
		  printf ("%s(): unexpected degree %d:%d at dummynode %d %s and skipped edge check shouldnothappen!\n", __FUNCTION__, dun->indegree,
			  dun->outdegree, dun->number, dun->name);
		  fflush (stdout);
		}
	      /* if no reverted edges or both reverted edges draw this line */
	      if ((dunbit == 0) || (dunbit == 2))
		{
		  cairo_line_to (crp, tnx1, tny1);
		}
	      else
		{
		  /* skip this line */
		}
	      /*
	       * check if this is a horizontal edge
	       * between two real nodes a dummy node at next level.
	       */
	      if (deptr->de->ue->bitflags.horedge)
		{
		  /* do not draw arrow at hor. edge, it looks not so good */
		}
	      else
		{
		  /* check for multiple arrows also at dummy nodes */
		  if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		    {
		      if (deptr->de->ue->bitflags.reversed == 1)
			{
			  /* up arrow */
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny1 + 10, tnx1, tny1);
			}
		      else
			{
			  /* down arrow */
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}
	    }
	  else if (deptr->de->ue->fn->bitflags.dummynode == 1 && deptr->de->ue->tn->bitflags.dummynode == 0)
	    {
	      /* from dummy node to real node */
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, fny2 - 10, fnx1, fny2);
		    }
		}
	      /* edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      /* halfway */
	      hx = ((tnx1 - fnx1) / 2) + fnx1;
	      hy = ((tny0 - flhny) / 2) + flhny;
	      if (fnx1 < tnx1)
		{
		  /* line from left to right */
		  cairo_curve_to (crp, (fnx1 + 1), (flhny + 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      else
		{
		  /* line from right to left */
		  cairo_curve_to (crp, (fnx1 - 1), (flhny - 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      cairo_line_to (crp, tnx1, tny1);
	      /* draw arrow at to node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 0) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at to node but not at edge label node */
		  if (deptr->de->ue->tn->bitflags.edgelabel == 0)
		    {
		      if ((tny1 - tny0) < 3)
			{
			  /* angle of spline line is not known */
			  if (fnx1 < tnx1)
			    {
			      /* line from left to right */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1 - 3, tny1 - 5, tnx1, tny1);
			    }
			  else
			    {
			      /* line from right to left */
			      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1 + 3, tny1 - 5, tnx1, tny1);
			    }
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}

	    }
	  else
	    {
	      /* from dummy node to dummy node */
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, fny2 - 10, fnx1, fny2);
		    }
		}
	      /* edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      /* halfway */
	      hx = ((tnx1 - fnx1) / 2) + fnx1;
	      hy = ((tny0 - flhny) / 2) + flhny;
	      if (fnx1 < tnx1)
		{
		  /* line from left to right */
		  cairo_curve_to (crp, (fnx1 + 1), (flhny + 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      else
		{
		  /* line from right to left */
		  cairo_curve_to (crp, (fnx1 - 1), (flhny - 1), fnx1, (flhny + 10), hx, hy);
		  cairo_curve_to (crp, hx, hy, tnx1, (tny0 - 10), tnx1, tny0);
		}
	      cairo_line_to (crp, tnx1, tny1);
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny1 + 10, tnx1, tny1);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
		    }
		}
	    }

	}
      else
	{
	  /* no spline edges but straight point to point */

	  /* skip past */
	  deptr_de_ue_tn = deptr->de->ue->tn->dout->tn;

	  /* calculate some points */
	  fnx1 = deptr->de->ue->fn->x1 - vxmin;
	  fny2 = deptr->de->ue->fn->y2 - vymin;
	  flhny = (dlip[deptr->de->ue->fn->level]->y0 + dlip[deptr->de->ue->fn->level]->hn) - vymin;
	  tny0 = dlip[deptr_de_ue_tn->level]->y0 - vymin;
	  tnx1 = deptr_de_ue_tn->x1 - vxmin;
	  tny1 = deptr_de_ue_tn->y1 - vymin;

	  /* fixme more points needed */

	  /* regular edges */
	  if (deptr->de->ue->fn->bitflags.dummynode == 0 && deptr->de->ue->tn->bitflags.dummynode == 0)
	    {
	      /* from real node to real node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 1) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at from node but not at edge label node */
		  if (deptr->de->ue->fn->bitflags.edgelabel == 0)
		    {
		      /* if the from-line is too short use longer */
		      if ((flhny - fny2) < 4)
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, fnx1, fny2);
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
			}
		    }
		}
	      /* draw edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      cairo_line_to (crp, tnx1, tny0);
	      cairo_line_to (crp, tnx1, tny1);
	      /* draw arrow at to node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 0) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at to node but not at edge label node */
		  if (deptr->de->ue->tn->bitflags.edgelabel == 0)
		    {
		      if ((tny1 - tny0) < 4)
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, tnx1, tny1);
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}
	    }
	  else if (deptr->de->ue->fn->bitflags.dummynode == 0 && deptr->de->ue->tn->bitflags.dummynode == 1)
	    {
	      /* from real node to dummy node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 1) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at from node but not at edge label node */
		  if (deptr->de->ue->fn->bitflags.edgelabel == 0)
		    {
		      /* if the from-line is too short use longer */
		      if ((flhny - fny2) < 4)
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, fnx1, fny2);
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
			}
		    }
		}
	      /* draw edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      /* check for horizontal edge. to update tune more. */
	      if (deptr->de->ue->bitflags.horedge)
		{
		  /* skip small line segment if this is a hor. edge toedoe */
		  cairo_line_to (crp, tnx1, tny0);
		}
	      else
		{
		  cairo_line_to (crp, tnx1, tny0);
		}

	      /* vertical line to center of dummy node to skip in some situations.
	       * need to check the edges connected with the to-node which is a dummy node.
	       * only draw this small line when both edges at dummy node are downward drawing.
	       * a dummy node is supposed to have always exactly two edges because it is
	       * only a splitting in a longer edge or redirecting a horizontal edge.
	       * indegree is 1 and outdegree is 1 at a dummy node or 2 at reversed edges.
	       */
	      dun = deptr->de->ue->tn;
	      /* params check */
	      if ((dun->indegree == 1) || (dun->outdegree == 1) || (dun->indegree == 2) || (dun->outdegree == 2))
		{
		  /* check the edges at this dummy node */
		  dunbit = 0;
		  dunsp = dun->sp_poe;
		  dunspn = splay_tree_min (dunsp);
		  while (dunspn)
		    {
		      dunue = (struct uedge *) dunspn->value;
		      dunbit = (dunbit + (int) dunue->bitflags.reversed);
		      dunspn = splay_tree_successor (dunsp, dunspn->key);
		    }
		}
	      else
		{
		  printf ("%s(): unexpected degree %d:%d at dummynode %d %s and skipped edge check shouldnothappen!\n", __FUNCTION__, dun->indegree,
			  dun->outdegree, dun->number, dun->name);
		  fflush (stdout);
		}
	      /* if no reverted edges or both reverted edges draw this line */
	      if ((dunbit == 0) || (dunbit == 2))
		{
		  cairo_line_to (crp, tnx1, tny1);
		}
	      else
		{
		  /* skip this line */
		}
	      /* check if this is a horizontal edge
	       * between two real nodes a dummy node at next level.
	       */
	      if (deptr->de->ue->bitflags.horedge)
		{
		  /* do not draw arrow at hor. edge, it looks not so good */
		}
	      else
		{
		  /* check for multiple arrows also at dummy nodes */
		  if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		    {
		      if (deptr->de->ue->bitflags.reversed == 1)
			{
			  /* up arrow */
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny1 + 10, tnx1, tny1);
			}
		      else
			{
			  /* down arrow */
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}
	    }
	  else if (deptr->de->ue->fn->bitflags.dummynode == 1 && deptr->de->ue->tn->bitflags.dummynode == 0)
	    {
	      /* from dummy node to real node */
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, fny2 - 10, fnx1, fny2);
		    }
		}
	      /* draw edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      cairo_line_to (crp, tnx1, tny0);
	      cairo_line_to (crp, tnx1, tny1);
	      /* draw arrow at to node */
	      if ((deptr->de->ue->bitflags.arrows == 1) && ((deptr->de->ue->bitflags.reversed == 0) || (deptr->de->ue->bitflags.botharrows == 1)))
		{
		  /* draw arrow at to node but not at edge label node */
		  if (deptr->de->ue->tn->bitflags.edgelabel == 0)
		    {
		      if ((tny1 - tny0) < 4)
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, tnx1, tny1);
			}
		      else
			{
			  on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
			}
		    }
		}
	    }
	  else
	    {
	      /* from dummy node to dummy node */
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, flhny, fnx1, fny2);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, fnx1, fny2 - 10, fnx1, fny2);
		    }
		}
	      /* draw edge lines */
	      cairo_move_to (crp, fnx1, fny2);
	      cairo_line_to (crp, fnx1, flhny);
	      cairo_line_to (crp, tnx1, tny0);
	      cairo_line_to (crp, tnx1, tny1);
	      /* check for multiple arrows also at dummy nodes */
	      if ((deptr->de->ue->bitflags.arrows == 1) && (multarrows == 1))
		{
		  if (deptr->de->ue->bitflags.reversed == 1)
		    {
		      /* up arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny1 + 10, tnx1, tny1);
		    }
		  else
		    {
		      /* down arrow */
		      on_top_level_window_drawingarea1_expose_event_edges_layer_arrow (crp, tnx1, tny0, tnx1, tny1);
		    }
		}
	    }
	}

      /* cairo_close_path (crp); causes draw errors */
      cairo_stroke (crp);

      /* next edge */
      deptr = deptr->next;
    }

  return;
}

/* check for valid save filename
 * gchar *
 * g_filename_from_utf8 (const gchar *utf8string,
 * gssize len,
 * gsize *bytes_read,
 * gsize *bytes_written,
 * GError **error);
 *
 * Converts a string from UTF-8 to the encoding GLib uses for filenames.
 * Note that on Windows GLib uses UTF-8 for filenames;
 * on other platforms, this function indirectly depends on the current locale.
 *
 * Parameters
 * utf8string
 * a UTF-8 encoded string.
 * len
 * the length of the string, or -1 if the string is nul-terminated.
 * bytes_read
 * location to store the number of bytes in the input string that were successfully converted, or NULL.
 * Even if the conversion was successful, this may be less than len if there were partial characters
 * at the end of the input.
 * If the error G_CONVERT_ERROR_ILLEGAL_SEQUENCE occurs,
 * the value stored will the byte offset after the last valid input sequence.
 * [out][allow-none]
 * bytes_written
 * the number of bytes stored in the output buffer (not including the terminating nul).
 * [out]
 * error
 * location to store the error occurring, or NULL to ignore errors.
 * Any of the errors in GConvertError may occur.
 * Returns
 * The converted string, or NULL on an error.
 */
char *
valid_save_filename (char *fname)
{
  char *s = NULL;
  gchar *ret = NULL;
  if (fname == NULL)
    {
      return ((char *) 0);
    }
  if (strlen (fname) == 0)
    {
      return ((char *) "");
    }
  s = fname;
  ret = (gchar *) g_filename_from_utf8 ((const gchar *) s, (gssize) - 1 /* 0 terminated */ ,
					(gsize *) 0 /* bytes_read */ ,
					(gsize *) 0 /* bytes_written */ ,
					(GError **) 0 /* error */ );
  /* check for error */
  if (ret == NULL)
    {
      /* use the raw input */
      s = uniqstring (fname);
    }
  else
    {
      /* used converted string */
      s = uniqstring ((char *) ret);
    }
  return ((char *) s);
}

/* check for valid open filename
 * gchar *
 * g_filename_to_utf8 (const gchar *opsysstring,
 * gssize len,
 * gsize *bytes_read,
 * gsize *bytes_written,
 * GError **error);
 *
 * Converts a string which is in the encoding used by GLib for filenames into a UTF-8 string.
 * Note that on Windows GLib uses UTF-8 for filenames;
 * on other platforms, this function indirectly depends on the current locale.
 * Parameters
 * opsysstring
 * a string in the encoding for filenames
 * len
 * the length of the string, or -1 if the string is nul-terminated
 * (Note that some encodings may allow nul bytes to occur inside strings.
 * In that case, using -1 for the len parameter is unsafe)
 * bytes_read
 * location to store the number of bytes in the input string that were successfully converted, or NULL.
 * Even if the conversion was successful, this may be less than len if there were partial characters
 * at the end of the input.
 * If the error G_CONVERT_ERROR_ILLEGAL_SEQUENCE occurs,
 * the value stored will the byte offset after the last valid input sequence.
 * bytes_written
 * the number of bytes stored in the output buffer (not including the terminating nul).
 * error
 * location to store the error occurring, or NULL to ignore errors.
 * Any of the errors in GConvertError may occur.
 * Returns
 * The converted string, or NULL on an error.
 */
static char *
valid_open_filename (char *fname)
{
  char *s = NULL;
  gchar *ret = NULL;
  if (fname == NULL)
    {
      return ((char *) 0);
    }
  if (strlen (fname) == 0)
    {
      return ((char *) "");
    }
  s = fname;
  ret = (gchar *) g_filename_to_utf8 ((const gchar *) s, (gssize) - 1 /* 0 terminated */ ,
				      (gsize *) 0 /* bytes_read */ ,
				      (gsize *) 0 /* bytes_written */ ,
				      (GError **) 0 /* error */ );
  /* check for conversion error */
  if (ret == NULL)
    {
      /* use raw input */
      s = uniqstring (fname);
    }
  else
    {
      s = uniqstring ((char *) ret);
    }
  return (s);
}

/* check for valid text label
 *
 * g_utf8_validate ()
 *
 * gboolean
 * g_utf8_validate (const gchar *str,
 * gssize max_len,
 * const gchar **end);
 *
 * Validates UTF-8 encoded text.
 * str is the text to validate; if str is nul-terminated,
 * then max_len can be -1, otherwise max_len should be the number of bytes to validate.
 * If end is non-NULL, then the end of the valid range will be stored there
 * (i.e. the start of the first invalid character if some bytes were invalid,
 * or the end of the text being validated otherwise).
 * Note that g_utf8_validate() returns FALSE if max_len is positive
 * and any of the max_len bytes are nul.
 *
 * Returns TRUE if all of str was valid.
 * Many GLib and GTK+ routines require valid UTF-8 as input;
 * so data read from a file or the network should be checked with
 * g_utf8_validate() before doing anything else with it.
 *
 * Parameters
 *
 * str
 * a pointer to character data.
 * [array length=max_len][element-type guint8]
 * max_len
 * max bytes to validate, or -1 to go until NUL
 * end
 * return location for end of valid data.
 * [allow-none][out][transfer none]
 *
 * Returns
 * TRUE if the text was valid UTF-8
 *
 * if the utf8 label is not oke then pango will print an
 * warning message on stderr from pango_layout_set_text()
 */
static char *
valid_label_string (char *label)
{
  char *s = NULL;
  gboolean ust = 0;
  const gchar *end = NULL;
  char *el = NULL;
  char *utf8name = NULL;
  GError *end2 = NULL;
  if (label == NULL)
    {
      return (NULL);
    }
  if (strlen (label) == 0)
    {
      return ((char *) "");
    }
  el = label;
  /* utf8 verify */
  ust = g_utf8_validate ((const gchar *) el, (gssize) - 1 /* 0 terminated */ ,
			 (const gchar **) &end);
  /* check for utf8 error */
  if ((ust == 0) || strlen (end))
    {
      /* try to convert into utf8 */
      utf8name = g_locale_to_utf8 ((const gchar *) el, (gssize) - 1 /* 0 terminated */ ,
				   (gsize *) 0 /* bytes read */ ,
				   (gsize *) 0 /* bytes written */ ,
				   (GError **) & end2 /* error */ );
      /* set at error */
      if (end2)
	{
	}
      /* check if success */
      if (utf8name == NULL)
	{
	  el = "utf8error";
	}
      else
	{
	  if (strlen (utf8name) == 0)
	    {
	      el = "utf8error0";
	    }
	  else
	    {
	      el = uniqstring (utf8name);
	    }
	}
      g_free (utf8name);
    }
  s = uniqstring (el);
  if (strlen (s) == 0)
    {
      printf ("%s(): strlen(s) is 0 for \"%s\"\n", __FUNCTION__, label);
      fflush (stdout);
    }
  return ((char *) s);
}

/* print data in fields in a record node */
static void
static_maingtk_textsizes_r3 (struct fld *root)
{
  int ix = 0;
  struct fld *tmpf = NULL;
  if (root == NULL)
    {
      return;
    }
  if (root->f == NULL)
    {
      return;
    }
  ix = 0;
  printf ("%s(): @root=%p ix=%d dir=%d bbxy (%d,%d) size (%d,%d) size0 (%d,%d) \"%s\"\n", __FUNCTION__, (void *) root, ix, root->dir, root->bbx, root->bby,
	  root->xsiz, root->ysiz, root->xsiz0, root->ysiz0, root->label);
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      printf ("%s(): root=%p ix=%d dir=%d bbxy (%d,%d) size (%d,%d) size0 (%d,%d) \"%s\"\n", __FUNCTION__, (void *) root, ix, tmpf->dir, tmpf->bbx, tmpf->bby,
	      tmpf->xsiz, tmpf->ysiz, tmpf->xsiz0, tmpf->ysiz0, tmpf->label);
      ix++;
    }
  return;
}

/* print data in fields in a record node */
static void
static_maingtk_textsizes_r2 (struct unode *un, struct fld *root)
{
  int ix = 0;
  struct fld *tmpf = NULL;
  ix = 0;
  tmpf = NULL;
  /* should not happen */
  if (root == NULL)
    {
      return;
    }
  if (root->f == NULL)
    {
      return;
    }
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      /* start it 0 index with a label */
      /* if (ix == 0 && tmpf->label != NULL) */
      {
	/* print data in it */
	static_maingtk_textsizes_r3 (root);
      }
      ix++;
      static_maingtk_textsizes_r2 (un, tmpf);
    }
  return;
}


/* process more of the node sizes */
static struct pnt
static_maingtk_textsizes_siz1r (struct unode *un, struct fld *root)
{
  struct pnt dsiz0;
  struct pnt dsiz;
  int ix = 0;
  struct fld *tmpf = NULL;
  ix = 0;
  tmpf = NULL;
  /* get size of known sublabel */
  if (root->label)
    {
      dsiz.x = root->bbx;
      dsiz.y = root->bby;
      if (option_gdebug > 1 || 0)
	{
	  printf ("%s(0): return (%d,%d) \"%s\"\n", __FUNCTION__, dsiz.x, dsiz.y, root->label);
	}
      root->xsiz = dsiz.x;
      root->ysiz = dsiz.y;
      return (dsiz);
    }
  /* calc size of children summed */
  dsiz.x = 0;
  dsiz.y = 0;
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      dsiz0 = static_maingtk_textsizes_siz1r (un, tmpf);
      if (option_gdebug > 1 || 0)
	{
	  printf ("%s(): size (%d,%d) dir=%d for \"%s\"\n", __FUNCTION__, dsiz0.x, dsiz0.y, tmpf->dir, tmpf->label);
	}
      if (tmpf->dir == 0)
	{
	  /* going horizontal */
	  dsiz.x = dsiz.x + dsiz0.x;
	  /* get max y-size */
	  if (dsiz0.y > dsiz.y)
	    {
	      dsiz.y = dsiz0.y;
	    }
	}
      else
	{
	  /* going vertical */
	  if (dsiz0.x > dsiz.x)
	    {
	      dsiz.x = dsiz0.x;
	    }
	  /* sum y-direction in pixels */
	  dsiz.y = dsiz.y + dsiz0.y;
	}
      ix++;
    }
  if (option_gdebug > 1 || 0)
    {
      printf ("%s(1): return (%d,%d) %p \"%s\"\n", __FUNCTION__, dsiz.x, dsiz.y, (void *) root, root->label);
    }
  root->xsiz = dsiz.x;
  root->ysiz = dsiz.y;
  return (dsiz);
}

/* zz9 */
static struct pnt
static_maingtk_textsizes_siz2r (struct fld *root, int xoff, int yoff)
{
  struct pnt dsiz0;
  struct pnt dsiz;
  int ix = 0;
  struct fld *tmpf = NULL;
  int xo = 0;
  int yo = 0;
  ix = 0;
  tmpf = NULL;
  /* get size of known sublabel */
  if (root->label)
    {
      dsiz.x = root->bbx;
      dsiz.y = root->bby;
      if (option_gdebug > 1 || 0)
	{
	  printf ("%s(0): return (%d,%d) \"%s\"\n", __FUNCTION__, dsiz.x, dsiz.y, root->label);
	}
      root->xsiz0 = dsiz.x;
      root->ysiz0 = dsiz.y;
      return (dsiz);
    }
  /* calc size of children summed */
  dsiz.x = 0;
  dsiz.y = 0;
  xo = 0;
  yo = 0;
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      if (tmpf->dir == 0)
	{
	  tmpf->x0 = xo + xoff;
	  tmpf->y0 = yoff;
	}
      else
	{
	  tmpf->x0 = xoff;
	  tmpf->y0 = yo + yoff;
	}
      if (tmpf->dir == 0)
	{
	  dsiz0 = static_maingtk_textsizes_siz2r (tmpf, xo, yoff);
	}
      else
	{
	  dsiz0 = static_maingtk_textsizes_siz2r (tmpf, xoff, yo);
	}
      if (option_gdebug > 1 || 0)
	{
	  printf ("%s(): size (%d,%d) dir=%d for \"%s\"\n", __FUNCTION__, dsiz0.x, dsiz0.y, tmpf->dir, tmpf->label);
	}
      if (tmpf->dir == 0)
	{
	  /* going horizontal */
	  dsiz.x = dsiz.x + dsiz0.x;
	  /* get max y-size */
	  if (dsiz0.y > dsiz.y)
	    {
	      dsiz.y = dsiz0.y;
	    }
	}
      else
	{
	  /* going vertical */
	  if (dsiz0.x > dsiz.x)
	    {
	      dsiz.x = dsiz0.x;
	    }
	  /* sum y-direction in pixels */
	  dsiz.y = dsiz.y + dsiz0.y;
	}
      xo = dsiz.x;
      yo = dsiz.y;
      ix++;
    }
  if (option_gdebug > 1 || 0)
    {
      printf ("%s(1): return (%d,%d) \"%s\"\n", __FUNCTION__, dsiz.x, dsiz.y, root->label);
    }
  root->xsiz = dsiz.x;
  root->ysiz = dsiz.y;
  return (dsiz);
}

static void
static_maingtk_textsizes_r4 (struct fld *root, int x, int y)
{
  int ix = 0;
  struct fld *tmpf = NULL;
  if (root == NULL)
    {
      return;
    }
  if (root->f == NULL)
    {
      return;
    }
  ix = 0;
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      if (tmpf->label)
	{
	  if (tmpf->dir == 0)
	    {
	      tmpf->ysiz0 = y;
	    }
	  else
	    {
	      tmpf->xsiz0 = x;
	    }
	}
      ix++;
    }

  ix = 0;
  for (;;)
    {
      if (root->f == NULL)
	{
	  break;
	}
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  break;
	}
      if (tmpf->label == NULL)
	{
	  static_maingtk_textsizes_r4 (tmpf, tmpf->xsiz, tmpf->ysiz);
	}
      ix++;
    }
  return;
}

/* get sizes of text in fields in a record node */
static void
static_maingtk_textsizes_r (struct unode *un, struct fld *root)
{
  int ix = 0;
  struct fld *tmpf = NULL;
  cairo_surface_t *surface = NULL;
  cairo_t *crdraw = NULL;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  char buf[128];
  char *s = NULL;
  int w = 0;
  int h = 0;
  ix = 0;
  tmpf = NULL;
  /* should not happen */
  if (root == NULL)
    {
      return;
    }
  if (root->f == NULL)
    {
      return;
    }
  /* scan all ix */
  for (;;)
    {
      tmpf = root->f[ix];
      if (tmpf == NULL)
	{
	  /* NULL is end of pointer list */
	  break;
	}
      /* label text can be NULL */
      if (tmpf->label)
	{

	  /* change "" into " " if needed */
	  if (strlen (tmpf->label) == 0)
	    {
	      tmpf->label = uniqstring (" ");
	    }

	  /* calculate the text area */
	  if (surface == NULL)
	    {
	      surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 10, 10);
	    }
	  /* */
	  if (crdraw == NULL)
	    {
	      crdraw = cairo_create (surface);
	    }
	  /* */
	  layout = pango_cairo_create_layout (crdraw);
	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, tmpf->label, -1);
	  /* create the fontname description */
	  memset (buf, 0, 128);
	  /* check if node has a specified fontname */
	  if (un->fontname)
	    {
	      default_fontname = un->fontname;
	    }
	  else
	    {
	      default_fontname = uniqstring ((char *) DEFAULT_FONTNAME);
	    }
	  /* check if node has a specified font slant */
	  default_fontslant = uniqstring ((char *) DEFAULT_FONTSLANT);
	  if (un->bitflags.textoblique)
	    {
	      default_fontslant = uniqstring ((char *) "Oblique");
	    }
	  if (un->bitflags.textitalic)
	    {
	      default_fontslant = uniqstring ((char *) "Italic");
	    }
	  /* check if node has a specified font weight */
	  default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	  if (un->bitflags.textbold)
	    {
	      default_fontweight = uniqstring ((char *) "Bold");
	    }
	  else
	    {
	      default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	    }
	  /* check if node has a specified font size */
	  if (un->fontsize)
	    {
	      default_fontsize = un->fontsize;
	    }
	  else
	    {
	      default_fontsize = uniqstring ((char *) DEFAULT_FONTSIZE);
	    }
	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	  /* copy string buffer */
	  s = uniqstring (buf);
	  /* */
	  desc = pango_font_description_from_string (s);
	  /* check and re-try with defaults */
	  if (desc == NULL)
	    {
	      /* name of font to use, example "Sans" */
	      default_fontname = uniqstring ((char *) DEFAULT_FONTNAME);
	      /* name of slant to use, example "Italic", "Oblique", "Roman" */
	      default_fontslant = uniqstring ((char *) DEFAULT_FONTSLANT);
	      /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
	      default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	      /* name of condensed to use, example "Semi-Condensed", "Condensed" */
	      default_fontcondensed = uniqstring ((char *) DEFAULT_FONTCONDENSED);
	      /* font size to use, example "10", "18", "20" etc. */
	      default_fontsize = uniqstring ((char *) DEFAULT_FONTSIZE);
	      /* change node to use defaults in next situations */
	      un->fontname = NULL;
	      un->bitflags.textoblique = 0;
	      un->bitflags.textitalic = 0;
	      un->bitflags.textbold = 0;
	      un->fontsize = NULL;
	      /* create the fontname description */
	      memset (buf, 0, 128);
	      /* create the font name string */
	      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	      /* copy string buffer */
	      s = uniqstring (buf);
	      /* */
	      desc = pango_font_description_from_string (s);
	    }
	  /* check */
	  if (desc == NULL)
	    {
	      printf ("%s(): desc is NULL\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  /* */
	  pango_layout_set_font_description (layout, desc);
	  /* */
	  pango_font_description_free (desc);
	  /* */
	  pango_cairo_update_layout (crdraw, layout);
	  /* */
	  w = 0;
	  h = 0;
	  /* */
	  pango_layout_get_size (layout, &w, &h);
	  /* */
	  g_object_unref (G_OBJECT (layout));
	  /* */
	  if (option_gdebug > 1 || 0)
	    {
	      printf ("%s(): size (%d,%d) for \"%s\"\n", __FUNCTION__, (w / PANGO_SCALE), (h / PANGO_SCALE), tmpf->label);
	      fflush (stdout);
	    }
	  /* set the text area size */
	  tmpf->tx = (w / PANGO_SCALE);
	  tmpf->ty = (h / PANGO_SCALE);
	  /* set box area size */
	  tmpf->bbx = (tmpf->tx + 0 /* (drawing_default_textborder * 2) */ );
	  tmpf->bby = (tmpf->ty + 0 /*(drawing_default_textborder * 2) */ );
	}
      /* next entry */
      ix++;
      /* recurse */
      static_maingtk_textsizes_r (un, tmpf);
    }
  return;
}


/*
 * update the (x,y) size of text in the nodes
 *
 * a GTK font pattern. These have the syntax
 * fontname [properties] [fontsize]
 *
 * where fontname is the family name,
 * properties is a list of property values separated by spaces,
 * and fontsize is the point size.
 * The properties that you may specify for GTK font patterns are as follows:
 * Slant properties: ‘Italic’ or ‘Oblique’. If omitted, the default (roman) slant is implied.
 * Weight properties: ‘Bold’, ‘Book’, ‘Light’, ‘Medium’, ‘Semi-bold’, or ‘Ultra-light’. If omitted, ‘Medium’ weight is implied.
 * Width properties: ‘Semi-Condensed’ or ‘Condensed’. If omitted, a default width is used.
 * Here are some examples of GTK font patterns:
 * Monospace 12
 * Monospace Bold Italic 12
 *
 * there are nodes with labels whicj is a piece of text on display
 * and nodes with a record type with multiple small text to layout
 * in multiple steps for the different fields.
 */
static void
static_maingtk_textsizes (void)
{
  splay_tree_node spn = (splay_tree_node) 0;
  struct unode *un = (struct unode *) 0;
  struct uedge *ue = (struct uedge *) 0;
  struct usubg *sg = (struct usubg *) 0;
  cairo_surface_t *surface = NULL;
  cairo_t *crdraw = NULL;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  int w = 0;
  int h = 0;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  /* helper */
  double dy = 0.0;
  /* svg shape scaling */
  double svgtsx = 0.0;
  double svgtsy = 0.0;
  /* svg bb(x,y) size */
  double svgbbx = 0.0;
  double svgbby = 0.0;
  int n_recordnodes = 0;
  /* scan parsed nodes list */
  if (splay_tree_has_data (sp_parsednl))
    {
      /* spn at start of list */
      spn = splay_tree_min (sp_parsednl);
      while (spn)
	{
	  un = (struct unode *) spn->value;
	  /* check if already calculated */
	  if (un->bitflags.txy)
	    {
	      spn = splay_tree_successor (sp_parsednl, spn->key);
	      continue;
	    }

	  /* check if record node */
	  if (un->bitflags.shape == NSHAPE_RECORD)
	    {
	      n_recordnodes++;
	    }

	  /* check if dummy node */
	  if ((un->bitflags.dummynode == 1) || (un->label == NULL))
	    {
	      /* dummy node has no text and text area size (0,0) */
	      un->tx = 0;
	      un->ty = 0;
	      un->bitflags.txy = 1;
	      un->bbx = 0;
	      un->bby = 0;
	      spn = splay_tree_successor (sp_parsednl, spn->key);
	      continue;
	    }

	  /* there should be a label now */
	  if (un->label)
	    {
	      /* check for "" because of gtk/cairo assert */
	      if (strlen (un->label) == 0)
		{
		  /* node has no text and text area size (0,0) */
		  un->utf8label = (char *) 0;
		  un->tx = 0;
		  un->ty = 0;
		  un->bitflags.txy = 1;
		  un->bbx = 0;
		  un->bby = 0;
		  spn = splay_tree_successor (sp_parsednl, spn->key);
		  continue;
		}
	    }

	  /* node with unknown text area size */
	  if (option_gdebug > 1)
	    {
	      printf ("%s(): calculating text area of node \"%s\" with label \"%s\"\n", __FUNCTION__, un->name, un->label);
	      fflush (stdout);
	    }

	  /* check the label */
	  un->utf8label = valid_label_string (un->label);
	  if (un->utf8label == NULL)
	    {
	      /* unusual that node has no text and set text area size (0,0) */
	      printf ("%s(): utf8label is NULL\n", __FUNCTION__);
	      fflush (stdout);
	      un->tx = 0;
	      un->ty = 0;
	      un->bitflags.txy = 1;
	      un->bbx = 0;
	      un->bby = 0;
	      spn = splay_tree_successor (sp_parsednl, spn->key);
	      continue;
	    }

	  if (strlen (un->utf8label) == 0)
	    {
	      /* unusual that node has no text and set text area size (0,0) */
	      printf ("%s(): strlen(utf8label) is 0\n", __FUNCTION__);
	      fflush (stdout);
	      un->utf8label = (char *) 0;
	      un->tx = 0;
	      un->ty = 0;
	      un->bitflags.txy = 1;
	      un->bbx = 0;
	      un->bby = 0;
	      spn = splay_tree_successor (sp_parsednl, spn->key);
	      continue;
	    }

	  un->utf8name = valid_label_string (un->name);
	  if (un->utf8name == NULL)
	    {
	      /* unusual */
	      un->utf8name = "<nil>";
	      printf ("%s(): utf8name is NULL\n", __FUNCTION__);
	      fflush (stdout);
	    }

	  if (strlen (un->utf8name) == 0)
	    {
	      /* unusual */
	      un->utf8name = "<nil>";
	      printf ("%s(): strlen(utf8name) is 0\n", __FUNCTION__);
	      fflush (stdout);
	    }

	  /* calculate the text area */
	  if (surface == NULL)
	    {
	      surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 10, 10);
	    }
	  /* */
	  if (crdraw == NULL)
	    {
	      crdraw = cairo_create (surface);
	    }
	  /* */
	  layout = pango_cairo_create_layout (crdraw);
	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, un->utf8label, -1);
	  /* create the fontname description */
	  memset (buf, 0, 128);
	  /* check if node has a specified fontname */
	  if (un->fontname)
	    {
	      default_fontname = un->fontname;
	    }
	  else
	    {
	      default_fontname = uniqstring ((char *) DEFAULT_FONTNAME);
	    }
	  /* check if node has a specified font slant */
	  default_fontslant = uniqstring ((char *) DEFAULT_FONTSLANT);
	  if (un->bitflags.textoblique)
	    {
	      default_fontslant = uniqstring ((char *) "Oblique");
	    }
	  if (un->bitflags.textitalic)
	    {
	      default_fontslant = uniqstring ((char *) "Italic");
	    }
	  /* check if node has a specified font weight */
	  default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	  if (un->bitflags.textbold)
	    {
	      default_fontweight = uniqstring ((char *) "Bold");
	    }
	  else
	    {
	      default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	    }
	  /* check if node has a specified font size */
	  if (un->fontsize)
	    {
	      default_fontsize = un->fontsize;
	    }
	  else
	    {
	      default_fontsize = uniqstring ((char *) DEFAULT_FONTSIZE);
	    }
	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	  /* copy string buffer */
	  s = uniqstring (buf);
	  /* */
	  desc = pango_font_description_from_string (s);
	  /* check and re-try with defaults */
	  if (desc == NULL)
	    {
	      /* name of font to use, example "Sans" */
	      default_fontname = uniqstring ((char *) DEFAULT_FONTNAME);
	      /* name of slant to use, example "Italic", "Oblique", "Roman" */
	      default_fontslant = uniqstring ((char *) DEFAULT_FONTSLANT);
	      /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
	      default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	      /* name of condensed to use, example "Semi-Condensed", "Condensed" */
	      default_fontcondensed = uniqstring ((char *) DEFAULT_FONTCONDENSED);
	      /* font size to use, example "10", "18", "20" etc. */
	      default_fontsize = uniqstring ((char *) DEFAULT_FONTSIZE);
	      /* change node to use defaults in next situations */
	      un->fontname = NULL;
	      un->bitflags.textoblique = 0;
	      un->bitflags.textitalic = 0;
	      un->bitflags.textbold = 0;
	      un->fontsize = NULL;
	      /* create the fontname description */
	      memset (buf, 0, 128);
	      /* create the font name string */
	      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	      /* copy string buffer */
	      s = uniqstring (buf);
	      /* */
	      desc = pango_font_description_from_string (s);
	    }
	  /* check */
	  if (desc == NULL)
	    {
	      printf ("%s(): desc is NULL\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  /* */
	  pango_layout_set_font_description (layout, desc);
	  /* */
	  pango_font_description_free (desc);
	  /* */
	  pango_cairo_update_layout (crdraw, layout);
	  /* */
	  w = 0;
	  h = 0;
	  /* */
	  pango_layout_get_size (layout, &w, &h);
	  /* */
	  g_object_unref (G_OBJECT (layout));
	  /* */
	  if (option_gdebug > 1)
	    {
	      printf ("%s(): size (%d,%d) for %s utf8 %s\n", __FUNCTION__, (w / PANGO_SCALE), (h / PANGO_SCALE), un->label, un->utf8label);
	      fflush (stdout);
	    }
	  /* set in unode the text area size */
	  un->tx = (w / PANGO_SCALE);
	  un->ty = (h / PANGO_SCALE);
	  /* if label is draw as shadowed text need more space (for summary nodes) */
	  if (un->bitflags.textshadow)
	    {
	      un->tx = (un->tx + 1);
	      un->ty = (un->ty + 1);
	    }
	  /* determine (x,y) size to draw node shape with text */
	  /* edge label does not have a node shape */
	  if (un->bitflags.edgelabel == 1)
	    {
	      un->bbx = un->tx;
	      un->bby = un->ty;
	    }
	  else
	    {
	      /* regular node */
	      /* node shapes */
	      switch (un->bitflags.shape)
		{
		case NSHAPE_ELLIPS:
		  un->bbx = (2 * un->tx);
		  dy = (2 * (un->ty * 0.8));
		  un->bby = (int) dy;
		  break;
		case NSHAPE_CIRCLE:
		  if (un->tx > un->ty)
		    {
		      un->bbx = (1.4 * un->tx);
		      un->bby = (1.4 * un->tx);
		    }
		  else
		    {
		      un->bbx = (1.4 * un->ty);
		      un->bby = (1.4 * un->ty);
		    }
		  break;
		case NSHAPE_RECORD:	/* record label is a box */
		case NSHAPE_BOX:
		  un->bbx = (un->tx + (drawing_default_textborder * 2));
		  un->bby = (un->ty + (drawing_default_textborder * 2));
		  break;
		case NSHAPE_DBOX:
		  un->bbx = (un->tx + 3 + 2 + 3 + 2 + drawing_default_textborder);
		  un->bby = (un->ty + 3 + 2 + 3 + 2 + drawing_default_textborder);
		  break;
		case NSHAPE_RBOX:
		  /* little bit larger because of the arcs */
		  un->bbx = (un->tx + drawing_default_textborder + 22);
		  un->bby = (un->ty + drawing_default_textborder + 22);
		  break;
		case NSHAPE_CARD:
		  svgtsx = ((double) un->tx / (double) SVG_CARD_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_CARD_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_CARD_XSIZE);
		  svgbby = (svgtsy * (double) SVG_CARD_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  break;
		case NSHAPE_CLOUD:
		  svgtsx = ((double) un->tx / (double) SVG_CLOUD_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_CLOUD_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_CLOUD_XSIZE);
		  svgbby = (svgtsy * (double) SVG_CLOUD_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape cloud size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_CLOUD_XSIZE, SVG_CLOUD_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_DIAMOND:
		  svgtsx = ((double) un->tx / (double) SVG_DIAMOND_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_DIAMOND_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_DIAMOND_XSIZE);
		  svgbby = (svgtsy * (double) SVG_DIAMOND_YSIZE);
		  /* set in node as rounded int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape diamond size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_DIAMOND_XSIZE, SVG_DIAMOND_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_MDIAMOND:
		  svgtsx = ((double) un->tx / (double) SVG_MDIAMOND_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_MDIAMOND_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_MDIAMOND_XSIZE);
		  svgbby = (svgtsy * (double) SVG_MDIAMOND_YSIZE);
		  /* set in node as rounded int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape mdiamond size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_MDIAMOND_XSIZE, SVG_MDIAMOND_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_HEXAGON:
		  svgtsx = ((double) un->tx / (double) SVG_HEXAGON_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_HEXAGON_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_HEXAGON_XSIZE);
		  svgbby = (svgtsy * (double) SVG_HEXAGON_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape hexagon size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_HEXAGON_XSIZE, SVG_HEXAGON_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_PENTAGON:
		  svgtsx = ((double) un->tx / (double) SVG_PENTAGON_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_PENTAGON_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_PENTAGON_XSIZE);
		  svgbby = (svgtsy * (double) SVG_PENTAGON_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape PENTAgon size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_PENTAGON_XSIZE, SVG_PENTAGON_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_FOLDER:
		  svgtsx = ((double) un->tx / (double) SVG_FOLDER_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_FOLDER_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_FOLDER_XSIZE);
		  svgbby = (svgtsy * (double) SVG_FOLDER_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape folder size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_FOLDER_XSIZE, SVG_FOLDER_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_SBOX:
		  svgtsx = ((double) un->tx / (double) SVG_SBOX_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_SBOX_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_SBOX_XSIZE);
		  svgbby = (svgtsy * (double) SVG_SBOX_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg shape note size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_SBOX_XSIZE, SVG_SBOX_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_CUBE:
		  svgtsx = ((double) un->tx / (double) SVG_CUBE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_CUBE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_CUBE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_CUBE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg laptop size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_CUBE_XSIZE, SVG_CUBE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_RCLOUD:
		  svgtsx = ((double) un->tx / (double) SVG_RCLOUD_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_RCLOUD_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_RCLOUD_XSIZE);
		  svgbby = (svgtsy * (double) SVG_RCLOUD_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg tux size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_RCLOUD_XSIZE, SVG_RCLOUD_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_CHEVRON:
		  svgtsx = ((double) un->tx / (double) SVG_CHEVRON_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_CHEVRON_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_CHEVRON_XSIZE);
		  svgbby = (svgtsy * (double) SVG_CHEVRON_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_CHEVRON_XSIZE, SVG_CHEVRON_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_CYLINDER:
		  svgtsx = ((double) un->tx / (double) SVG_CYLINDER_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_CYLINDER_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_CYLINDER_XSIZE);
		  svgbby = (svgtsy * (double) SVG_CYLINDER_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_CYLINDER_XSIZE, SVG_CYLINDER_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_DIALOG:
		  svgtsx = ((double) un->tx / (double) SVG_DIALOG_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_DIALOG_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_DIALOG_XSIZE);
		  svgbby = (svgtsy * (double) SVG_DIALOG_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_DIALOG_XSIZE, SVG_DIALOG_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_MAXIMIZE:
		  svgtsx = ((double) un->tx / (double) SVG_MAXIMIZE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_MAXIMIZE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_MAXIMIZE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_MAXIMIZE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_MAXIMIZE_XSIZE, SVG_MAXIMIZE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_PAGE:
		  svgtsx = ((double) un->tx / (double) SVG_PAGE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_PAGE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_PAGE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_PAGE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_PAGE_XSIZE, SVG_PAGE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_PLAQUE:
		  svgtsx = ((double) un->tx / (double) SVG_PLAQUE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_PLAQUE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_PLAQUE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_PLAQUE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_PLAQUE_XSIZE, SVG_PLAQUE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_PREP:
		  svgtsx = ((double) un->tx / (double) SVG_PREP_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_PREP_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_PREP_XSIZE);
		  svgbby = (svgtsy * (double) SVG_PREP_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_PREP_XSIZE, SVG_PREP_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_PUNCHED:
		  svgtsx = ((double) un->tx / (double) SVG_PUNCHED_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_PUNCHED_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_PUNCHED_XSIZE);
		  svgbby = (svgtsy * (double) SVG_PUNCHED_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_PUNCHED_XSIZE, SVG_PUNCHED_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_SEPTAGON:
		  svgtsx = ((double) un->tx / (double) SVG_SEPTAGON_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_SEPTAGON_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_SEPTAGON_XSIZE);
		  svgbby = (svgtsy * (double) SVG_SEPTAGON_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_SEPTAGON_XSIZE, SVG_SEPTAGON_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_SHIELD:
		  svgtsx = ((double) un->tx / (double) SVG_SHIELD_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_SHIELD_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_SHIELD_XSIZE);
		  svgbby = (svgtsy * (double) SVG_SHIELD_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_SHIELD_XSIZE, SVG_SHIELD_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_STORED:
		  svgtsx = ((double) un->tx / (double) SVG_STORED_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_STORED_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_STORED_XSIZE);
		  svgbby = (svgtsy * (double) SVG_STORED_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_STORED_XSIZE, SVG_STORED_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_TRAPEZOID:
		  svgtsx = ((double) un->tx / (double) SVG_TRAPEZOID_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_TRAPEZOID_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_TRAPEZOID_XSIZE);
		  svgbby = (svgtsy * (double) SVG_TRAPEZOID_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_TRAPEZOID_XSIZE, SVG_TRAPEZOID_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_TRIANGLE:
		  svgtsx = ((double) un->tx / (double) SVG_TRIANGLE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_TRIANGLE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_TRIANGLE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_TRIANGLE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_TRIANGLE_XSIZE, SVG_TRIANGLE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_ITRIANGLE:
		  svgtsx = ((double) un->tx / (double) SVG_ITRIANGLE_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_ITRIANGLE_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_ITRIANGLE_XSIZE);
		  svgbby = (svgtsy * (double) SVG_ITRIANGLE_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_ITRIANGLE_XSIZE, SVG_ITRIANGLE_YSIZE);
		      fflush (stdout);
		    }
		  break;
		case NSHAPE_VSCROLL:
		  svgtsx = ((double) un->tx / (double) SVG_VSCROLL_TXSIZE);
		  svgtsy = ((double) un->ty / (double) SVG_VSCROLL_TYSIZE);
		  /* use scaling for the shape size */
		  svgbbx = (svgtsx * (double) SVG_VSCROLL_XSIZE);
		  svgbby = (svgtsy * (double) SVG_VSCROLL_YSIZE);
		  /* set in node as roundedn int */
		  un->bbx = (int) svgbbx;
		  un->bby = (int) svgbby;
		  if (option_gdebug > 1)
		    {
		      printf
			("%s(): scale=(%2.2f,%2.2f) in svg db size (%d,%d) from shape size (%d,%d)\n",
			 __FUNCTION__, svgtsx, svgtsy, un->bbx, un->bby, SVG_VSCROLL_XSIZE, SVG_VSCROLL_YSIZE);
		      fflush (stdout);
		    }
		  break;
		default:
		  printf ("%s(): 4 unknown shape %d\n", __FUNCTION__, un->bitflags.shape);
		  fflush (stdout);
		  un->bbx = (un->tx + drawing_default_textborder);
		  un->bby = (un->ty + drawing_default_textborder);
		  break;
		}

	    }
	  /* indicate size of text is calculated */
	  un->bitflags.txy = 1;
	  /* next node in node list */
	  spn = splay_tree_successor (sp_parsednl, spn->key);
	}

      /* if (splay_tree_has_data (sp_parsednl)... */
    }

  /* set (x,y) size of edge labels */
  if (splay_tree_has_data (sp_parsedel))
    {

      spn = splay_tree_min (sp_parsedel);
      while (spn)
	{
	  ue = (struct uedge *) spn->value;
	  if (ue == (struct uedge *) 0)
	    {
	      printf ("%s(): nil ue should not happen\n", __FUNCTION__);
	      spn = splay_tree_successor (sp_parsedel, spn->key);
	      continue;
	    }
	  /* check for edge label */
	  if (ue->label == NULL)
	    {
	      /* set text area size */
	      ue->tx = 0;
	      ue->ty = 0;
	      /* edge label has no shape */
	      ue->bbx = ue->tx;
	      ue->bby = ue->ty;
	      /* indicate size of text is calculated */
	      ue->bitflags.txy = 1;
	      /* next edge in edge list */
	      spn = splay_tree_successor (sp_parsedel, spn->key);
	      continue;
	    }

	  /* check for edge label */

	  /* check for "" because of gtk/cairo assert */
	  if (strlen (ue->label) == 0)
	    {
	      /* set text area size */
	      ue->tx = 0;
	      ue->ty = 0;
	      /* edge label has no shape */
	      ue->bbx = ue->tx;
	      ue->bby = ue->ty;
	      /* indicate size of text is calculated */
	      ue->bitflags.txy = 1;
	      /* next edge in edge list */
	      spn = splay_tree_successor (sp_parsedel, spn->key);
	      continue;
	    }

	  /* */
	  layout = pango_cairo_create_layout (crdraw);
	  /* check label text */
	  ue->utf8label = valid_label_string (ue->label);
	  if (ue->utf8label == NULL)
	    {
	      /* unusual */
	      printf ("%s(): ue->utf8label is NULL and label is \"%s\"\n", __FUNCTION__, ue->label);
	      fflush (stdout);
	      /* set text area size */
	      ue->tx = 0;
	      ue->ty = 0;
	      /* edge label has no shape */
	      ue->bbx = ue->tx;
	      ue->bby = ue->ty;
	      /* indicate size of text is calculated */
	      ue->bitflags.txy = 1;
	      /* next edge in edge list */
	      spn = splay_tree_successor (sp_parsedel, spn->key);
	      continue;
	    }

	  if (strlen (ue->utf8label) == 0)
	    {
	      /* unusual */
	      printf ("%s(): strlen(ue->utf8label) is 0 and label is \"%s\"\n", __FUNCTION__, ue->label);
	      fflush (stdout);
	      ue->utf8label = (char *) 0;
	      /* set text area size */
	      ue->tx = 0;
	      ue->ty = 0;
	      /* edge label has no shape */
	      ue->bbx = ue->tx;
	      ue->bby = ue->ty;
	      /* indicate size of text is calculated */
	      ue->bitflags.txy = 1;
	      /* next edge in edge list */
	      spn = splay_tree_successor (sp_parsedel, spn->key);
	      continue;
	    }
	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, ue->utf8label, -1);
	  /* create the fontname description */
	  memset (buf, 0, 128);
	  /* check if edge label has a specified fontname */
	  if (ue->fontname)
	    {
	      default_fontname = ue->fontname;
	    }
	  else
	    {
	      default_fontname = uniqstring ((char *) DEFAULT_FONTNAME);
	    }
	  /* check if edge label has a specified font slant */
	  default_fontslant = uniqstring ((char *) DEFAULT_FONTSLANT);
	  if (ue->bitflags.textoblique)
	    {
	      default_fontslant = "Oblique";
	    }
	  if (ue->bitflags.textitalic)
	    {
	      default_fontslant = "Italic";
	    }
	  /* check if edge label has a specified font weight */
	  default_fontweight = uniqstring ((char *) DEFAULT_FONTWEIGHT);
	  if (ue->bitflags.textbold)
	    {
	      default_fontweight = uniqstring ((char *) "Bold");
	    }
	  /* check if edge label has a specified font size */
	  if (ue->fontsize)
	    {
	      default_fontsize = ue->fontsize;
	    }
	  else
	    {
	      default_fontsize = uniqstring ((char *) DEFAULT_FONTSIZE);
	    }

	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	  /* copy string buffer */
	  s = uniqstring (buf);
	  /* */
	  desc = pango_font_description_from_string (s);
	  /* check and re-try with defaults */
	  if (desc == NULL)
	    {
	      /* name of font to use, example "Sans" */
	      default_fontname = DEFAULT_FONTNAME;
	      /* name of slant to use, example "Italic", "Oblique", "Roman" */
	      default_fontslant = DEFAULT_FONTSLANT;
	      /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
	      default_fontweight = DEFAULT_FONTWEIGHT;
	      /* name of condensed to use, example "Semi-Condensed", "Condensed" */
	      default_fontcondensed = DEFAULT_FONTCONDENSED;
	      /* font size to use, example "10", "18", "20" etc. */
	      default_fontsize = DEFAULT_FONTSIZE;
	      /* change edge label to use defaults in next situations */
	      ue->fontname = NULL;
	      ue->bitflags.textoblique = 0;
	      ue->bitflags.textitalic = 0;
	      ue->bitflags.textbold = 0;
	      ue->fontsize = NULL;
	      /* create the fontname description */
	      memset (buf, 0, 128);
	      /* create the font name string */
	      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
	      /* copy string buffer */
	      s = uniqstring (buf);
	      /* */
	      desc = pango_font_description_from_string (s);
	    }
	  /* check */
	  if (desc == NULL)
	    {
	      printf ("%s(): desc is NULL\n", __FUNCTION__);
	      fflush (stdout);
	    }
	  /* */
	  pango_layout_set_font_description (layout, desc);
	  /* */
	  pango_font_description_free (desc);
	  /* */
	  pango_cairo_update_layout (crdraw, layout);
	  /* */
	  w = 0;
	  h = 0;
	  /* */
	  pango_layout_get_size (layout, &w, &h);
	  /* */
	  g_object_unref (G_OBJECT (layout));
	  /* */
	  if (option_gdebug > 1)
	    {
	      printf ("%s(): size (%d,%d) for %s utf8 %s\n", __FUNCTION__, (w / PANGO_SCALE), (h / PANGO_SCALE), ue->label, ue->utf8label);
	      fflush (stdout);
	    }
	  /* set text area size */
	  ue->tx = (w / PANGO_SCALE);
	  ue->ty = (h / PANGO_SCALE);
	  /* edge label has no shape */
	  ue->bbx = ue->tx;
	  ue->bby = ue->ty;
	  /* indicate size of text is calculated */
	  ue->bitflags.txy = 1;
	  /* next edge in edge list */
	  spn = splay_tree_successor (sp_parsedel, spn->key);
	}

      /* if (splay_tree_has_data (sp_parsedel))... */
    }

  /* set (x,y) size of summary nodes in subgraphs */
  if (splay_tree_has_data (sp_parsedsgl))
    {

      spn = splay_tree_min (sp_parsedsgl);
      while (spn)
	{
	  sg = (struct usubg *) spn->value;
	  maingtk_textsizes_subg (sg, crdraw);
	  /* indicate size of text is calculated */
	  sg->bitflags.txy = 1;
	  /* next subgraph in subgraph list */
	  spn = splay_tree_successor (sp_parsedsgl, spn->key);
	}
    }

  if (option_gdebug || 0)
    {
      printf ("%s(): found %d record nodes with text fields\n", __FUNCTION__, n_recordnodes);
      fflush (stdout);
    }

  /* are there record nodes? */
  if (n_recordnodes)
    {
      /* scan the fields of the record nodes */
      /* scan parsed nodes list */
      if (splay_tree_has_data (sp_parsednl))
	{
	  /* spn at start of list */
	  spn = splay_tree_min (sp_parsednl);
	  while (spn)
	    {
	      un = (struct unode *) spn->value;
	      /* get the record nodes now */
	      if (un->bitflags.shape == NSHAPE_RECORD)
		{
		  /* there should be fields */
		  /* get the size of the text in the fields */
		  if (un->f != NULL)
		    {
		      static_maingtk_textsizes_r (un, un->f);
		    }
		  un->bitflags.txy = 1;
		}
	      spn = splay_tree_successor (sp_parsednl, spn->key);
	    }

	  if (option_gdebug > 1 || 0)
	    {
	      /* spn at start of list */
	      spn = splay_tree_min (sp_parsednl);
	      while (spn)
		{
		  un = (struct unode *) spn->value;
		  /* get the record nodes now */
		  if (un->bitflags.shape == NSHAPE_RECORD || 0)
		    {
		      /* there should be fields */
		      if (un->f != NULL)
			{
			  /* print data in the fields */
			  static_maingtk_textsizes_r2 (un, un->f);
			}
		    }
		  spn = splay_tree_successor (sp_parsednl, spn->key);
		}
	      fflush (stdout);
	    }

	  /* process more of the node sizes */
	  {
	    struct pnt xy;
	    /* spn at start of list */
	    spn = splay_tree_min (sp_parsednl);
	    while (spn)
	      {
		un = (struct unode *) spn->value;
		/* get the record nodes now */
		if (un->bitflags.shape == NSHAPE_RECORD || 0)
		  {
		    /* there should be fields */
		    if (un->f != NULL)
		      {
			xy.x = 0;
			xy.y = 0;
			/* sizeof data in the fields */
			xy = static_maingtk_textsizes_siz1r (un, un->f);
			un->bbx = xy.x;
			un->bby = xy.y;
			if (option_gdebug || 0)
			  {
			    printf ("%s(): size (%d,%d) for \"%s\"\n", __FUNCTION__, un->bbx, un->bby, un->name);
			  }
			xy = static_maingtk_textsizes_siz2r (un->f, 0, 0);
			if (xy.x)
			  {
			  }

			/* print data in the fields */
			if (option_gdebug > 1 || 0)
			  {
			    static_maingtk_textsizes_r2 (un, un->f);
			  }

			/* adjust fields */
			static_maingtk_textsizes_r4 (un->f, un->f->xsiz, un->f->ysiz);

		      }
		  }
		spn = splay_tree_successor (sp_parsednl, spn->key);
	      }
	    fflush (stdout);
	  }


	}


    }				/* endof "if (n_recordnodes)..." */


  /* clear the draw context etc */
  if (layout)
    {
      layout = NULL;
    }

  if (crdraw)
    {
      cairo_destroy (crdraw);
      crdraw = NULL;
    }

  if (surface)
    {
      cairo_surface_destroy (surface);
      surface = NULL;
    }

  return;
}

/* set (x,y) text size of summary node in subgraph */
static void
maingtk_textsizes_subg (struct usubg *subg, cairo_t * crdraw)
{
  struct unode *un = NULL;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  int w = 0;
  int h = 0;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  /* helper */
  double dy = 0.0;
  splay_tree_node spn = (splay_tree_node) 0;
  struct usubg *sg = (struct usubg *) 0;
  /* recurse */
  if (splay_tree_has_data (subg->sp_sg))
    {

      spn = splay_tree_min (subg->sp_sg);
      while (spn)
	{
	  sg = (struct usubg *) spn->value;
	  maingtk_textsizes_subg (sg, crdraw);
	  spn = splay_tree_successor (subg->sp_sg, spn->key);
	}
    }

  /* check */
  if (subg->summaryn == NULL)
    {
      /* shouldnothappen */
      printf ("%s(): summaryn is NULL\n", __FUNCTION__);
      fflush (stdout);
      return;
    }

  /* the summary node */
  un = subg->summaryn;
  /* make sure it has a display label */
  if (un->label == NULL)
    {
      un->label = un->name;
    }
  if (un->label == NULL)
    {
      un->label = subg->label;
    }
  if (un->label == NULL)
    {
      un->label = subg->name;
    }
  if (un->label == NULL)
    {
      un->label = uniqstring ((char *) "shouldnothappen1");
    }
  if (strlen (un->label) == 0)
    {
      un->label = uniqstring ((char *) "shouldnothappen2");
    }
  /* */
  layout = pango_cairo_create_layout (crdraw);
  /* check label */
  un->utf8label = valid_label_string (un->label);
  un->utf8name = valid_label_string (un->name);
  subg->utf8name = valid_label_string (subg->name);
  if ((un->utf8label == NULL) || (un->utf8name == NULL) || (subg->utf8name == NULL))
    {
      printf ("%s(): utf8label is NULL\n", __FUNCTION__);
      fflush (stdout);
      un->tx = 0;
      un->ty = 0;
      un->bitflags.txy = 1;
      un->bbx = 0;
      un->bby = 0;
      return;
    }

  if ((strlen (un->utf8label) == 0) || (strlen (un->utf8name) == 0) || (strlen (subg->utf8name) == 0))
    {
      printf ("%s(): strlen(utf8label) is 0\n", __FUNCTION__);
      fflush (stdout);
      un->tx = 0;
      un->ty = 0;
      un->bitflags.txy = 1;
      un->bbx = 0;
      un->bby = 0;
      return;
    }

  /* set text to display */
  pango_layout_set_text (layout, un->utf8label, -1);
  /* create the fontname description */
  memset (buf, 0, 128);
  /* check if node has a specified fontname */
  if (un->fontname)
    {
      default_fontname = un->fontname;
    }
  else
    {
      default_fontname = DEFAULT_FONTNAME;
    }

  /* check if node has a specified font slant */
  default_fontslant = DEFAULT_FONTSLANT;
  if (un->bitflags.textoblique)
    {
      default_fontslant = "Oblique";
    }
  if (un->bitflags.textitalic)
    {
      default_fontslant = "Italic";
    }
  /* check if node has a specified font weight */
  default_fontweight = DEFAULT_FONTWEIGHT;
  if (un->bitflags.textbold)
    {
      default_fontweight = "Bold";
    }
  else
    {
      default_fontweight = DEFAULT_FONTWEIGHT;
    }

  /* check if node has a specified font size */
  if (un->fontsize)
    {
      default_fontsize = un->fontsize;
    }
  else
    {
      default_fontsize = DEFAULT_FONTSIZE;
    }

  /* create the font name string */
  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
  /* copy string buffer */
  s = uniqstring (buf);
  /* */
  desc = pango_font_description_from_string (s);
  /* check and re-try with defaults */
  if (desc == NULL)
    {
      /* name of font to use, example "Sans" */
      default_fontname = DEFAULT_FONTNAME;
      /* name of slant to use, example "Italic", "Oblique", "Roman" */
      default_fontslant = DEFAULT_FONTSLANT;
      /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
      default_fontweight = DEFAULT_FONTWEIGHT;
      /* name of condensed to use, example "Semi-Condensed", "Condensed" */
      default_fontcondensed = DEFAULT_FONTCONDENSED;
      /* font size to use, example "10", "18", "20" etc. */
      default_fontsize = DEFAULT_FONTSIZE;
      /* change node to use defaults in next situations */
      un->fontname = NULL;
      un->bitflags.textoblique = 0;
      un->bitflags.textitalic = 0;
      un->bitflags.textbold = 0;
      un->fontsize = NULL;
      /* create the fontname description */
      memset (buf, 0, 128);
      /* create the font name string */
      snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname, default_fontslant, default_fontweight, default_fontcondensed, default_fontsize);
      /* copy string buffer */
      s = uniqstring (buf);
      /* */
      desc = pango_font_description_from_string (s);
    }
  /* check */
  if (desc == NULL)
    {
      printf ("%s(): desc is NULL\n", __FUNCTION__);
      fflush (stdout);
    }
  /* */
  pango_layout_set_font_description (layout, desc);
  /* */
  pango_font_description_free (desc);
  /* */
  pango_cairo_update_layout (crdraw, layout);
  /* */
  w = 0;
  h = 0;
  /* */
  pango_layout_get_size (layout, &w, &h);
  /* */
  g_object_unref (G_OBJECT (layout));
  /* */
  if (option_gdebug > 1)
    {
      printf ("%s(): size (%d,%d) for %s utf8 %s\n", __FUNCTION__, (w / PANGO_SCALE), (h / PANGO_SCALE), un->label, un->utf8label);
      fflush (stdout);
    }
  /* set in unode the text area size */
  un->tx = (w / PANGO_SCALE);
  un->ty = (h / PANGO_SCALE);
  un->bitflags.txy = 1;
  /* determine (x,y) size to draw node shape with text */
  /* edge label does not have a node shape */
  if (un->bitflags.edgelabel == 1)
    {
      un->bbx = un->tx;
      un->bby = un->ty;
    }
  else
    {
      /* regular node */
      /* node shapes */
      switch (un->bitflags.shape)
	{
	case NSHAPE_ELLIPS:
	  un->bbx = (2 * un->tx);
	  dy = 2 * (un->ty * 0.8);
	  un->bby = (int) dy;
	  break;
	case NSHAPE_CIRCLE:
	  if (un->tx > un->ty)
	    {
	      un->bbx = (1.4 * un->tx);
	      un->bby = (1.4 * un->tx);
	    }
	  else
	    {
	      un->bbx = (1.4 * un->ty);
	      un->bby = (1.4 * un->ty);
	    }
	  break;
	case NSHAPE_BOX:
	case NSHAPE_DBOX:
	case NSHAPE_RBOX:
	default:
	  un->bbx = (un->tx + drawing_default_textborder);
	  un->bby = (un->ty + drawing_default_textborder);
	  break;
	}
    }

  return;
}

/* use full screen */
static void
on_top_level_window_fullscreen1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  gtk_window_fullscreen (GTK_WINDOW (mainwindow1));
  return;
}

/* use screen in a window */
static void
on_top_level_window_unfullscreen1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  gtk_window_unfullscreen (GTK_WINDOW (mainwindow1));
  return;
}

/* heuristic select barycenter */
static void
on_top_level_window_heuristicbary_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  if (set_heuristic ("bary"))
    {
      /* set color drawing to normal */
      reset_altcolor ();
      /* calculate the new graph */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }
  return;
}

/* heuristic barycenter adjust node weight average */
static void
on_top_level_window_heuristicadjustweightavg_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  if (set_adjustweight ("avg"))
    {
      /* set color drawing to normal */
      reset_altcolor ();
      /* calculate the new graph */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }
  return;
}

/* heuristic barycenter adjust node weight left */
static void
on_top_level_window_heuristicadjustweightleft_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  if (set_adjustweight ("left"))
    {
      /* set color drawing to normal */
      reset_altcolor ();
      /* calculate the new graph */
      calculate ();
      /* re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }
  return;
}

/* normal, level interleaved with dummy nodes level */
static void
on_top_level_window_heuristiclevelsnormal_activate (GtkMenuItem * menuitem, gpointer user_data)
{

  if (menuitem)
    {
    }

  if (user_data)
    {
    }

  levelextra = 0;

  /* set color drawing to normal */
  reset_altcolor ();

  /* calculate the new graph */
  calculate ();

  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* extra level dummy nodes */
static void
on_top_level_window_heuristiclevelsextra_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
/* temp turned off todo fix in draw routines */
  if (0)
    {
      levelextra = 1;
    }
  else
    {
      levelextra = 0;
    }
  /* set color drawing to normal */
  reset_altcolor ();
  /* calculate the new graph */
  calculate ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}




/* show about with ok button */
static void
on_top_level_window_show_about (GtkWidget * widget, gpointer data)
{
  GtkWidget *dialog = NULL;
  if (widget)
    {
    }
  if (data)
    {
    }

  dialog = gtk_about_dialog_new ();
#if GTK_HAVE_API_VERSION_2 == 1
  gtk_about_dialog_set_name (GTK_ABOUT_DIALOG (dialog), PACKAGE_NAME);
#endif
#if GTK_HAVE_API_VERSION_3 == 1
  gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (dialog), PACKAGE_NAME);
#endif
  gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (dialog), PACKAGE_VERSION);
  gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (dialog), "Copyright is GNU GPL Version 3, see http://www.gnu.org/");

  gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG (dialog),
				 "This is a graph layout program for dot and tys graph data files.\n"
				 "This program is free to share, copy, use and modify.\n"
				 "Used with gcc-4.7.2, gcc-4.9.2, gcc-6.0, gcc-6.1, gcc-7.1, gcc-8.0\n"
				 "/*\n"
				 " *  This program is free software: you can redistribute it and/or modify\n"
				 " *  it under the terms of the GNU General Public License as published by\n"
				 " *  the Free Software Foundation, either version 3 of the License, or\n"
				 " *  (at your option) any later version.\n"
				 " *  These are the four essential freedoms with GNU GPL software:\n"
				 " *  1: freedom to run the program, for any purpose\n"
				 " *  2: freedom to study how the program works, and change it to make it do what you wish\n"
				 " *  3: freedom to redistribute copies to help your Free Software girlfriends and friends\n"
				 " *  4: freedom to distribute copies of your modified versions to your Free Software girlfriends and friends\n"
				 " */\n" "tuxsee AT users.sourceforge.net\n");


  gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (dialog), PACKAGE_URL);
  /* suppress worning */
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (mainwindow1));
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
  return;
}

/* center prio1 mode */
static void
on_top_level_window_centerm (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_CENTERM)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_CENTERM;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* center prio2 mode */
static void
on_top_level_window_centern (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_CENTERN)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_CENTERN;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode following edges */
static void
on_top_level_window_followi (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_FOLLOWI)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_FOLLOWI;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode following edges */
static void
on_top_level_window_followl (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_FOLLOWL)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_FOLLOWL;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode following edges */
static void
on_top_level_window_followr (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_FOLLOWR)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_FOLLOWR;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode following edges */
static void
on_top_level_window_followo (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_FOLLOWO)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_FOLLOWO;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode following edges */
static void
on_top_level_window_left (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }
  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }
  /* check for mode change */
  if (xplace_mode == XPLACE_LEFT)
    {
      return;
    }
  /* set to draw follow edges */
  xplace_mode = XPLACE_LEFT;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* draw mode centered */
static void
on_top_level_window_centered (GtkWidget * widget, gpointer data)
{
  if (widget)
    {
    }
  if (data)
    {
    }

  /* check if there is node data to draw */
  if (drawnldata == NULL)
    {
      return;
    }

  /* check if changed */
  if (xplace_mode == XPLACE_CENTERED)
    {
      return;
    }
  /* set to draw centered */
  xplace_mode = XPLACE_CENTERED;
  /* set color drawing to normal */
  reset_altcolor ();
  /* re-position the new graph */
  layouter2draw_pos ();
  /* re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* treeview window is in maintgk2.c */

/* show statistics dialog with ok button */
static void
on_top_level_window_show_statistics (GtkWidget * widget, gpointer data)
{
  GtkWidget *statisticsdialog = NULL;
  char buf[512];
  char *s = NULL;
  if (widget)
    {
    }
  if (data)
    {
    }

  /* check if there is node data to draw
   * if (drawnldata == NULL) { return; }
   */

  memset (buf, 0, 512);
  snprintf (buf, (512 - 1),
	    "pre-algo: %s\nalgorithm: %s\nadjust-weight: %s\n%d input nodes\n%d input single-nodes\n%d input edges\n%d reversed edges\n%d input self-edges\n%d input edgelabels\n%d input subgraphs\n%d drawing nodes\n%d drawing edges\n%d levels\n%d cycles\n%d begin crossings\n%d end crossings\ndfs start level %d\nmalloc() is %llu times used\nfree() is %llu times used\ngraph layout uses %llu bytes",
	    /* pre-processor for barycenter algorithm */
	    get_preprocessor (),
	    /* barycenter algorithm */
	    get_heuristic (),
	    /* barycenter algorithm node adjust weight */
	    get_adjustweight (),
	    /* input number of nodes */
	    n_input_nodes,
	    /* input number of singlenodes */
	    n_input_singlenodes,
	    /* input number of edges */
	    n_input_edges,
	    /* input number of reversed edges */
	    n_input_revedges,
	    /* input number of selfedges */
	    n_input_selfedges,
	    /* input number of edge labels */
	    n_input_edgelabels,
	    /* input number of subgraphs */
	    n_input_subgraphs,
	    /* drawing nodes */
	    n_drawing_nodes,
	    /* drawing edges */
	    n_drawing_edges,
	    /* number of levels */
	    dli_nlevels,
	    /* cycles in folded graph */
	    n_folded_cycles,
	    /* start crossings */
	    get_begin_crossings (),
	    /* end crossings */
	    get_end_crossings (),
	    /* where dfs started */
	    dfsstartlevel,
	    /* how many times malloc() is called */
	    ntotalmallocs (),
	    /* how many times free() is called */
	    ntotalfree (),
	    /* how much memory is in use */
	    ntotalmemuse ());
  s = uniqstring (buf);
  statisticsdialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1), GTK_DIALOG_MODAL, GTK_MESSAGE_OTHER, GTK_BUTTONS_OK, "%s", (char *) s);
  gtk_window_set_transient_for (GTK_WINDOW (statisticsdialog), GTK_WINDOW (mainwindow1));
  gtk_dialog_run (GTK_DIALOG (statisticsdialog));
  gtk_widget_destroy (statisticsdialog);
  statisticsdialog = NULL;
  return;
}

/* show versions dialog with ok button */
static void
on_top_level_window_show_versions (GtkWidget * widget, gpointer data)
{
  GtkWidget *versionsdialog = NULL;
  char buf[512];
  char *s = NULL;
  if (widget)
    {
    }
  if (data)
    {
    }
  memset (buf, 0, 512);

#if GTK_HAVE_API_VERSION_2 == 1
  snprintf (buf, (512 - 1),
	    "compiled with library versions:\nrunning with gtk version %d.%d.%d\ncompiled with gtk version %d.%d.%d\ncairo-library version %s\nrhp layout lib version %s",
	    /* gtk version */
	    GTK_MAJOR_VERSION,
	    /* gtk version */
	    GTK_MINOR_VERSION,
	    /* gtk version */
	    GTK_MICRO_VERSION,
	    /* gtk version */
	    GTK_MAJOR_VERSION,
	    /* gtk version */
	    GTK_MINOR_VERSION,
	    /* gtk version */
	    GTK_MICRO_VERSION,
	    /* gtk cairo drawing version */
	    cairo_version_string (),
	    /* layout lib */
	    rhp_version ());
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  snprintf (buf, (512 - 1),
	    "compiled with library versions:\nrunning with gtk version %d.%d.%d\ncompiled with gtk version %d.%d.%d\ncairo-library version %s\nrhp layout lib version %s",
	    /* gtk version */
	    gtk_get_major_version (),
	    /* gtk version */
	    gtk_get_minor_version (),
	    /* gtk version */
	    gtk_get_micro_version (),
	    /* gtk version */
	    GTK_MAJOR_VERSION,
	    /* gtk version */
	    GTK_MINOR_VERSION,
	    /* gtk version */
	    GTK_MICRO_VERSION,
	    /* gtk cairo drawing version */
	    cairo_version_string (),
	    /* layout lib */
	    rhp_version ());
#endif

  /* */
  s = uniqstring (buf);
  versionsdialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1), GTK_DIALOG_MODAL,
					   /* GTK_MESSAGE_INFO */
					   GTK_MESSAGE_OTHER, GTK_BUTTONS_OK, "%s", s);
  gtk_window_set_transient_for (GTK_WINDOW (versionsdialog), GTK_WINDOW (mainwindow1));
  gtk_dialog_run (GTK_DIALOG (versionsdialog));
  gtk_widget_destroy (versionsdialog);
  versionsdialog = NULL;
  return;
}

/* end */
