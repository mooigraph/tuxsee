/* replacement malloc/realloc only used whtn win32 mingw compilation */
#include <stdio.h>
#include <stdlib.h>

void *rpl_malloc(size_t n){return(malloc(n));}
void *rpl_realloc(void *ptr, size_t n){return(realloc(ptr,n));}
