
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

/* creates hex dump of the source code to include in the binary */

#include <stdio.h>

/* ./a.out tuxsee.tar.gz >source.c */

int
main (int argc, char *argv[])
{
  FILE *f = NULL;
  unsigned int n = 0;
  unsigned char buf[2];
  size_t nr = 0;
  if (argc != 2)
    {
      printf ("argc=%d\n", argc);
      return (0);
    }
  f = fopen (argv[1], "rb");
  if (f == NULL)
    {
      printf ("/* f is null at %s */\n", argv[1]);
      return (0);
    }
  /* max. 2G */
  printf ("%s\n", "unsigned char source_bytes[] = {");
  for (;;)
    {
      buf[0] = 0;
      nr = fread (&buf, 1, 1, f);
      if (nr == 0)
	{
	  break;
	}
      printf ("%d,", buf[0]);
      n = n + 1;
      if ((n % 16) == 0)
	{
	  printf ("\n");
	}
    }
  printf ("%s\n", "0 };");
  printf ("unsigned int n_source_bytes = %d;\n", n);
  fclose (f);
  return (0);
}

/* end */
