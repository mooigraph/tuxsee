
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

/*
 * test program to run rhp layout without gtk, parser etc.
 * this needs config.h which must have set HAVE_INTTYPES_H
 * this needs <stdint.h>
 * this needs c99 mode -std=c99
 * this needs mem.h
 * mem.c needs options.h with memtrack and memlog int settings
 * or put here the wrappers for mymalloc() and myfree()
 * mymalloc() *must* return zeroed memory
 * check memory usage with valgrind
 * valgrind --tool=memcheck -v --leak-check=full --show-leak-kinds=all --track-origins=yes  ./mainrhp 1>O1 2>O2
 */

#include <stdio.h>

#include "rhp.h"


int
main (int rgc, char *argv[])
{
  int status = 0;
  printf ("version %s\n", rhp_version ());
  /* init, create log file, loglevel 2 includes memory tracking otherwise 1 or 0 (off) */
  rhp_init ("mainrhplog.txt", 2);
  /* add node number 0, level 0, no data attached */
  status = rhp_addnode (0, 0, NULL);
  if (status)
    {
      /* if here the some error */
    }
  /* add node number 1, level 1, no data attached */
  status = rhp_addnode (1 /* node number */ , 1 /* level */ , NULL /* attached data */ );
  status = rhp_addnode (2, 2, NULL);
  status = rhp_addnode (3, 3, NULL);
  /* create edge 0, from node 0 to node 1, no data attached */
  status = rhp_addedge (0, 0, 1, NULL);
  status = rhp_addedge (1 /* edge number */ , 1 /* from-node */ , 2 /* to-node */ , NULL /* attached data */ );
  status = rhp_addedge (2, 2, 3, NULL);
  /* run layout, no options, left node weight adjust */
  rhp_layout (0, 0);
  /* here using foreach() the calculated layout can be retrieved */
  /* clear all memory */
  rhp_deinit ();
  return (0);
}

/* end */
